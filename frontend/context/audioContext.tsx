'use client';

import { createContext, useContext, useEffect, useRef, useState } from "react";
import { useSettings } from "./settingsContext";

interface AudioContextType {
  playMusic: (src: string) => void;
  stopMusic: () => void;
  playSoundEffect: (src: string) => void;
  playSoundAmbianceEffect: (src: string) => void;
  playSoundVoiceEffect: (src: string) => void;
  playSoundInterfaceEffect: (src: string) => void;
  isPlaying: boolean;
}

export enum MusicEnum {
  ENDGAME="/music/endgame.mp3",
  MENU="/music/menu.mp3",
  GAME="/music/game.mp3",
  TRANSITION="/music/transition.wav"
}

export enum SoundEnum {
  ARRIVAL_0="/sounds/se_game_arrival_0.wav",
  ATTACK_0="/sounds/se_game_attack_0.wav",
  ATTACK_1="/sounds/se_game_attack_1.wav",
  CARD_CANCEL="/sounds/se_game_card_cancel.wav",
  CARD_TAKE="/sounds/se_game_card_take.wav",
  DEFEAT="/sounds/se_game_defeat.wav",
  ENDTURN="/sounds/se_game_endturn_0.mp3",
  VICTORY="/sounds/se_game_victory.wav",
  CANCEL="/sounds/se_menu_cancel.wav",
  COUNTDOWN="/sounds/se_menu_countdown.wav",
  ERROR="/sounds/se_menu_error.wav",
  INTERFACE_0="/sounds/se_menu_interface_0.wav",
  INTERFACE_1="/sounds/se_menu_interface_1.wav",
  NOTIFICATION="/sounds/se_menu_notification.wav",
  START_0="/sounds/se_menu_start_0.wav",
  START_1="/sounds/se_menu_start_1.wav",
  TRANSITION_0="/sounds/se_menu_transition_0.wav",
  TRANSITION_1="/sounds/se_menu_transition_1.wav",
}


const AudioContext = createContext<AudioContextType | undefined>(undefined);

export const AudioProvider = ({ children }: { children: React.ReactNode }) => {
  const audioRef = useRef<HTMLAudioElement | null>(null);
  const [isPlaying, setIsPlaying] = useState(false);

  const { masterVolume,
    musicVolume,
    ambiantVolume,
    interfaceVolume,
    effectVolume,
    voiceVolume } = useSettings();

  const playMusic = (src: string) => {
    if (audioRef.current) {
      // Si la musique actuelle est différente de la nouvelle source
      if (audioRef.current.src !== new URL(src, window.location.href).href) {
        audioRef.current.pause(); // Arrêter l'ancienne musique
        audioRef.current = new Audio(src); // Créer un nouvel objet Audio
        audioRef.current.loop = true; // Boucler la musique
        audioRef.current.volume = musicVolume * masterVolume; 
      }
    } else {
      // Première initialisation
      audioRef.current = new Audio(src);
      audioRef.current.loop = true;
      audioRef.current.volume = musicVolume * masterVolume; 
    }

    audioRef.current.play()
      .then(() => setIsPlaying(true))
      .catch((err) => {
        document.addEventListener("click", handleUserInteraction, { once: true });
        document.addEventListener("keydown", handleUserInteraction, { once: true });
      });
  };


  const handleUserInteraction = () => {
    if (audioRef.current) {
      audioRef.current.play().then(() => setIsPlaying(true));
    }
  };

  const stopMusic = () => {
    if (audioRef.current) {
      audioRef.current.pause();
      setIsPlaying(false);
    }
  };

  const playSoundEffect = (src: string) => {
    const sound = new Audio(src);
    sound.volume = effectVolume * masterVolume; 
    sound.play().catch((err) => {
      document.addEventListener("click", handleUserInteraction, { once: true });
      document.addEventListener("keydown", handleUserInteraction, { once: true });
    });
  };

  const playSoundAmbianceEffect = (src: string) => {
    const sound = new Audio(src);
    sound.volume = ambiantVolume * masterVolume; 
    sound.play().catch((err) => {
      document.addEventListener("click", handleUserInteraction, { once: true });
      document.addEventListener("keydown", handleUserInteraction, { once: true });
    });
  };

  const playSoundVoiceEffect = (src: string) => {
    const sound = new Audio(src);
    sound.volume = voiceVolume * masterVolume; 
    sound.play().catch((err) => {
      document.addEventListener("click", handleUserInteraction, { once: true });
      document.addEventListener("keydown", handleUserInteraction, { once: true });
    });
  };

  const playSoundInterfaceEffect = (src: string) => {
    const sound = new Audio(src);
    sound.volume = interfaceVolume * masterVolume; 
    sound.play().catch((err) => {
      document.addEventListener("click", handleUserInteraction, { once: true });
      document.addEventListener("keydown", handleUserInteraction, { once: true });
    });
  };

  useEffect(() => {
    if(audioRef.current === null) return;
    audioRef.current.volume = musicVolume * masterVolume; 
  }, [musicVolume, masterVolume]);

  return (
    <AudioContext.Provider value={{ playMusic, stopMusic, playSoundEffect, playSoundAmbianceEffect, playSoundInterfaceEffect, playSoundVoiceEffect, isPlaying }}>
      {children}
    </AudioContext.Provider>
  );
};

export const useAudio = () => {
  const context = useContext(AudioContext);
  if (!context) {
    throw new Error("useAudio must be used within an AudioProvider");
  }
  return context;
};