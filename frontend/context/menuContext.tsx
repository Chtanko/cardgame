'use client';

import { EscapeMenu } from "@/components/MenuComponent";
import { Settings } from "@/components/SettingsComponent";
import { MusicEnum, SoundEnum, useAudio } from "@/context/audioContext";
import { usePathname } from "next/navigation";
import { useEffect, createContext, useContext, useState } from "react";

export type MenuContextType = {
  openMenu: () => void;
  openSettings: () => void;
  openChat: () => void;
  openedMenu: boolean;
  openedSettings: boolean;
  openedChat: boolean;
};

const MenuContext = createContext<MenuContextType | undefined>(undefined);

export const MenuProvider =  ({ children }: { children: React.ReactNode }) => {

  const [openedMenu, setOpenedMenu] = useState<boolean>(false);
  const [openedSettings, setOpenedSettings] = useState<boolean>(false);
  const [openedChat, setOpenedChat] = useState<boolean>(false);


  const { playMusic, playSoundInterfaceEffect } = useAudio();
  const pathname = usePathname(); 
  useEffect(() => {
    console.log(pathname);
    if (pathname.includes("/duel")) {
      playMusic(MusicEnum.GAME);
    } else {
      playMusic(MusicEnum.MENU);
    }
  }, [pathname]);

  const openMenu = () =>{
    playSoundInterfaceEffect(SoundEnum.INTERFACE_0);
    setOpenedMenu(prevState => !prevState);
    setOpenedSettings(false);
    setOpenedChat(false);
  }
  const openSettings = () =>{
    playSoundInterfaceEffect(SoundEnum.INTERFACE_0);
    setOpenedMenu(false);
    setOpenedSettings(prevState => !prevState);
    setOpenedChat(false);
  }
  const openChat = () => {
    playSoundInterfaceEffect(SoundEnum.INTERFACE_0);
    setOpenedMenu(false);
    setOpenedSettings(false);
    setOpenedChat(prevState => !prevState);
  }
  
  return (
    <MenuContext.Provider value={{ openMenu, openSettings, openChat, openedMenu, openedSettings, openedChat}}>
      <EscapeMenu/>
      <Settings/>
      {children}
    </MenuContext.Provider>
  )
}

export const useMenu = () => {
    const context = useContext(MenuContext);
    if (!context) {
        throw new Error("useChat must be used within an ChatProvider");
    }
    return context;
};