import { Card } from "@models/gameModel";

export interface PlayerPayload {
    playerId: string;
}

export interface JoinGamePayload {
    playerId: string;
    deckId: string,
}
  
export interface RoomPayload {
    playerId: string;
    roomId: string;
}

export interface PlayCardPayload {
    playerId: string;
    roomId: string;
    cardId: string;
}

export interface AttackPlayerPayload {
    playerId: string;
    roomId: string;
    cardId: string;
    targetPlayerId: string;
}

export interface AttackPlayerCardPayload {
    playerId: string;
    roomId: string;
    cardId: string;
    targetPlayerId: string;
    targetCardId: string;
}

export interface ChatMessagePayload {
    roomId: string;
    sender: string;
    message: string;
}