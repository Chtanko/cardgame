'use client';

import { useTranslations } from "next-intl";
import { MainHeader, MarketPlaceHeader } from "@/components/HeaderComponent";
import "./inventory.css";

const InventoryPage = () => {
    const t = useTranslations('Global');

    const FAKE_DECK = [
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
    ]

    return(
        <div className="background-inventory">
            <MainHeader activate={4}/>
            <MarketPlaceHeader activate={1}/>   
            <main className="game-body-inventory">
                <div className="inventory-deck-display h-96 overflow-y-auto p-4 bg-gray-100 rounded-xl shadow-md">
                    <div className="grid grid-cols-9 gap-9">
                        {FAKE_DECK.map((card, id: number ) => (

                            <div key={id} className="card-display" style={{backgroundImage: `url(/cards/${card.id}.png)`}}>
                                
                            </div>
                            
                        ))}
                    </div>
                </div>
            </main>
        </div> 
    )
}

export default InventoryPage;