import { SoundEnum, useAudio } from "@/context/audioContext";
import { GameHandler, EndTurnHandler, MessageHandler, AttackCardHandler, AttackPlayerHandler, PlayCardHandler, ChatMessageHandler, EndGameHandler } from "@/models/handlerModel";
import { MessagePayload, NoMessagePayload, AttackPlayerCardPayload, AttackPlayerPayload, PlayCardPayload, ChatMessagePayload} from "@/models/payloadModel";


const defaultGameHandler = (message: MessagePayload, handler: GameHandler) => {     
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);
}

const noDeckHandler = (message: NoMessagePayload) => {
    
}

const endTurnToClientHandler = (message: MessagePayload, handler: EndTurnHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);

    const index = message.room.players.indexOf(handler.playerId);
    handler.setIsYourTurn(message.room.turn % 2 === index);

    handler.playSoundEffect(SoundEnum.ENDTURN);
}

const endTurnToRoomHandler = (message: MessagePayload, handler: EndTurnHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);

    const index = message.room.players.indexOf(handler.playerId);
    handler.setIsYourTurn(message.room.turn % 2 === index);

    handler.playSoundEffect(SoundEnum.ENDTURN);
}

const quitGameToClientHandler = (message: MessagePayload, handler: GameHandler) => {
    defaultGameHandler(message, handler);
}

const endGameToClientHandler = (message: MessagePayload, handler: EndGameHandler) => {
    handler.setRoom(message.room);
    handler.setEndGame(true);
}

const endGameToRoomHandler = (message: MessagePayload, handler: EndGameHandler) => {
    handler.setRoom(message.room);
    handler.setEndGame(true);
}

const cardAttackToClientHandler = (message: AttackPlayerCardPayload, handler: AttackCardHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);

    handler.playSoundEffect(SoundEnum.ATTACK_1);
}

const cardAttackToRoomHandler = (message: AttackPlayerCardPayload, handler: AttackCardHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);
    handler.playSoundEffect(SoundEnum.ATTACK_1);
}

const playerAttackToClientHandler = (message: AttackPlayerPayload, handler: AttackPlayerHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);
    handler.playSoundEffect(SoundEnum.ATTACK_0);
}

const playerAttackToRoomHandler = (message: AttackPlayerPayload, handler: AttackPlayerHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);
    handler.playSoundEffect(SoundEnum.ATTACK_0);
}

const playCardToClientHandler = (message: PlayCardPayload, handler: PlayCardHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);
    handler.playSoundEffect(SoundEnum.ARRIVAL_0);
}

const playCardToRoomHandler = (message: PlayCardPayload, handler: PlayCardHandler) => {
    handler.setRoom(message.room);

    const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

    handler.setPlayerTwoId(defaultPlayerTwoId);
    handler.playSoundEffect(SoundEnum.ARRIVAL_0);
}

const cardNotInBoardHandler = (message: NoMessagePayload, handler: MessageHandler) => {
    handler.setMessage("MESSAGE_CARD_NOT_IN_BOARD");
    handler.playSoundEffect(SoundEnum.ERROR);
}

const cardNotInTargetBoardHandler = (message: NoMessagePayload, handler: MessageHandler) => {
    handler.setMessage("MESSAGE_CARD_NOT_IN_TARGET_BOARD");
    handler.playSoundEffect(SoundEnum.ERROR);
}

const cardInTargetBoardHandler = (message: NoMessagePayload, handler: MessageHandler) => {
    handler.setMessage("MESSAGE_CARD_IN_TARGET_BOARD");
    handler.playSoundEffect(SoundEnum.ERROR);
}

const notEnoughManaHandler = (message: NoMessagePayload, handler: MessageHandler) => {
    handler.setMessage("MESSAGE_NOT_ENOUGH_MANA");
}

const cardNotInHandHandler = (message: NoMessagePayload, handler: MessageHandler) => {
    handler.setMessage("MESSAGE_CARD_NOT_IN_HAND");
    handler.playSoundEffect(SoundEnum.ERROR);
}

const cardHaveAlreadyPlayedToClientHandler = (message: NoMessagePayload, handler: MessageHandler) => {
    handler.setMessage("MESSAGE_CARD_HAVE_ALREADY_PLAYED");
    handler.playSoundEffect(SoundEnum.ERROR);
}

const notYourTurnHandler = (message: NoMessagePayload, handler: MessageHandler) => {
    handler.setMessage("MESSAGE_NOT_YOUR_TURN");
    handler.playSoundEffect(SoundEnum.ERROR);
}

const chatToClientHandler = (message: ChatMessagePayload, handler: ChatMessageHandler) => {
    handler.setChatMessage(message.message, message.sender);
    handler.playSoundEffect(SoundEnum.NOTIFICATION);
}

const chatToRoomHandler = (message: ChatMessagePayload, handler: ChatMessageHandler) => {
    handler.setChatMessage(message.message, message.sender);
}

const game = {
    noDeckHandler,
    endTurnToClientHandler,
    endTurnToRoomHandler,
    notYourTurnHandler,
    endGameToClientHandler,
    endGameToRoomHandler,
    cardNotInBoardHandler,
    cardNotInTargetBoardHandler,
    cardInTargetBoardHandler,
    cardAttackToClientHandler,
    cardAttackToRoomHandler,
    playerAttackToClientHandler,
    playerAttackToRoomHandler,
    playCardToClientHandler,
    playCardToRoomHandler,
    notEnoughManaHandler,
    cardNotInHandHandler,
    cardHaveAlreadyPlayedToClientHandler,
    quitGameToClientHandler,
    chatToClientHandler,
    chatToRoomHandler,
}

export default game;