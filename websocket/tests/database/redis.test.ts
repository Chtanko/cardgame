import Redis from "ioredis-mock"; // Utilise un mock de Redis pour éviter de se connecter à un vrai serveur
import redisClient from "../../src/database/redis";

jest.mock("../../src/database/redis", () => new Redis());

describe("Redis Client", () => {
    beforeAll(async () => {
        await redisClient.set("test-key", "test-value");
    });

    afterAll(async () => {
        await redisClient.quit();
    });

    it("should set and get a key-value pair", async () => {
        const value = await redisClient.get("test-key");
        expect(value).toBe("test-value");
    });

    it("should delete a key", async () => {
        await redisClient.del("test-key");
        const value = await redisClient.get("test-key");
        expect(value).toBeNull();
    });
});
