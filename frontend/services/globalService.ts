import { useState, useEffect } from 'react';

export const isValidUUID = (uuid: string) : boolean => {
    const uuidRegex = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/;
    return uuidRegex.test(uuid);
};

export function useHalfScreenWidth(): number {
  const [halfWidthVw, setHalfWidthVw] = useState(50); // 50vw par défaut

  useEffect(() => {
    const updateHalfWidth = () => {
      const vw = (window.innerWidth / 2 / window.innerWidth) * 100; // Convertir en vw
      setHalfWidthVw(vw);
    };

    updateHalfWidth(); // Initialiser au chargement
    window.addEventListener('resize', updateHalfWidth); // Mettre à jour au redimensionnement

    return () => {
      window.removeEventListener('resize', updateHalfWidth);
    };
  }, []);

  return halfWidthVw;
}