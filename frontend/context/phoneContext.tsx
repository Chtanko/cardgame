'use client';

import { ReactNode, useEffect, useState } from "react";
import { SoundEnum, useAudio } from "@/context/audioContext";

export const PhoneProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    const [isPortrait, setIsPortrait] = useState(false);
    const { playSoundEffect } = useAudio();
  

    useEffect(() => {
        const handleResize = () => {
            const size = window.innerHeight > window.innerWidth
            setIsPortrait(size);

            if(size) playSoundEffect(SoundEnum.ERROR);
        };

        window.addEventListener("resize", handleResize);
        handleResize(); // Vérifie au chargement

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    return (
        <>
            {isPortrait ? (
                <div className="landscape-warning-container">
                    <div className="landscape-warning">
                    </div>
                </div>
                ) : (<>
                    {children}
                    </>
                )
            }
        </>
    );
};