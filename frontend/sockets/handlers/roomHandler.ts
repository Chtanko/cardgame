import { RoomStatus } from "@/models/gameModel";
import { NoRoomHandler, RoomHandler, LoadGameHandler } from "@/models/handlerModel";
import { MessagePayload, NoMessagePayload, LoadGamePayload } from "@/models/payloadModel"

const defaultRoomHandler = (message: MessagePayload, handler: RoomHandler) => {
    if (message.room.status === RoomStatus.Full) {
        handler.setRoom(message.room);

        const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

        handler.setPlayerTwoId(defaultPlayerTwoId);
        const index = message.room.players.indexOf(handler.playerId);
        handler.setIsYourTurn(message.room.turn % 2 === index);
    } else {
        handler.router.replace("/lobby");
    }
}

const roomJoinedHandler = (message: MessagePayload, handler: RoomHandler) => {
    defaultRoomHandler(message, handler);
}


const playerJoinedHandler = (message: MessagePayload, handler: RoomHandler) => {
    defaultRoomHandler(message, handler);
}

const playerLeftHandler = (message: MessagePayload, handler: RoomHandler) => {
    //defaultRoomHandler(message, handler);
}

const roomRejoinedHandler = (message: MessagePayload, handler: RoomHandler) => {
    defaultRoomHandler(message, handler);
}

const playerRejoinedHandler = (message: MessagePayload, handler: RoomHandler) => {
    //defaultRoomHandler(message, handler);
}

const noRoomHandler = (message: NoMessagePayload, handler: NoRoomHandler) => {
    handler.router.replace("/lobby");
}

const loadGameHandler = (message: LoadGamePayload, handler: LoadGameHandler) => {
    if (message.room.status === RoomStatus.Full) {
        handler.setRoom(message.room);

        const defaultPlayerTwoId = message.room.players.filter(item => item !== handler.playerId)[0];

        handler.setPlayerTwoId(defaultPlayerTwoId);
        const index = message.room.players.indexOf(handler.playerId);
        handler.setIsYourTurn(message.room.turn % 2 === index);

        handler.setLoadChatMessage(message.chat);
    } else {
        handler.router.replace("/lobby");
    }
}

const room = {
    roomJoinedHandler,
    playerJoinedHandler,
    playerLeftHandler,
    roomRejoinedHandler,
    playerRejoinedHandler,
    noRoomHandler,
    loadGameHandler
}

export default room;