"use client";
import * as THREE from "three";
import { Canvas, useFrame, useThree } from "@react-three/fiber";
import { Dispatch, SetStateAction, Suspense, useRef, useState } from "react";
import { OrbitControls, useTexture } from "@react-three/drei";
import { BufferGeometry, Mesh, NormalBufferAttributes } from "three";
import { EffectComposer, Bloom } from "@react-three/postprocessing";


type PlanetProps = {
    texturePath: string;
    position: [number, number, number];
    size: [number, number, number];
    rotationSpeed: number;
    setShowUI: Dispatch<SetStateAction<boolean>>;
}

function Planet({texturePath, position, size, setShowUI, rotationSpeed}: PlanetProps) {
    const texture = useTexture(texturePath); 
    const sphereRef = useRef<Mesh<BufferGeometry<NormalBufferAttributes>>>(null); 
    const [hovered, setHovered] = useState(false);

    const { camera } = useThree();
    const [clicked, setClicked] = useState(false);
    const zoomSpeed = 0.05; 

    useFrame(() => {
      if (sphereRef.current) {
        sphereRef.current.rotation.y += rotationSpeed; // Ajuste la vitesse ici
      }

      if (clicked && sphereRef.current) {
        const targetPosition = new THREE.Vector3(5, 20, 0); // Position cible de la caméra
        camera.position.lerp(targetPosition, zoomSpeed);

        // Vérifie si la caméra est proche de la cible
        if (camera.position.distanceTo(targetPosition) < 0.1) {
            setClicked(false);
            setShowUI(true); // Affiche la div après le focus
        }
      }
    });

    return (
        <mesh ref={sphereRef}
            onPointerOver={(e) => {
                e.stopPropagation();
                setHovered(true);
                console.log('light');
            }}
            onPointerOut={(e) => {
                e.stopPropagation();
                setHovered(false);
                console.log('unlight');
            }}
            onClick={() => setClicked(true)}
            position={position}
            >
            <sphereGeometry args={size}/>
            <meshStandardMaterial map={texture} 
                emissive={hovered ? new THREE.Color(0x4488ff) : new THREE.Color(0x000000)} 
                emissiveIntensity={hovered ? 1 : 0} 
                transparent={false} 
            />
        </mesh>
    );
  }

export function Scene() {
    const [showUI, setShowUI] = useState(false);

  return (
        <>
            <Canvas camera={{ position: [5, 20, 0], fov: 60}} >
                <Suspense fallback={null}>
                    <ambientLight intensity={0.5} />
                    <directionalLight intensity={7} position={[2, 2, 2]} />
                    <Planet texturePath={"/planets/moon_0.png"} position={[1,1,1]} size={[1, 32, 32]} setShowUI={setShowUI} rotationSpeed={0.001}/>
                    <Planet texturePath={"/planets/earth.jpg"} position={[4,4,4]} size={[2, 128, 128]} setShowUI={setShowUI} rotationSpeed={0.0005}/>
                    {!showUI && <OrbitControls />}
                    <EffectComposer>
                        <Bloom intensity={1} luminanceThreshold={0.2} luminanceSmoothing={0.5} />
                    </EffectComposer>     
                </Suspense>
            </Canvas>
            {showUI && (
                <div
                style={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    background: "rgba(0,0,0,0.8)",
                    padding: "20px",
                    color: "white",
                    borderRadius: "10px",
                }}
                >
                <h2>Informations sur la planète</h2>
                <p>Ceci est une planète spéciale.</p>
                <button onClick={() => setShowUI(false)}>Fermer</button>
                </div>
            )}
        </>
    );
}