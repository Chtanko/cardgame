import { useEffect, useState } from "react";
import { useParams, useNavigate } from 'react-router-dom';
import { useSocket } from '../context/socketContext';
import { gameEvents } from '../sockets/events/game'
import backgroundImage from '../assets/background/landing.png';


const Lobby = () => {
    const { socket, isConnected } = useSocket();
    const [roomStatus, setRoomStatus] = useState("empty");
    const navigate = useNavigate();

    const handlers = {
        roomJoinedHandler: (message) => {
            setRoomStatus(message.room.status); 
            if (message.room.status === "full") {
                navigate(`/duel/${message.roomId}`);
            }
        },
    };

    useEffect(() => {
        if (!socket) return;
        const { cleanup } = gameEvents(socket, handlers);

        return () => {
            cleanup();
        };
    }, [socket]);

    useEffect(() => {
        if(!isConnected) return;
        socket.emit('playerIsInGameHandler', {"playerId": "22222222-2222-2222-2222-222222222222"});
    }, [isConnected]);
    
    const handleJoinGame = () => {
      if (isConnected && socket) {
        const send = {
            "playerId": "22222222-2222-2222-2222-222222222222",
            "deckId": "11111111-1111-1111-1111-111111111111"
        }
  
        // Envoyer le message 'join game' au serveur
        socket.emit('joinGame', send);
        console.log('Join game message sent:', send);
      } else {
        console.error('Socket not connected!');
      }
    };

    const containerStyle = {
        backgroundImage: `url(${backgroundImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: "cover",
        height: "100vh",
        color: "white"
    };

    return(
        <div style={containerStyle}>
            <p>
                realm factions
            </p>
            <h1>Lobby</h1>
            {roomStatus === "empty" && (
                <button onClick={handleJoinGame}>
                    join battle
                </button>
            )}
            {roomStatus === "incomplete" && (
                <p>Incomplete</p>
            )}
            {roomStatus === "full" && (
                <p>Complete</p>
            )}
        </div>
    )
}

export default Lobby;