'use client';

import { useMenu } from "@/context/menuContext";
import { useTranslations } from "next-intl";
import { ChangeEvent, JSX, useEffect, useRef, useState } from "react";

import "./settings.css";
import { useSettings } from "@/context/settingsContext";


export const Settings = () => {
    const t = useTranslations('Global');
    const {openMenu, openedSettings} = useMenu();
    
    const [Page, setPage] = useState(() => InterfaceSettings)

    return(
        <>  
            <div className={ openedSettings ? "settings-menu-container" : "settings-menu-close"}>
                <div className='settings-rigth-menu'>
                    <h1>{t("SETTINGS")}</h1>
                    <hr />
                    <br/>
                    <button onClick={() => setPage(() =>InterfaceSettings)}>Interface</button>
                    <button onClick={() => setPage(() =>SoundSettings)}>Sound</button>
                    <button onClick={() => setPage(() =>GameplaySettings)}>{t("GAMEPLAY")}</button>
                    <button onClick={openMenu}>{t("BACK")}</button>
                </div>
                <div className='settings-left-menu'>
                    <Page/>
                </div>
            </div>
        </>
    );
}

const InterfaceSettings = () => {
    const t = useTranslations('Settings');
    return(
        <>
            <h1>Interface</h1>
        </>
    );
}

const SoundSettings = () => {
    const t = useTranslations('Settings');
    const { masterVolume,
    musicVolume,
    ambiantVolume,
    interfaceVolume,
    effectVolume,
    voiceVolume,
    updateMasterVolume,
    updateMusicVolume,
    updateAmbiantVolume,
    updateInterfaceVolume,
    updateEffectVolume,
    updateVoiceVolume} = useSettings();

    const sliderRefMaster = useRef<HTMLInputElement | null>(null);
    const sliderRefMusic = useRef<HTMLInputElement | null>(null);
    const sliderRefAmbiant = useRef<HTMLInputElement | null>(null);
    const sliderRefInterface = useRef<HTMLInputElement | null>(null);
    const sliderRefEffect = useRef<HTMLInputElement | null>(null);
    const sliderRefVoice = useRef<HTMLInputElement | null>(null);

    const handleChange = (event: ChangeEvent<HTMLInputElement>, updateValue: (volume: number) => void) => {
        updateValue(parseFloat(event.target.value));
        event.target.style.setProperty("--progress", `${parseFloat(event.target.value) * 100}%`);
    };

    useEffect(() => {
        if (sliderRefMaster.current) {
            sliderRefMaster.current.style.setProperty("--progress", `${masterVolume * 100}%`);
        }
        if (sliderRefMusic.current) {
            sliderRefMusic.current.style.setProperty("--progress", `${musicVolume * 100}%`);
        }
        if (sliderRefAmbiant.current) {
            sliderRefAmbiant.current.style.setProperty("--progress", `${ambiantVolume * 100}%`);
        }
        if (sliderRefInterface.current) {
            sliderRefInterface.current.style.setProperty("--progress", `${interfaceVolume * 100}%`);
        }
        if (sliderRefEffect.current) {
            sliderRefEffect.current.style.setProperty("--progress", `${effectVolume * 100}%`);
        }
        if (sliderRefVoice.current) {
            sliderRefVoice.current.style.setProperty("--progress", `${voiceVolume * 100}%`);
        }
    }, []);

    return(
        <>
            <h1>Sounds</h1>
            <hr />
            <br/>
            <div className="sound-container">
                <p>{t("MASTER_VOLUME")}</p>
                <div className="sound-bar">
                    <input type="range"
                        ref={sliderRefMaster}
                        min="0"
                        max="1"
                        step="0.01"
                        value={masterVolume}
                        onChange={(e) => handleChange(e, updateMasterVolume)}/>
                    <p>{Math.round((masterVolume + Number.EPSILON) * 100)}%</p>
                </div>
                <p>{t("MUSIC_VOLUME")}</p>
                <div className="sound-bar">
                    <input type="range"
                        ref={sliderRefMusic}
                        min="0"
                        max="1"
                        step="0.01"
                        value={musicVolume}
                        onChange={(e) => handleChange(e, updateMusicVolume)}/>
                    <p>{Math.round((musicVolume + Number.EPSILON) * 100)}%</p>
                </div>
                <p>{t("EFFECT_VOLUME")}</p>
                <div className="sound-bar">
                    <input type="range"
                        ref={sliderRefEffect}
                        min="0"
                        max="1"
                        step="0.01"
                        value={effectVolume}
                        onChange={(e) => handleChange(e, updateEffectVolume)}/>
                    <p>{Math.round((effectVolume + Number.EPSILON) * 100)}%</p>
                </div>

                <p>{t("INTERFACE_VOLUME")}</p>
                <div className="sound-bar">
                    <input type="range"
                        ref={sliderRefInterface}
                        min="0"
                        max="1"
                        step="0.01"
                        value={interfaceVolume}
                        onChange={(e) => handleChange(e, updateInterfaceVolume)}/>
                    <p>{Math.round((interfaceVolume + Number.EPSILON) * 100)}%</p>
                </div>

                <p>{t("VOICE_VOLUME")}</p>
                <div className="sound-bar">
                    <input type="range"
                        ref={sliderRefVoice}
                        min="0"
                        max="1"
                        step="0.01"
                        value={voiceVolume}
                        onChange={(e) => handleChange(e, updateVoiceVolume)}/>
                    <p>{Math.round((voiceVolume + Number.EPSILON) * 100)}%</p>
                </div>
                <p>{t("AMBIANCE_VOLUME")}</p>
                <div className="sound-bar">
                    <input type="range"
                        ref={sliderRefAmbiant}
                        min="0"
                        max="1"
                        step="0.01"
                        value={ambiantVolume}
                        onChange={(e) => handleChange(e, updateAmbiantVolume)}/>
                    <p>{Math.round((ambiantVolume + Number.EPSILON) * 100)}%</p>
                </div>
            </div>
        </>
    );
}

const GameplaySettings = () => {
    const t = useTranslations('Settings');
    return(
        <>
            <h1>{t("GAMEPLAY")}</h1>
        </>
    );
}