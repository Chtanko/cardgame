export type SlideButtonType = {
    height: number;
    width: number;
    button: SlideButtonDefinition[];
}

export type SlideButtonDefinition = {
    action: (idx: number) => any;
    text: string;
}