'use client';

import { useTranslations } from "next-intl";
import { MainHeader, MarketPlaceHeader } from "@/components/HeaderComponent";
import "./marketplace.css";


const MarketPlacePage = () => {
    const t = useTranslations('Global');


    return(
        <div className="background-marketplace">
            <MainHeader activate={4}/>
            <MarketPlaceHeader activate={2}/>   
            <main className="game-body-marketplace">
 
            </main>
        </div> 
    )
}

export default MarketPlacePage;