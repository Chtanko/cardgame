import { Socket } from "socket.io";
import { JoinGamePayload, RoomPayload, PlayCardPayload, AttackPlayerPayload, AttackPlayerCardPayload, PlayerPayload, ChatMessagePayload } from '@models/payloadModel';
import Redis from "ioredis";
import { loadGame, playerIsInGameHandler, joinGameHandler, playCardHandler, 
  endTurnHandler, attackPlayerHandler, attackCardHandler, leaveGameHandler,
  chatHandler, joinDemoGameHandler}from "@sockets/handlers/gameHandler";
import { HandlerMessageToServer} from "@models/handlerModel";

const gameEvents = (socket: Socket, redisClient: Redis, pool: any): void => {
  console.log("New connection to /game");

  socket.on(HandlerMessageToServer.PlayerIsInGame, async(payload: PlayerPayload) => {
    playerIsInGameHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.LoadGame, async(payload: RoomPayload) => {
    loadGame(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.JoinGame, async (payload: JoinGamePayload) => {
    joinGameHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.JoinDemoGame, async (payload: PlayerPayload) => {
    joinDemoGameHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.JoinDemoAIGame, async (payload: PlayerPayload) => {
    joinDemoGameHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.PlayCard, async(payload: PlayCardPayload) => {
    playCardHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.AttackPlayer, async(payload: AttackPlayerPayload) => {
    attackPlayerHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.AttackCard, async(payload: AttackPlayerCardPayload) => {
    attackCardHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.EndTurn, async(payload: RoomPayload) => {
    endTurnHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.LeaveGame, async(payload: RoomPayload) => {
    leaveGameHandler(socket, redisClient, pool, payload);
  });

  socket.on(HandlerMessageToServer.Chat, async(payload: ChatMessagePayload) => {
    chatHandler(socket, redisClient, pool, payload);
  });

  // Gestion de la déconnexion
  socket.on(HandlerMessageToServer.Disconnect, () => {
    console.log("User disconnected from /game");
  });
};

export default gameEvents;
