import React, {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';
import CustomButton from './CustomButton';
import { useGlobalContext } from '../context';
import { player01, player02, grid } from '../assets';
import styles from '../styles';
import gradient from '../assets/gradient.png';

const GameLoad = () => {
    const{ walletAddress } = useGlobalContext();
    const navigate = useNavigate();

    const headTexts = [
        ["The battle is about to begin...", "Ready yourself!"],
        ["Opponents are rallying…", "Prepare for glory!"],
        ["Destiny awaits…", "Your rival is on their way!"],
        ["Champions rise...", "Will you answer the call?"],
        ["Your legacy starts here…", "Claim your place in history!"],
        ["Courage fuels the brave…", "The arena awaits your presence!"],
        ["A clash of titans...", "Prepare to prove your might!"],
        ["Every move matters...", "Strategy is the key to victory!"],
        ["The battlefield is alive…", "Step in and seize your moment!"],
        ["Glory or defeat...", "The choice is yours to make!"]
    ];

    const proTips = [
        "Quick tip: Explore your battleground to gain the edge!",
        "While you wait, visualize your victory. Every move counts!",
        "Legendary warriors take their time. Use it wisely.",
        "Fine-tune your strategy. Champions are born in the waiting.",
        "Rival not detected yet… scanning the multiverse!",
        "The challenger is assembling their forces…",
        "Searching the arena for worthy competition…",
        "Protip: Review your skills and master your combos!",
        "Did you know? A change of battleground can change the game.",
        "Success favors the prepared. Ready your tactics while you wait!"
    ];

    const opponentPhrase = [
        "FINDING A RIVAL...",
        "OPPONENT CONNECTING...",
        "MATCHMAKING...",
        "SEARCHING FOR RIVAL...",
        "PREPARING OPPONENT...",
        "SCANNING FOR FOES...",
        "RIVAL IS NEAR...",
        "OPPONENT READYING...",
        "LOOKING FOR CHALLENGE...",
        "WAITING FOR A RIVAL..."
    ];

    const [randomHeadText, setRandomHeadText] = useState("");
    const [randomProTip, setRandomProTip] = useState("");
    const [randomOpponentPhrase, setOpponentPhrase] = useState("");


    const getRandomPhrase = (array) => {
        const index = Math.floor(Math.random() * array.length);
        return array[index];
    }

    useEffect(() => {
        const newHeadText = getRandomPhrase(headTexts);
        const newProTip = getRandomPhrase(proTips);
        const newOpponentPhrase = getRandomPhrase(opponentPhrase);

        setRandomHeadText(newHeadText);
        setRandomProTip(newProTip);
        setOpponentPhrase(newOpponentPhrase);

        const interval = setInterval(() => {
            setRandomHeadText(getRandomPhrase(HeadText));
            setRandomProTip(getRandomPhrase(wProTip));
            setOpponentPhrase(getRandomPhrase(oppoPhrase));
        }, 5000);

        return () => clearInterval(interval);
    }, []);

  return (
    <div className= {`${styles.flexBetween} ${styles.gameLoadContainer}`}>

        <div className="absolute inset-0 -z-10">
            <img
                src={gradient}
                alt="Gradient effect"
                className="w-[200%] h-[200%] object-cover opacity-70"
            />
        </div>

        <div className="absolute inset-0 -z-10">
            <img
                src={grid}
                alt="Grid background"
                className="w-full h-full object-cover opacity-30"
            />
        </div>
        <div className={styles.gameLoadBtnBox}>
            <CustomButton
                title="Choose Battleground"
                handleClick={() => navigate('/battleground')}
                restStyles="mt-6"
            />
        </div>
        <div className={`flex-1 ${styles.flexCenter} flex-col`}>
            <h1 className={`${styles.headText} text-center`}>
                {randomHeadText[0]} <br /> {randomHeadText[1]}
            </h1>
            <p className={styles.gameLoadText}>
                {randomProTip}
            </p>

            <div className={styles.gameLoadPlayersBox}>
                <div className={`${styles.flexCenter} flex-col`}>
                    <img src={player01} className={styles.gameLoadPlayerImg}/>
                    <p className= {styles.gameLoadPlayerText}>
                        {walletAddress.slice(0, 30)}
                    </p>
                </div>

                <h2 className={styles.gameLoadVS}>VS</h2>

                <div className={`${styles.flexCenter} flex-col`}>
                    <img src={player02} className={styles.gameLoadPlayerImg}/>
                    <p className= {styles.gameLoadPlayerText}>
                        {randomOpponentPhrase}
                    </p>
                </div>
            </div>
        </div>
    </div>
  )
}
export default GameLoad