'use client';

import { useEffect, useState } from "react";
import { useSocket } from '@/context/socketContext';
import { gameEvents } from '@/sockets/events/game';

import {CardComponent, CardComponentEnemy, CardComponentBoard, CardComponentBoardEnnemy} from "@/components/CardComponent";
import { Card, Room } from "@/models/gameModel";
import { MAX_LIFE, MAX_MANA, PLAYER_TWO_ID } from "@/models/constant";
import { useRouter } from '@/i18n/routing';
import { useParams } from "next/navigation";
import { GameHandlers, HandlerMessageToServer } from "@/models/handlerModel";
import { isValidUUID} from "@/services/globalService";
import { useTranslations } from "next-intl";

import pl1 from "@/public/character/player01.png";
import pl2 from "@/public/character/player02.png";

import { useAccount } from "wagmi";
import { MusicEnum, SoundEnum, useAudio } from "@/context/audioContext";
import "@/app/[locale]/duel/[roomId]/duel.css";
import { GameChat } from "@/components/ChatComponent";
import { useChat } from "@/context/chatContext";
import { Menu } from "@/components/MenuComponent";
import { useMenu } from "@/context/menuContext";

const progressBar = (percentage: number) => ({
  width: `${percentage}%`,
});

const Duel = () => {
    const t = useTranslations('Game');
    const router = useRouter();
    const { roomId} = useParams() as {roomId: string};
    //redirect();
    const { socket, isConnected } = useSocket();

    const [room, setRoom] = useState<Room>();
    const account = useAccount(); 
    const playerId= account.address;

    const { playSoundEffect, playMusic } = useAudio();
    const {openedChat} = useMenu();

    if(playerId === undefined) router.replace("/multiplayer/demo");

    const [isYourTurn, setIsYourTurn] = useState(true);
    const [showIndicator, setShowIndicator] = useState(false);
    const [message, setMessage] = useState<string>("DEBUG");
    const [showIndicatorEnding, setShowIndicatorEnding] = useState(false);
    const [youWin, setYouWin] = useState(false);
    const [endGame, setEndGame] = useState(false);
    //const playerId = PLAYER_ONE_ID;
    const [playerTwoId, setPlayerTwoId] = useState(PLAYER_TWO_ID);

    const [cardSelected, setCardSelected] = useState<Card | undefined>(undefined);

    const { setLoadChatMessage, setChatMessage, setChat } = useChat();

    const setWarningMessage = (msg: string, soundEffect: SoundEnum) => {
      playSoundEffect(soundEffect);
      setMessage(msg);
    }

    useEffect(() => {
      if(playerId === undefined) {
        return;
      }
      if(endGame === false){ 
        return;
      }
      if(room === undefined){ 
        return;
      }
      setShowIndicatorEnding(true);
      setYouWin(room.playerStates[playerId].life > 0);

      const timer = setTimeout(() => {
        router.replace("/multiplayer/demo");
      }, 5000);

      return () => clearTimeout(timer);
    }, [endGame])


    useEffect(() => {
      if(endGame === false){ 
        return;
      }
      playMusic(MusicEnum.ENDGAME);
      if(youWin){
        playSoundEffect(SoundEnum.VICTORY);
      }else{
        playSoundEffect(SoundEnum.DEFEAT);
      }
    }, [youWin]);

    const handlers : GameHandlers = {
      endGameHandler: {
        "setRoom": setRoom,
        "setEndGame": setEndGame
      },
      loadGameHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "setIsYourTurn": setIsYourTurn,
        "playerId": playerId|| "",
        "router": router,
        "setLoadChatMessage": setLoadChatMessage
      },
      roomHandler : {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "setIsYourTurn": setIsYourTurn,
        "playerId": playerId || "",
        "router": router
      },
      noRoomHandler: {
        "router": router
      },
      gameHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "playerId": playerId || ""
      },
      endTurnHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "setIsYourTurn": setIsYourTurn,
        "playerId": playerId || "",
        "playSoundEffect": playSoundEffect
      },
      attackCardToClientHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "playerId": playerId || "",
        "playSoundEffect": playSoundEffect
      },
      attackCardToRoomHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "playerId": playerId || "",
        "playSoundEffect": playSoundEffect
      },
      attackPlayerToClientHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "playerId": playerId || "",
        "playSoundEffect": playSoundEffect
      },
      attackPlayerToRoomHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "playerId": playerId || "",
        "playSoundEffect": playSoundEffect
      },
      playCardToClientHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "playerId": playerId || "",
        "playSoundEffect": playSoundEffect
      },
      playCardToRoomHandler: {
        "setRoom": setRoom,
        "setPlayerTwoId": setPlayerTwoId,
        "playerId": playerId || "",
        "playSoundEffect": playSoundEffect
      },
      notEnoughManaHandler: {
        "setMessage": setMessage,
        "playSoundEffect": playSoundEffect
      },
      notYourTurnHandler: {
        "setMessage": setMessage,
        "playSoundEffect": playSoundEffect
      },
      cardNotInBoardHandler: {
        "setMessage": setMessage,
        "playSoundEffect": playSoundEffect
      },
      cardHaveAlreadyPlayedHandler: {
        "setMessage": setMessage,
        "playSoundEffect": playSoundEffect
      },
      cardInTargetBoardHandler: {
        "setMessage": setMessage,
        "playSoundEffect": playSoundEffect
      },
      cardNotInHandHandler: {
        "setMessage": setMessage,
        "playSoundEffect": playSoundEffect
      },
      cardNotInTargetBoardHandler: {
        "setMessage": setMessage,
        "playSoundEffect": playSoundEffect
      },
      chatToClientHandler: {
        "setChatMessage": setChatMessage,
        "playSoundEffect": playSoundEffect
      },
      chatToRoomHandler:{
        "setChatMessage": setChatMessage,
        "playSoundEffect": playSoundEffect
      }
    };
      
    useEffect(() => {
        if (!socket) return;
        
        const {cleanup} = gameEvents(socket, handlers);
        return () => {
            cleanup();
        };
    }, [socket]);

    useEffect(() => {
        if(!isConnected) return;
        if(socket === null) return; //faire la gestion d'erreur
        if(!isValidUUID(roomId)) {
          router.replace("/lobby");
          console.log(roomId)
            return;
        }
        socket.emit(HandlerMessageToServer.LoadGame, {"playerId": playerId, "roomId":roomId});

    }, [isConnected, roomId]);

    const placeCard = (card: Card) => {
      if(playerId === undefined) return false;
      if(room === undefined) return false; //faire la gestion d'erreur
      if(socket === null) return false; //faire la gestion d'erreur
      if(isYourTurn === false) {
        setWarningMessage("MESSAGE_NOT_YOUR_TURN", SoundEnum.ERROR);
        return false;
      }
      if(room.playerStates[playerId].mana < card.mana){
        console.log(`mana: ${room.playerStates[playerId].mana}, card: ${card.mana}`);
        setWarningMessage("MESSAGE_NOT_ENOUGH_MANA", SoundEnum.ERROR);
        return false;
      }
      socket.emit(HandlerMessageToServer.PlayCard, {"playerId": playerId, "roomId":roomId, "cardId": card.id});
      return true;
    }

    const endTurn = () => {
      if(socket === null) return; //faire la gestion d'erreur
      socket.emit(HandlerMessageToServer.EndTurn, {"playerId": playerId, "roomId":roomId});
    }

    const attackCard = (targetCard: Card) => {
      if(socket === null) return; //faire la gestion d'erreur
      if(isYourTurn === false) {
        setWarningMessage("MESSAGE_NOT_YOUR_TURN", SoundEnum.ERROR);
        return;
      }
      if(targetCard === undefined){
        setWarningMessage("MESSAGE_CARD_NOT_IN_TARGET_BOARD", SoundEnum.ERROR);
        return;
      }
      if(cardSelected === undefined){
        setWarningMessage("MESSAGE_CARD_NOT_IN_BOARD", SoundEnum.ERROR);
        return;
      }
      if(cardSelected.havePlayed === true) {
        setWarningMessage("MESSAGE_CARD_HAVE_ALREADY_PLAYED", SoundEnum.ERROR);
        return;
      }
      socket.emit(HandlerMessageToServer.AttackCard, {"playerId" : playerId,"roomId": roomId, "cardId":cardSelected.id, "targetPlayerId": playerTwoId, "targetCardId": targetCard.id});
      setCardSelected(undefined);
    }

    const attackPlayer = () => {
      if(socket === null) return; //faire la gestion d'erreur
      if(isYourTurn === false) {
        setWarningMessage("MESSAGE_NOT_YOUR_TURN", SoundEnum.ERROR);
        return;
      }
      if(cardSelected === undefined){
        setWarningMessage("MESSAGE_CARD_NOT_IN_BOARD", SoundEnum.ERROR);
        return;
      }
      if(cardSelected.havePlayed === true) {
        setWarningMessage("MESSAGE_CARD_HAVE_ALREADY_PLAYED", SoundEnum.ERROR);
        return;
      }
      if(room === undefined){
        setWarningMessage("MESSAGE_ERROR", SoundEnum.ERROR);
        return;
      }
      if(room.playerStates[playerTwoId].board.length > 0) {
        setWarningMessage("MESSAGE_CARD_IN_TARGET_BOARD", SoundEnum.ERROR);
        return;
      }
      socket.emit(HandlerMessageToServer.AttackPlayer, {"playerId" : playerId,"roomId": roomId, "cardId":cardSelected.id, "targetPlayerId": playerTwoId });
      setCardSelected(undefined);
    }

    const selectCard = (card: Card, setIsClicked:React.Dispatch<React.SetStateAction<boolean>>, havePlayed:boolean) => {
      if(isYourTurn === false) {
        setWarningMessage("MESSAGE_NOT_YOUR_TURN", SoundEnum.ERROR);
        return;
      }
      if(havePlayed) {
        setWarningMessage("MESSAGE_CARD_HAVE_ALREADY_PLAYED", SoundEnum.ERROR);
        return;
      }
      if(card === undefined) return; //faire la gestion d'erreur
      if(card === cardSelected) {
        setIsClicked(false);
        setCardSelected(undefined);
        playSoundEffect(SoundEnum.CARD_CANCEL);
        return
      };
      setIsClicked(true);
      setCardSelected(card);
      playSoundEffect(SoundEnum.CARD_TAKE);
    }

    useEffect(() => {
      setShowIndicator(true);

      // Masquer l'indicateur après 3 secondes (ou un autre délai)
      const timer = setTimeout(() => {
        setShowIndicator(false);
      }, 3000);

      return () => clearTimeout(timer);
    }, [isYourTurn]);

    useEffect(() => {
      if(message === "" || message === "DEBUG") return;
      setChat(prevChat => [...prevChat, {sender: "system", message: t(message), color: 'red'}]);
      setMessage("DEBUG");
    }, [message]);

    return (
      <div className="game-container-style">
        {
          room === undefined ? (
            <></>
          ) : (
        <>
          <div className={`turn-indicator-container ${showIndicator ? "show" : ""}`}>
            <h1 className="turn-indicator text-4xl font-bold mb-4">
              {t("TURN")}{" "}{room.turn + 1}
            </h1>
            { isYourTurn ? (
                <h2 className="turn-indicator text-2xl font-medium">
                {t("YOUR_TURN")}
                </h2>
              ) : (
                <h2 className="turn-indicator text-2xl font-medium">
                {t("ENEMY_TURN")}
                </h2>
              )
            }
          </div>
          <div className={`turn-indicator-container ${showIndicatorEnding ? "show" : ""}`}>
            <h1 className="turn-indicator text-4xl font-bold mb-4">
              {t("END_GAME")}{" "}
            </h1>
            { youWin ? (
                <h2 className="indicator-victory text-2xl font-medium">
                {t("VICTORY")}
                </h2>
              ) : (
                <h2 className="indicator-defeat text-2xl font-medium">
                {t("DEFEAT")}
                </h2>
              )
            }
          </div>
        <Menu/>

        {/* Barre bleue : Cartes en main */}
        <div id="hand_2" className="hand-container-1">
          <div className="hand-style-2-0">
            <div className="progress-bar-container">
              <div
                className="lifebar" 
                style={progressBar(room.playerStates[playerTwoId].life * 100 / MAX_LIFE)}>
                {room.playerStates[playerTwoId].life}/{MAX_LIFE}
              </div>
            </div>
            <div className="progress-bar-container">
              <div 
                className="manabar" 
                style={progressBar(room.playerStates[playerTwoId].mana * 100 / MAX_MANA)}>
                {room.playerStates[playerTwoId].mana}/{MAX_MANA}
              </div>
            </div>
          </div>
          <div id="hand" className="hand-style-2-1">
            { Array.from({ length: room.playerStates[playerTwoId].handLength }).map((_, index) => (
              <CardComponentEnemy key={index}/>
            ))}
            </div>
            <div className="hand-style-2-2">
              <div className="player-style" style={{backgroundImage: `url(${pl2.src})`}} onClick={() => attackPlayer()}/>
            </div>
        </div>

        {/* Barre rouge : Cartes sur le board */}
        <div className="board-container">
          <div className="board-style-0"/>
          <div className="board-style-1">
              {room.playerStates[playerTwoId].board.map((card, index) => (
                <CardComponentBoardEnnemy key={index} card={card} attackCard={attackCard}/>
              ))}
          </div>
          <div className="board-style-0"/>
        </div>
        <div className="end-style">
          <div className="end-turn-style-2">
          {
            isYourTurn && (
              <button className="end-turn-style" onClick={endTurn}>{t("END_TURN")}</button>
            )
          }
          </div>
        </div>
        {/* Deuxième barre rouge : Éventuellement pour autre chose */}
        <div className="board-container">
          <div className="board-style-0"/>
          <div className="board-style-1">
              {room.playerStates[playerId || ""].board.map((card, index) => (
                <CardComponentBoard key={index} card={card} selectCard={selectCard}/>
              ))}
            </div>
          <div className="board-style-0"/>
        </div>

        {/* Deuxième barre bleue : Cartes en main (seconde zone) */}
        <div className="hand-container-2">
          <div className="hand-style-2-0">
            {
              socket && (
                <GameChat open={openedChat} socket={socket} roomId={roomId} playerId={playerId || ""}/>
              )
            }            <div className="progress-bar-container">
              <div 
                className="lifebar" 
                style={progressBar(room.playerStates[playerId|| ""].life * 100 / 30)}>
                {room.playerStates[playerId|| ""].life}/{MAX_LIFE}
                </div>
            </div>
            <div className="progress-bar-container">
              <div 
                className="manabar" 
                style={progressBar(room.playerStates[playerId|| ""].mana * 100 / 10)}>
                {room.playerStates[playerId|| ""].mana}/{MAX_MANA}
              </div>
            </div>
          </div>
          <div className="hand-style-2-1">
              {room.playerStates[playerId || ""].hand.map((card, index) => (
                  <CardComponent key={index} card={card} 
                  totalMana={room.playerStates[playerId|| ""].mana}
                  placeCard={placeCard}/>
              ))}
          </div>
          <div className="hand-style-2-2">
            <div className="player-style" style={{backgroundImage: `url(${pl1.src})`}}/>
          </div>
        </div>
        </>
        )
      }
    </div>
    );
}

export default Duel;