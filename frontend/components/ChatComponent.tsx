import { HandlerMessageToServer } from "@/models/handlerModel";
import { useEffect, useRef, useState } from "react";
import { Socket } from "socket.io-client";
import { useChat } from "@/context/chatContext";
import {IconSend2} from '@tabler/icons-react';

import "./chat.css";


type GameChatProps = {
    open: boolean;
    socket: Socket;
    playerId: string;
    roomId: string
}

export const GameChat = ({open, socket, playerId, roomId}: GameChatProps) => {
    const { chat } = useChat();
    const scrollableDivRef = useRef<HTMLDivElement | null>(null);

    const [chatInput, setChatInput] = useState<string>("");

    useEffect(() => {
        if (scrollableDivRef.current) {
            scrollableDivRef.current.scrollTop = scrollableDivRef.current.scrollHeight;
        }
    }, [chat]);

    const handleChange = (event: { target: { value: any; }; }) => {
        const value = event.target.value;
        setChatInput(value);
    };

    function sendChat() {
        if(chatInput === "") return;
        if(socket === null) return; //faire la gestion d'erreur

        socket.emit(HandlerMessageToServer.Chat, {"roomId": roomId, "sender": playerId, "message": chatInput});
        setChatInput("");
        if (scrollableDivRef.current) {
            scrollableDivRef.current.scrollTop = scrollableDivRef.current.scrollHeight;
        }
    }

    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
          event.preventDefault(); 
          sendChat();
        }
      };
    

    return (
        <div className={`${open ? "chat-container-display" : "chat-container"}`}>
            <div className="chat-message" ref={scrollableDivRef} >
            {
                chat.map((c, id) => (
                <div key={id}>
                    <p style={{display: "inline-block"}}>{c.sender}: </p>
                    <p style={{color: c.color, display: "inline-block"}}>{c.message}</p>
                </div>
                ))
            }
            </div>
            <div className="chat-send">
                <input className="chat-input" onKeyDown={handleKeyDown} onChange={handleChange} value={chatInput}></input>
                <button className="chat-button" onClick={() => sendChat()}><IconSend2/></button>
            </div>
        </div>
    );
}