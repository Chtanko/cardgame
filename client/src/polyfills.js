import { Buffer } from 'buffer';

window.global = window;
window.Buffer = Buffer;

if (typeof process === 'undefined') {
    window.process = {
        env: {},
        nextTick: (callback, ...args) => {
            Promise.resolve().then(() => callback(...args));
        },
    };
}
