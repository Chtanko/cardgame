import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { CustomButton, CustomInput, GameLoad, PageHOC } from '../components';
import { useGlobalContext } from '../context';
import styles from '../styles';


const CreateBattle = () => {
  const { contract, battleName, setBattleName, gameData } = useGlobalContext();
  const [waitBattle, setWaitBattle] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const navigate = useNavigate();

  useEffect(() => {

    if(gameData?.activeBattle?.battleStatus === 1){
    navigate(`/battle/${gameData.activeBattle.name}`)
    } else if(gameData?.activeBattle?.battleStatus === 0){
      setWaitBattle(true);
    }
  }, [gameData]);
  
  const handleClick = async () => {
    if(!battleName || !battleName.trim()) return null;

    try{
      await contract.createBattle(battleName, { gasLimit: 200000 });
      setWaitBattle(true);
    } catch (error) {
      setErrorMessage(error.message);    }
   }

   return (
    <>
      {waitBattle && <GameLoad />}

      {errorMessage && <p className={styles.errorText}>{errorMessage}</p>}

      <div className="flex flex-col mb-5">
        <CustomInput
          label="Battle"
          placeHolder="Enter battle name"
          value={battleName}
          handleValueChange={setBattleName}
        />

        <CustomButton
          title="Create Battle"
          handleClick={handleClick}
          restStyles="mt-6"
        />
      </div>
      <p className={styles.infoText} onClick={() => navigate('/join-battle')}>
        Or join already existing battles
      </p>
    </>
  );
};

export default PageHOC(
CreateBattle,
  <>Create <br /> a new Battle</>,
  <>Create your own battle and wait for others players to join you</>

);