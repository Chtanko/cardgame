pour lancer l'application en local il vous faut:

1. Variable d'env
À la racine du projet créer un fichier .env
Rempli le comme ceci:
```
#REDIS
REDIS_ADMIN_PORT=7638
REDIS_PORT=8736

#POSTGRES
POSTGRES_USER=admin
POSTGRES_PASSWORD=admin
POSTGRES_DB=rf
POSTGRES_PORT=7363
PGADMIN_DEFAULT_EMAIL=admin@admin.fr
PGADMIN_DEFAULT_PASSWORD=admin
PGADMIN_PORT=7763

CORS_ORIGIN=

MONGO_USER=admin
MONGO_PASSWORD=admin

KEYCLOAK_DB=default
KEYCLOAK_ADMIN=admin
KEYCLOAK_ADMIN_PASSWORD=admin
```

2. Dossier
Créer un dossier data à la racine, si il n'est pas créer
et dans ce dossier data créer plusieurs dossier.
  |- data/
    |- pgadmin_data/
    |- timescaledb_data/
    |- redis_data/
    |- redisinsight_data/

3. Docker 
À la racine du projet executer cette commande
```docker-compose up -d --build```

4. Init SQL
Une fois les images docker build rend toi dans la base timescaledb via l'adress de l'adminer
identifiant et port specifié en haut et dans le .env

rend toi dans la base créer pour le projet et execute le SQL 'init.sql' disposé dans le dossier init/pg
Tu peux tester en executant cette requête.
```
SELECT * FROM decks;
```

5. Executer WS
créer un terminal et rend toi dans le dossier /websocket/
Applique cette commande
```
npm install
```

Puis celle ci 
```
npm run dev
```

6. Executer Web
Créer un second terminal et rend toi dans le dossier /frontend/
Applique cette commande
```
npm install
```

Puis celle ci 
```
npm run dev
```

7. Jeux
va sur l'url http://localhost:3000 ou sur ton adresse local http://192.168.1.x:3000 (remplacer x par le numero)
Connecte ton metamask et joue !