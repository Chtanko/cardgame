import { drawCards, endTurn, MAX_MANA, playCard, attackCard, attackPlayer } from '../../src/services/gameService';
import { Card, MessageResult, Room, RoomStatus } from '../../src/models/gameModel';


describe('attackPlayer', () => {
  const createRoom = (): Room => ({
    turn: 0,
    players: ['player1', 'player2'],
    playerStates: {
      player1: {
        playerId: 'player1',
        mana: 5,
        life: 10,
        board: [
          { cardId: '1', mana: 1, life: 1, attack: 3 },
          { cardId: '1', mana: 1, life: 1, attack: 10 },
        ],
        deck: [],
        hand: [],
        grave: []
      },
      player2: {
        playerId: 'player2',
        mana: 5,
        life: 10,
        board: [],
        deck: [],
        hand: [],
        grave: []
      },
    },
    roomId: 'room1',
    status: RoomStatus.InGame,
    dateInsert: new Date(),
    dateUpdate: new Date()
  });

  test('should return NotYourTurn if it is not the player’s turn', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player2'].board[0];

    const message = attackPlayer(room, 'player2', attackingCard, 'player1');

    expect(message).toBe(MessageResult.NotYourTurn);
    expect(room).toEqual(room);
  });

  test('should return CardNotInBoard if the attacking card is not on the player’s board', () => {
    const room = createRoom();
    const cardNotInBoard = { cardId: '999', mana: 1, life: 1, attack: 3 };

    const message = attackPlayer(room, 'player1', cardNotInBoard, 'player2');

    expect(message).toBe(MessageResult.CardNotInBoard);
    expect(room).toEqual(room);
  });

  test('should return CardInTargetBoard if the target player has cards on their board', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[0];
    room.playerStates['player2'].board.push({ cardId: '2', mana: 2, life: 3, attack: 1 });

    const message = attackPlayer(room, 'player1', attackingCard, 'player2');

    expect(message).toBe(MessageResult.CardInTargetBoard);
    expect(room).toEqual(room);
  });

  test('should reduce the target player’s life by the attack value of the card', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[0];

    const message = attackPlayer(room, 'player1', attackingCard, 'player2');

    expect(room.playerStates['player2'].life).toBe(7); // La vie passe de 10 à 7 (10 - attaque de 3)
    expect(message).toBe(MessageResult.PlayerAttack);
  });

  test('should kill the target player’s life by the attack value of the card', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[1];

    const message = attackPlayer(room, 'player1', attackingCard, 'player2');

    expect(room.playerStates['player2'].life).toBe(0); // La vie passe de 10 à 7 (10 - attaque de 3)
    expect(message).toBe(MessageResult.EndGame);
  });

  test('should not affect the attacking player’s life or board', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[0];

    const message = attackPlayer(room, 'player1', attackingCard, 'player2');

    expect(room.playerStates['player1'].life).toBe(10); // La vie de l'attaquant reste inchangée
    expect(room.playerStates['player1'].board).toContain(attackingCard); // La carte reste sur le plateau
  });

  test('should handle multiple attacks on the same player', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[0];

    // Première attaque
    attackPlayer(room, 'player1', attackingCard, 'player2');

    // Deuxième attaque
    const message = attackPlayer(room, 'player1', attackingCard, 'player2');

    expect(room.playerStates['player2'].life).toBe(4); // La vie passe de 7 à 4 (7 - attaque de 3)
  });
});


describe('attackCard', () => {
  const createRoom = (): Room => ({
    turn: 0,
    players: ['player1', 'player2'],
    playerStates: {
      player1: {
        playerId: 'player1',
        mana: 5,
        life: 10,
        board: [
          { cardId: '1', mana: 1, life: 5, attack: 3 },
          { cardId: '1', mana: 1, life: 5, attack: 1 },
        ],
        deck: [],
        hand: [],
        grave: []
      },
      player2: {
        playerId: 'player2',
        mana: 5,
        life: 10,
        board: [
          { cardId: '2', mana: 2, life: 3, attack: 1 },
        ],
        deck: [],
        hand: [],
        grave: []
      },
    },
    roomId: 'room1',
    status: RoomStatus.InGame,
    dateInsert: new Date(),
    dateUpdate: new Date()
  });

  test('should return NotYourTurn if it is not the player’s turn', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player2'].board[0];
    const targetCard = room.playerStates['player1'].board[0];

    const message = attackCard(room, 'player2', attackingCard, targetCard, 'player1');

    expect(message).toBe(MessageResult.NotYourTurn);
    expect(room).toEqual(room);
  });

  test('should return CardNotInBoard if the attacking card is not on the player’s board', () => {
    const room = createRoom();
    const cardNotInBoard = { cardId: '999', mana: 1, life: 5, attack: 3 };
    const targetCard = room.playerStates['player2'].board[0];

    const message = attackCard(room, 'player1', cardNotInBoard, targetCard, 'player2');

    expect(message).toBe(MessageResult.CardNotInBoard);
    expect(room).toEqual(room);
  });

  test('should return CardNotInTargetBoard if the target card is not on the target player’s board', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[0];
    const cardNotInTargetBoard = { cardId: '999', mana: 2, life: 3, attack: 1 };

    const message = attackCard(room, 'player1', attackingCard, cardNotInTargetBoard, 'player2');

    expect(message).toBe(MessageResult.CardNotInTargetBoard);
    expect(room).toEqual(room);
  });

  test('should reduce the target card’s life by the attacking card’s attack value', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[1];
    const targetCard = room.playerStates['player2'].board[0];

    const message = attackCard(room, 'player1', attackingCard, targetCard, 'player2');

    expect(room.playerStates['player2'].board).toHaveLength(1);
    expect(room.playerStates['player2'].grave).toHaveLength(0);
    expect(targetCard.life).toEqual(2);
    expect(message).toBe(MessageResult.CardAttack);
  });

  test('should remove the target card from the board if its life reaches 0', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[0];
    const targetCard = room.playerStates['player2'].board[0];

    attackCard(room, 'player1', attackingCard, targetCard, 'player2');

    expect(room.playerStates['player2'].board).toHaveLength(0); // La carte cible est retirée du plateau
    expect(room.playerStates['player2'].grave).toHaveLength(1);
  });

  test('should not remove the target card if its life is greater than 0', () => {
    const room = createRoom();
    room.playerStates['player2'].board[0].life = 5; // Ajuste la vie de la carte cible
    const attackingCard = room.playerStates['player1'].board[0];
    const targetCard = room.playerStates['player2'].board[0];

    attackCard(room, 'player1', attackingCard, targetCard, 'player2');

    expect(room.playerStates['player2'].board).toHaveLength(1); // La carte cible reste sur le plateau
  });

  test('should not affect other players or cards when attacking', () => {
    const room = createRoom();
    const attackingCard = room.playerStates['player1'].board[0];
    const targetCard = room.playerStates['player2'].board[0];

    attackCard(room, 'player1', attackingCard, targetCard, 'player2');

    expect(room.playerStates['player1'].mana).toBe(5); // Le mana du joueur attaquant reste inchangé
    expect(room.playerStates['player1'].life).toBe(10); // La vie du joueur attaquant reste inchangée
    expect(room.playerStates['player2'].life).toBe(10); // La vie du joueur cible reste inchangée
  });
});


describe('playCard', () => {
  const createRoom = (): Room => ({
    turn: 0,
    players: ['player1', 'player2'],
    playerStates: {
      player1: {
        playerId: 'player1',
        mana: 5,
        life: 10,
        board: [],
        deck: [],
        hand: [
          { cardId: '1', mana: 4, life: 1, attack: 1 },
          { cardId: '2', mana: 2, life: 2, attack: 2 },
        ],
        grave:[]
      },
      player2: {
        playerId: 'player2',
        mana: 5,
        life: 10,
        board: [],
        deck: [],
        hand: [
          { cardId: '1', mana: 4, life: 1, attack: 1 },
          { cardId: '2', mana: 2, life: 2, attack: 2 },
        ],
        grave:[]
      },
    },
    roomId: 'room1',
    status: RoomStatus.InGame,
    dateInsert: new Date(),
    dateUpdate: new Date()
  });
  
  test('should play the card if it is in the player’s hand and they have enough mana', () => {
    const room = createRoom();
    const cardToPlay = room.playerStates['player1'].hand[0];

    const message = playCard(room, 'player1', cardToPlay);

    expect(message).toBe(MessageResult.PlayCard);
    expect(room.playerStates['player1'].hand).not.toContain(cardToPlay);
    expect(room.playerStates['player1'].board).toContain(cardToPlay);
    expect(room.playerStates['player1'].mana).toBe(1); 
  });

  test('should return NotYourTurn if it is not the player’s turn', () => {
    const room = createRoom();

    const cardToPlay = room.playerStates['player2'].hand[0];

    const message = playCard(room, 'player2', cardToPlay);

    expect(message).toBe(MessageResult.NotYourTurn);
    expect(room).toEqual(room);
  });

  test('should return CardNotInHand if the card is not in the player’s hand', () => {
    const room = createRoom();
    const cardNotInHand = { cardId: '999', mana: 3, life: 3, attack: 3 };

    const message = playCard(room, 'player1', cardNotInHand);

    expect(message).toBe(MessageResult.CardNotInHand);
    expect(room).toEqual(room); // L'état de la room reste inchangé
  });

  test('should return NotEnoughMana if the player does not have enough mana', () => {
    const room = createRoom();
    room.playerStates['player1'].mana = 0; // Pas de mana disponible
    const cardToPlay = room.playerStates['player1'].hand[0];

    const message = playCard(room, 'player1', cardToPlay);

    expect(message).toBe(MessageResult.NotEnoughMana);
    expect(room).toEqual(room); // L'état de la room reste inchangé
  });

  test('should not affect other players’ states', () => {
    const room = createRoom();
    const cardToPlay = room.playerStates['player1'].hand[0];

    playCard(room, 'player1', cardToPlay);

    expect(room.playerStates['player2'].mana).toBe(5); // L'autre joueur n'est pas affecté
    expect(room.playerStates['player2'].board).toHaveLength(0);
    expect(room.playerStates['player2'].hand).toHaveLength(2);
  });

  test('should reduce mana by 1 when a card is played', () => {
    const room = createRoom();
    const cardToPlay = room.playerStates['player1'].hand[0];

    const message = playCard(room, 'player1', cardToPlay);

    expect(room.playerStates['player1'].mana).toBe(1);
  });

  test('should add the card to the board after being played', () => {
    const room = createRoom();
    const cardToPlay = room.playerStates['player1'].hand[0];

    const message = playCard(room, 'player1', cardToPlay);

    expect(room.playerStates['player1'].board).toContain(cardToPlay);
  });

  test('should remove the card from the hand after being played', () => {
    const room = createRoom();
    const cardToPlay = room.playerStates['player1'].hand[0];

    const message = playCard(room, 'player1', cardToPlay);

    expect(room.playerStates['player1'].hand).not.toContain(cardToPlay);
  });
});


describe('endTurn', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const createRoom = (): Room => ({
      turn: 0,
      players: ['player1', 'player2'],
      playerStates: {
          player1: {
              playerId: "player1", mana: 5, life: 10, board: [],
              deck: [{ cardId: "1", mana: 1, life: 1, attack: 1 }], 
              hand: [{ cardId: "2", mana: 1, life: 1, attack: 1 }],
              grave: []
            },
          player2: {
              playerId: "player2", mana: 5, life: 10, board: [],
              deck: [], 
              hand: [],
              grave: [{ cardId: "2", mana: 1, life: 1, attack: 1 }, { cardId: "3", mana: 1, life: 1, attack: 1 }]
          },
      },
      roomId: '',
      status: RoomStatus.InGame,
      dateInsert: new Date(),
      dateUpdate: new Date()
  });



  test('should return NotYourTurn if it is not the player’s turn', () => {
    const room = createRoom();
    const message = endTurn(room, 'player2');

    expect(message).toBe(MessageResult.NotYourTurn);
    expect(room).toEqual(room);
  });

  test('should increment player mana', () => {
    const room = createRoom();
    
    const playerState = room.playerStates['player1'];
    playerState.mana = 9;

    const message = endTurn(room, 'player1');
    expect(room.playerStates['player1'].mana).toBe(MAX_MANA);
  });

  test('should increment player mana but not exceed MAX_MANA', () => {
    const room = createRoom();
    
    const playerState = room.playerStates['player1'];
    playerState.mana = 10;

    const message = endTurn(room, 'player1');
    expect(room.playerStates['player1'].mana).toBe(MAX_MANA);
  });

  test('should draw one card and update player hand and deck', () => {
    const room = createRoom();
    const playerState = room.playerStates['player1'];

    const message = endTurn(room, 'player1');
    expect(room.playerStates['player1'].hand).toHaveLength(2);
    expect(room.playerStates['player1'].deck).toHaveLength(0);
  });

  test('should increment room turn after endTurn', () => {
    const room = createRoom();

    const message = endTurn(room, 'player1');
    expect(room.turn).toBe(1);
  });

  test('should return EndTurn if the turn ends successfully', () => {
    const room = createRoom();

    const message = endTurn(room, 'player1');
    expect(message).toBe(MessageResult.EndTurn);
  });

  test('grave to deck shuffle in end turn', () => {
    const room = createRoom();
    const message = endTurn(room, 'player1');
    const message2 = endTurn(room, 'player2');
    expect(room.playerStates["player2"].grave).toHaveLength(0);
    expect(room.playerStates["player2"].deck).toHaveLength(1);
    expect(room.playerStates["player2"].hand).toHaveLength(1);
  });
});


describe('drawCards', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });
  
  const createDeck = (size: number): Card[] => 
    Array.from({ length: size }, (_, i) => ({ cardId: `${i + 1}`, mana: 1, life: 1, attack:1 }));

  
  test('should draw the correct number of cards', () => {
    const deck = createDeck(30);
    const cardsToDraw = 5;

    const { drawnCards, updatedDeck } = drawCards(deck, cardsToDraw);

    expect(drawnCards).toHaveLength(cardsToDraw);
    expect(updatedDeck).toHaveLength(deck.length - cardsToDraw);
  });

  test('should not modify the original deck', () => {
    const deck = createDeck(30);
    const deckCopy = [...deck];
    const cardsToDraw = 5;

    drawCards(deck, cardsToDraw);

    expect(deck).toEqual(deckCopy); // Vérifie que le deck original n'est pas modifié
  });

  test('should not include drawn cards in the updated deck', () => {
    const deck = createDeck(30);
    const cardsToDraw = 5;

    const { drawnCards, updatedDeck } = drawCards(deck, cardsToDraw);

    drawnCards.forEach(card => {
      expect(updatedDeck).not.toContainEqual(card); // Vérifie que les cartes tirées ne sont pas dans le deck restant
    });
  });

  test('should return the entire deck if cardsToDraw >= deck.length', () => {
    const deck = createDeck(5);
    const cardsToDraw = 10;

    const { drawnCards, updatedDeck } = drawCards(deck, cardsToDraw);

    expect(drawnCards).toHaveLength(5); // Toutes les cartes sont tirées
    expect(updatedDeck).toHaveLength(0); // Le deck restant est vide
  });

  test('should return empty drawnCards if cardsToDraw is 0', () => {
    const deck = createDeck(30);

    const { drawnCards, updatedDeck } = drawCards(deck, 0);

    expect(drawnCards).toHaveLength(0); // Aucune carte n'est tirée
    expect(updatedDeck).toHaveLength(30); // Aucune carte n'est tirée
  });

  test('should handle an empty deck gracefully', () => {
    const deck: Card[] = [];
    const cardsToDraw = 5;

    const { drawnCards, updatedDeck } = drawCards(deck, cardsToDraw);

    expect(drawnCards).toHaveLength(0); // Aucune carte tirée
    expect(updatedDeck).toHaveLength(0); // Deck restant également vide
  });
});