import { Socket } from "socket.io";
import { ExtendedError } from "socket.io/dist/namespace";

interface AuthenticatedSocket extends Socket {
  user?: { id: number; username: string }; 
}

const authMiddleware = (socket: AuthenticatedSocket, next: (err?: ExtendedError) => void): void => {
  const token = socket.handshake.auth.token;

  if (!token) {
    const err = new Error("Authentication error: Token is missing") as ExtendedError;
    err.data = { code: 401 }; 
    return next(err);
  }

  try {
    const user = verifyToken(token);
    socket.user = user; 
    next();
  } catch (error) {
    const err = new Error("Authentication error: Invalid token") as ExtendedError;
    err.data = { code: 403 };
    return next(err);
  }
};

function verifyToken(token: string): { id: number; username: string } {
  if (token === "valid-token") {
    return { id: 1, username: "Player1" }; 
  }
  throw new Error("Invalid token");
}

export default authMiddleware;
