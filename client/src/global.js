import buffer from 'buffer/'
const Buffer = buffer.Buffer
globalThis.Buffer = Buffer

// Définir global si nécessaire
if (typeof global === 'undefined') {
    var global = globalThis; // Utilise globalThis comme fallback
}

// Définir Buffer
if (!global.Buffer) {
    global.Buffer = Buffer;
}

// Définir process
if (!global.process) {
    global.process = process;
}

// Ajoute des propriétés supplémentaires à process
global.process.browser = true; // Indique que c'est un environnement navigateur
if (!global.process.version) {
    global.process.version = 'v16.0.0'; // Fournit une version fictive si absente
}

// Définir location pour les bibliothèques qui en ont besoin
if (!global.location) {
    global.location = {
        protocol: 'file:', // Définit un protocole par défaut
    };
}

console.log('Global object setup complete:', global);
