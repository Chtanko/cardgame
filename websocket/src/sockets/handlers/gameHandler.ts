import { Socket } from "socket.io";
import { createRoom, joinRoom,getRoomState, getRoomWithPlayerId,
  insertRoomState, getIncompleteRoom, deleteRoom, leaveRoom } from "@services/roomService";
import {playCard, endTurn, attackCard, attackPlayer, resetPlayerStatesExcept, resetPlayerStatesWithout } from "@services/gameService";
import { HandlerMessageToClient } from "@models/handlerModel";
import { v4 as uuidv4 } from 'uuid';
import { JoinGamePayload, PlayCardPayload, RoomPayload, 
  AttackPlayerCardPayload, AttackPlayerPayload, PlayerPayload,
  ChatMessagePayload} from '@models/payloadModel';
import { MessageResult, Card, Room, RoomStatus, ActionResult, ChatMessage } from '@models/gameModel';
import Redis from "ioredis";
import { getDeckById } from "@services/deckService";
import { getChatRoomState, insertChatRoom } from "@services/chatService";


export const loadGame = async(
  socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: RoomPayload
): Promise<void> => {
  const { playerId, roomId} = payload;
  try {
    const actionResult : ActionResult = await getRoomState(roomId, redisClient);

    if(actionResult.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }

    const room : Room = actionResult.action;
    socket.join(roomId);

    room.playerStates = resetPlayerStatesExcept(room.playerStates, playerId);

    const chatResult = await getChatRoomState(roomId, redisClient);
    
    let chatMessage = [] as ChatMessage[];
    if(chatResult.message === MessageResult.GetChatRoom){
      chatMessage = chatResult.action as ChatMessage[]
    }

    socket.emit(HandlerMessageToClient.LoadGameToClient, { "roomId": roomId, "room": room, chat: chatMessage});
    console.log(`player: ${playerId} rejointbiss room ${roomId}`);
  } catch (error) {
    console.error("Error joining room:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
}

export const playerIsInGameHandler = async(
  socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: PlayerPayload
): Promise<void> => {
  const { playerId } = payload;
  try {
    const playerIsInRoom = await getRoomWithPlayerId(playerId, redisClient)

    if(playerIsInRoom.message === MessageResult.PlayerIsInRoom) {
      const roomJoined = playerIsInRoom.action as Room;
      const roomId = roomJoined.roomId;

      roomJoined.playerStates = resetPlayerStatesExcept(roomJoined.playerStates, playerId);

      socket.emit(HandlerMessageToClient.RoomRejoinedToClient, { "roomId": roomId, "room": roomJoined });

      socket.to(roomId).emit(HandlerMessageToClient.PlayerRejoinedToRoom, { "roomId": roomId, "playerJoinId": playerId});
      console.log(`player: ${playerId} rejoint room ${roomId}`);
      return;
    }

    socket.emit(HandlerMessageToClient.NoRoomToClient, {});

  } catch (error) {
    console.error("Error joining room:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
}

export const joinGameHandler = async (
    socket: Socket,
    redisClient: Redis,
    pool: any,
    payload: JoinGamePayload
  ): Promise<void> => {
  const { playerId, deckId } = payload;
  try {
    const playerIsInRoom = await getRoomWithPlayerId(playerId, redisClient)

    if(playerIsInRoom.message === MessageResult.PlayerIsInRoom) {
      const roomJoined = playerIsInRoom.action as Room;
      const roomId = roomJoined.roomId;
      socket.join(roomId);

      roomJoined.playerStates = resetPlayerStatesExcept(roomJoined.playerStates, playerId);
      socket.emit(HandlerMessageToClient.RoomRejoinedToClient, { "roomId": roomId, "room": roomJoined });
      socket.to(roomId).emit(HandlerMessageToClient.PlayerRejoinedToRoom, { "roomId": roomId, "playerJoinId": playerId });
      console.log(`player: ${playerId} rejoin room ${roomId}`);
      return;
    }
    
    const actionResult = await getDeckById(playerId, deckId, redisClient, pool);

    if(actionResult.message === MessageResult.NoDeck){
      socket.emit(HandlerMessageToClient.NoDeckToClient, {});
      return;
    }

    const deck: Card[] = actionResult.action

    let roomId = await getIncompleteRoom(redisClient);

    // Si aucune room incomplète, en créer une nouvelle
    if (!roomId) {
      roomId = uuidv4();
      let room = createRoom(roomId);
      await insertRoomState(room, redisClient);
      console.log(`create room ${roomId}`);
    }
    const actionResult2 = await getRoomState(roomId, redisClient);

    if(actionResult2.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }

    let room : Room = actionResult2.action;

    room = joinRoom(room, playerId, deck);
    await insertRoomState(room, redisClient)
    socket.join(roomId);

    // Envoyer l'état initial de la room au joueur
    const actionResult3: ActionResult = await getRoomState(roomId, redisClient);

    if(actionResult3.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }

    const roomP1 = JSON.parse(JSON.stringify(actionResult3.action)) as Room;
    const roomP2 = JSON.parse(JSON.stringify(actionResult3.action)) as Room;

    roomP1.playerStates = resetPlayerStatesExcept(roomP1.playerStates, playerId);
    roomP2.playerStates = resetPlayerStatesWithout(roomP2.playerStates, playerId);

    socket.emit(HandlerMessageToClient.RoomJoinedToClient, { "roomId": roomId, "room": roomP1 });

    // Notifier les autres joueurs dans la room
    socket.to(roomId).emit(HandlerMessageToClient.PlayerJoinedToRoom, { "roomId": roomId, "playerJoinId": playerId, "room": roomP2 });
    console.log(`player: ${playerId} join room ${roomId}`);
  } catch (error) {
    console.error("Error joining room:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
};


export const joinDemoGameHandler = async (
  socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: PlayerPayload
): Promise<void> => {
const { playerId } = payload;
try {
  const playerIsInRoom = await getRoomWithPlayerId(playerId, redisClient)

  if(playerIsInRoom.message === MessageResult.PlayerIsInRoom) {
    const roomJoined = playerIsInRoom.action as Room;
    const roomId = roomJoined.roomId;
    socket.join(roomId);

    roomJoined.playerStates = resetPlayerStatesExcept(roomJoined.playerStates, playerId);
    socket.emit(HandlerMessageToClient.RoomRejoinedToClient, { "roomId": roomId, "room": roomJoined });
    socket.to(roomId).emit(HandlerMessageToClient.PlayerRejoinedToRoom, { "roomId": roomId, "playerJoinId": playerId });
    console.log(`player: ${playerId} rejoin room ${roomId}`);
    return;
  }
  
  const actionResult = await getDeckById("22222222-2222-2222-2222-222222222222", "11111111-1111-1111-1111-111111111111", redisClient, pool);

  if(actionResult.message === MessageResult.NoDeck){
    socket.emit(HandlerMessageToClient.NoDeckToClient, {});
    return;
  }

  const deck: Card[] = actionResult.action

  let roomId = await getIncompleteRoom(redisClient);

  // Si aucune room incomplète, en créer une nouvelle
  if (!roomId) {
    roomId = uuidv4();
    let room = createRoom(roomId);
    await insertRoomState(room, redisClient);
    console.log(`create room ${roomId}`);
  }
  const actionResult2 = await getRoomState(roomId, redisClient);

  if(actionResult2.message === MessageResult.NoRoom){
    socket.emit(HandlerMessageToClient.NoRoomToClient, {});
    return;
  }

  let room : Room = actionResult2.action;

  room = joinRoom(room, playerId, deck);
  await insertRoomState(room, redisClient)
  socket.join(roomId);

  // Envoyer l'état initial de la room au joueur
  const actionResult3: ActionResult = await getRoomState(roomId, redisClient);

  if(actionResult3.message === MessageResult.NoRoom){
    socket.emit(HandlerMessageToClient.NoRoomToClient, {});
    return;
  }

  const roomP1 = JSON.parse(JSON.stringify(actionResult3.action)) as Room;
  const roomP2 = JSON.parse(JSON.stringify(actionResult3.action)) as Room;

  roomP1.playerStates = resetPlayerStatesExcept(roomP1.playerStates, playerId);
  roomP2.playerStates = resetPlayerStatesWithout(roomP2.playerStates, playerId);

  socket.emit(HandlerMessageToClient.RoomJoinedToClient, { "roomId": roomId, "room": roomP1 });

  // Notifier les autres joueurs dans la room
  socket.to(roomId).emit(HandlerMessageToClient.PlayerJoinedToRoom, { "roomId": roomId, "playerJoinId": playerId, "room": roomP2 });
  console.log(`player: ${playerId} join room ${roomId}`);
} catch (error) {
  console.error("Error joining room:", error);
  socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
}
};

export const playCardHandler = async (socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: PlayCardPayload
): Promise<void> => {
  const { roomId, playerId, cardId } = payload;
  try {
    const actionResult : ActionResult = await getRoomState(roomId, redisClient);

    if(actionResult.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }

    const room : Room = actionResult.action;
    const actionResult2 = playCard(room, playerId, cardId);

    if(actionResult2.message === MessageResult.PlayCard){
      await insertRoomState(actionResult2.action, redisClient);
      console.log(`card play in ${actionResult2.message}`);

      const roomP1 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;
      const roomP2 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;

      roomP1.playerStates = resetPlayerStatesExcept(roomP1.playerStates, playerId);
      roomP2.playerStates = resetPlayerStatesWithout(roomP2.playerStates, playerId);

      socket.emit(HandlerMessageToClient.PlayCardToClient, { "roomId": roomId, "room": roomP1, "playerId": playerId, "cardId": cardId });
      socket.to(roomId).emit(HandlerMessageToClient.PlayCardToRoom, { "roomId": roomId, "room": roomP2, "playerId": playerId, "cardId": cardId });
    } else if (actionResult2.message === MessageResult.NotYourTurn){
      socket.emit(HandlerMessageToClient.NotYourTurnToClient, {});
    } else if (actionResult2.message === MessageResult.CardNotInHand){
      socket.emit(HandlerMessageToClient.CardNotInHandToClient, {});
    } else if (actionResult2.message === MessageResult.NotEnoughMana){
      socket.emit(HandlerMessageToClient.NotEnoughManaToClient, {});
    } else {
      console.error("message not found");
      socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
    }

  } catch (error) {
    console.error("Error play card:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
}


export const attackPlayerHandler = async (socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: AttackPlayerPayload)
  :Promise<void> => {

  const { roomId, playerId, cardId, targetPlayerId } = payload;
  try {
    const actionResult : ActionResult = await getRoomState(roomId, redisClient);

    if(actionResult.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }

    const room : Room = actionResult.action;

    const actionResult2 = attackPlayer(room, playerId, cardId, targetPlayerId);
  
    if(actionResult2.message === MessageResult.PlayerAttack){
      await insertRoomState(actionResult2.action, redisClient);

      const roomP1 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;
      const roomP2 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;

      roomP1.playerStates = resetPlayerStatesExcept(roomP1.playerStates, playerId);
      roomP2.playerStates = resetPlayerStatesWithout(roomP2.playerStates, playerId);

      socket.emit(HandlerMessageToClient.PlayerAttackToClient, { "roomId": roomId, "room": roomP1, "playerId": playerId, "targetPlayerId": targetPlayerId, "cardId": cardId });
      socket.to(roomId).emit(HandlerMessageToClient.PlayerAttackToRoom, { "roomId": roomId, "room": roomP2, "playerId": playerId, "targetPlayerId": targetPlayerId, "cardId": cardId });
    } else if (actionResult2.message === MessageResult.EndGame){
      await insertRoomState(actionResult2.action, redisClient);

      const roomP1 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;
      const roomP2 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;

      roomP1.playerStates = resetPlayerStatesExcept(roomP1.playerStates, playerId);
      roomP2.playerStates = resetPlayerStatesWithout(roomP2.playerStates, playerId);

      socket.emit(HandlerMessageToClient.EndGameToClient, { "roomId": roomId, "room": roomP1 });
      socket.to(roomId).emit(HandlerMessageToClient.EndGameToRoom, { "roomId": roomId, "room": roomP2 });
    } else if (actionResult2.message === MessageResult.NotYourTurn){
      socket.emit(HandlerMessageToClient.NotYourTurnToClient, {});
    } else if (actionResult2.message === MessageResult.CardNotInBoard){
      socket.emit(HandlerMessageToClient.CardNotInBoardToClient, {});
    } else if (actionResult2.message === MessageResult.CardInTargetBoard){
      socket.emit(HandlerMessageToClient.CardInTargetBoardToClient, {});
    } else if (actionResult2.message === MessageResult.CardHaveAlreadyPlayed){
      socket.emit(HandlerMessageToClient.CardHaveAlreadyPlayedToClient, {});
    } else {
      console.error("message not found");
      socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
    }
  } catch (error) {
    console.error("Error attack player:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
}

export const attackCardHandler = async (socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: AttackPlayerCardPayload)
  :Promise<void> => {
  const { roomId, playerId, cardId, targetCardId, targetPlayerId } = payload;
  try {
    const actionResult : ActionResult = await getRoomState(roomId, redisClient);

    if(actionResult.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }

    const room : Room = actionResult.action;

    const actionResult2 = attackCard(room, playerId, cardId, targetCardId, targetPlayerId)

    if(actionResult2.message === MessageResult.CardAttack){
      await insertRoomState(actionResult2.action, redisClient);
      const roomP1 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;
      const roomP2 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;

      roomP1.playerStates = resetPlayerStatesExcept(roomP1.playerStates, playerId);
      roomP2.playerStates = resetPlayerStatesWithout(roomP2.playerStates, playerId);

      socket.emit(HandlerMessageToClient.CardAttackToClient, { "roomId": roomId, "room": roomP1, "playerId": playerId, "targetPlayerId": targetPlayerId, "cardId": cardId, "targetCardId": targetCardId});
      socket.to(roomId).emit(HandlerMessageToClient.CardAttackToRoom, { "roomId": roomId, "room": roomP2, "playerId": playerId, "targetPlayerId": targetPlayerId, "cardId": cardId, "targetCardId": targetCardId });
    } else if (actionResult2.message === MessageResult.NotYourTurn){
      socket.emit(HandlerMessageToClient.NotYourTurnToClient, {});
    } else if (actionResult2.message === MessageResult.CardNotInBoard){
      socket.emit(HandlerMessageToClient.CardNotInBoardToClient, {});
    } else if (actionResult2.message === MessageResult.CardNotInTargetBoard){
      socket.emit(HandlerMessageToClient.CardNotInTargetBoardToClient, {});
    } else if (actionResult2.message === MessageResult.CardHaveAlreadyPlayed){
      socket.emit(HandlerMessageToClient.CardHaveAlreadyPlayedToClient, {});
    } else {
      console.error("message not found");
      socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
    }
  } catch (error) {
    console.error("Error attack card:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
}

export const leaveGameHandler = async (socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: RoomPayload)
  :Promise<void> => {
  const { playerId, roomId } = payload;

  try {
    let actionResult : ActionResult = await getRoomState(roomId, redisClient);

    if(actionResult.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }
    let room : Room = actionResult.action;
    room = leaveRoom(room, playerId);

    
    if (room.status === RoomStatus.Empty) await deleteRoom(room, redisClient);
    else await insertRoomState(room, redisClient);

    // Quitter le channel Socket.IO
    socket.leave(roomId);

    // Notifier les autres joueurs
    socket.emit(HandlerMessageToClient.QuitGameToClient, {});
    socket.to(roomId).emit(HandlerMessageToClient.PlayerLeftToRoom, { "roomId": roomId, "playerIdLeft": playerId });
  } catch (error) {
    console.error("Error leave game:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
}

export const endTurnHandler = async (socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: RoomPayload)
  :Promise<void> => {
  const { roomId, playerId } = payload;
  try {
    const actionResult : ActionResult = await getRoomState(roomId, redisClient);

    if(actionResult.message === MessageResult.NoRoom){
      socket.emit(HandlerMessageToClient.NoRoomToClient, {});
      return;
    }
    const room : Room = actionResult.action;
    const actionResult2 = endTurn(room, playerId);

    if (actionResult2.message === MessageResult.EndTurn){
      await insertRoomState(actionResult2.action, redisClient);


      const roomP1 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;
      const roomP2 = JSON.parse(JSON.stringify(actionResult2.action)) as Room;

      roomP1.playerStates = resetPlayerStatesExcept(roomP1.playerStates, playerId);
      roomP2.playerStates = resetPlayerStatesWithout(roomP2.playerStates, playerId);

      socket.emit(HandlerMessageToClient.EndTurnToClient, { "roomId": roomId, "room": roomP1});
      socket.to(roomId).emit(HandlerMessageToClient.EndTurnToRoom, { "roomId": roomId, "room": roomP2});
    }
    else if (actionResult2.message === MessageResult.NotYourTurn){
      socket.emit(HandlerMessageToClient.NotYourTurnToClient, {});
    } else {
      console.error("message not found");
      socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
    }
  } catch (error) {
    console.error("Error end turn:", error);
    socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
  }
}


export const chatHandler = async(socket: Socket,
  redisClient: Redis,
  pool: any,
  payload: ChatMessagePayload) => {
    const { roomId, sender, message } = payload;
    try {
      const result = await insertChatRoom(roomId, message, sender, redisClient);

      if(result.message === MessageResult.NoChatRoom){
        socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
      }else if(result.message === MessageResult.InsertChatRoom){
        const chatMessage = JSON.parse(JSON.stringify(result.action)) as ChatMessage;

        socket.emit(HandlerMessageToClient.ChatToClient, { "message": chatMessage.message, "sender": chatMessage.sender});
        socket.to(roomId).emit(HandlerMessageToClient.ChatToRoom, { "message": chatMessage.message, "sender": chatMessage.sender});
      }

    } catch (error) {
      console.error("Error end turn:", error);
      socket.emit(HandlerMessageToClient.ErrorToClient, { message: "Internal server error" });
    }
  }
export default joinGameHandler;

  