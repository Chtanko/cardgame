'use client';

import { useTranslations } from "next-intl";
import { FactionsHeader, MainHeader } from "@/components/HeaderComponent";
import "./factions.css";


const FactionsPage = () => {
    const t = useTranslations('Global');


    return(
        <div className="background">
            <MainHeader activate={5}/>
            <FactionsHeader activate={0}/>
            <main className="game-body-factions">
 
            </main>
        </div> 
    )
}

export default FactionsPage;