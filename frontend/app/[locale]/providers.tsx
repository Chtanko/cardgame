'use client';

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { WagmiProvider } from "wagmi";
import { config } from "@/services/wagmi";

import { RainbowKitProvider, type Locale } from '@rainbow-me/rainbowkit';
import { CustomAvatar } from "@/components/CustomAvatar";

const queryClient = new QueryClient();

export function Providers({ children, locale}: { children: React.ReactNode; locale: string }) {
    return(
        <WagmiProvider config={config}>
            <QueryClientProvider client={queryClient}>
                <RainbowKitProvider locale={locale as Locale} avatar={CustomAvatar}>
                {children}
                </RainbowKitProvider>
            </QueryClientProvider>
        </WagmiProvider>
    );
}