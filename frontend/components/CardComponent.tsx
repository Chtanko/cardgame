import React, { useState, useRef, useEffect } from "react";
import { Card } from "@/models/gameModel";

interface CardComponentProps{
  card: Card;
}


interface CardComponentAttackProps extends CardComponentProps {
  attackCard: (card: Card) => void;
}

interface CardComponentPlaceProps extends CardComponentProps{
  placeCard: (card: Card) => boolean;
  totalMana: number;
}

interface CardComponentSelectProps {
  card: Card;
  selectCard: (card: Card, 
      setIsClicked:React.Dispatch<React.SetStateAction<boolean>>, 
      havePlayed:boolean
  ) => void;
}


export const CardComponent = ({card, placeCard, totalMana} : CardComponentPlaceProps) => {
  return (
    <div
        className="card"
        onClick={() => placeCard(card)}
        style={{
            backgroundImage: `url(/cards/${card.cardId}.png)`,
            filter: `grayscale(${card.mana <= totalMana ? 0 : 100}%)`,
        }}
    >
    </div>
  );
};

export const CardComponentEnemy = () => {

return (
  <div
      style={{
          aspectRatio: "481 / 618",
          backgroundImage: "url(/cards/back.png)",
          backgroundSize: 'cover',
      }}
  >
  </div>
);
};

export const CardComponentBoardEnnemy = ({card, attackCard}: CardComponentAttackProps) => {

  return (
    <div
      onMouseDown={() => attackCard(card)}
      className="card2"
      style={{
          backgroundImage: `url(/cards/${card.cardId}.png)`,
      }}
    />
  );
};

export const CardComponentBoard = ({card, selectCard}: CardComponentSelectProps) => { 
  const [isClicked, setIsClicked] = useState(false);

  const handleClick = () => {
    selectCard(card, setIsClicked, card.havePlayed);
  };

  useEffect(() => {
    if(card.havePlayed && isClicked) setIsClicked(false);
  },[card]);

  return (
    <div
      className={`card ${isClicked ? 'card-selected' : ''}`}
      onMouseDown={() => handleClick()}
      style={{
        backgroundImage: `url(/cards/${card.cardId}.png)`,
        filter: `grayscale(${card.havePlayed ? 100 : 0}%)`,
      }}
    >
    </div>
  );
};

export default CardComponent;
