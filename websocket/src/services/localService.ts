import os from "os";

export function getLocalIPAdress(): string {
    const interfaces = os.networkInterfaces();

    for(const interfaceName in interfaces){
        const networkInterfaces = interfaces[interfaceName];

        if(networkInterfaces) {
            for (const net of networkInterfaces){
                if(net.family === "IPv4" && !net.internal){
                    return `http://${net.address}:3000`;
                }
            }
        }
    }
    return  "http://192.168.1.11:3000";
}