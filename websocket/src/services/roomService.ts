import { Room, Card, RoomStatus, ActionResult, MessageResult } from "@models/gameModel";
import { drawCards } from "@services/gameService";
import Redis from "ioredis";

const MAX_PLAYER = 2;
const MAX_LIFE = 30;
const STARTING_MANA = 5;
const CARDS_IN_HAND = 5;


export const createRoom = (roomId: string): Room => {
  const room: Room = {
    roomId: roomId,
    players: [],
    status: RoomStatus.Incomplete,
    turn: 0,
    playerStates: {},
    dateInsert:  new Date(),
    dateUpdate: new Date()
  }
  return room;
};
  
export const joinRoom = (room: Room, playerId: string, deck: Card[]): Room => {
  room.players.push(playerId);
  if (room.players.length === MAX_PLAYER) room.status = RoomStatus.Full;

  if (!room.playerStates) room.playerStates = {};

  const { drawnCards, updatedDeck } = drawCards(deck, CARDS_IN_HAND);

  room.playerStates[playerId] = {
    playerId: playerId,
    life: MAX_LIFE,
    mana: STARTING_MANA,
    deck: updatedDeck, 
    deckLength: updatedDeck.length,
    hand: drawnCards, 
    handLength: drawnCards.length,
    board: [],
    grave: [],
    graveLength: 0
  };

  return room
};
  
export const leaveRoom = (room: Room, playerId: string): Room => {
  room.players = room.players.filter((id: string) => id !== playerId);
  room.status = room.players.length === 0 ? RoomStatus.Empty : RoomStatus.Incomplete;
  return room;
};

export async function getRoomWithPlayerId(
  playerId: string,
  redisClient: Redis
): Promise<ActionResult> {
  const pattern = "room:*"; // Pattern pour les clés Redis des rooms
  const statuses = [RoomStatus.Incomplete, RoomStatus.Full]; // Statuts valides à rechercher
  let cursor = "0";

  try {
    do {
      // Utiliser SCAN pour récupérer les clés par lot
      const [newCursor, keys] = await redisClient.scan(cursor, "MATCH", pattern, "COUNT", 100);
      cursor = newCursor;

      for (const key of keys) {
        // Récupérer et parser chaque room
        const roomData = await redisClient.get(key);
        if (roomData) {
          const room: Room = JSON.parse(roomData);

          // Vérifier si le joueur est présent et que le statut correspond
          if (
            room.players.includes(playerId) && 
            statuses.includes(room.status)
          ) {
            return { action: room, message: MessageResult.PlayerIsInRoom }
          }
        }
      }
    } while (cursor !== "0"); // Continuer jusqu'à ce que SCAN boucle complètement

    return { action: null, message: MessageResult.RoomNotFound }
  } catch (error) {
    console.error("Error fetching room from Redis:", error);
    throw error;
  }
}


export async function getRoomState(roomId: string, redisClient: Redis) : Promise<ActionResult> {
    const room = await redisClient.get(`room:${roomId}`);

    if (room === null){
      return { action: null, message: MessageResult.NoRoom};
    }
    return { action:JSON.parse(room), message: MessageResult.HaveRoom};
}
  
export async function insertRoomState(room: Room, redisClient: Redis) : Promise<void> {
  await redisClient.set(`room:${room.roomId}`, JSON.stringify(room), 'EX', 2000);
}
  
export const getIncompleteRoom = async (redisClient: Redis): Promise<string | null> => {
  const keys = await redisClient.keys("room:*");
  for (const key of keys) {
      const room = JSON.parse(await redisClient.get(key) || "{}");
      if (room.status === RoomStatus.Incomplete) {
        return key.split(":")[1]; // Return roomId
      }
  }
  return null;
};

export async function deleteRoom(room: Room, redisClient: Redis): Promise<void>{
  redisClient.del(`room:${room.roomId}`);
}