import { Card, ChatMessage, Room } from "@/models/gameModel";

export type MessagePayload = {
    roomId: string;
    room: Room;
}

export type LoadGamePayload = {
    roomId: string;
    room: Room;
    chat: ChatMessage[];
}

export interface PlayerPayload {
    playerId: string;
}

export interface JoinGamePayload {
    playerId: string;
    deckId: string,
}
  
export interface RoomPayload {
    playerId: string;
    roomId: string;
    
}

export interface PlayCardPayload {
    playerId: string;
    roomId: string;
    cardId: string;
    room: Room;

}

export interface AttackPlayerPayload {
    playerId: string;
    roomId: string;
    cardId: string;
    targetPlayerId: string;
    room: Room;

}

export interface AttackPlayerCardPayload {
    playerId: string;
    roomId: string;
    cardId: string;
    targetPlayerId: string;
    targetCardId: string;
    room: Room;
}

export interface ChatMessagePayload {
    sender: string;
    message: string;
}

export interface NoMessagePayload {

}

export interface MessageErrorPayload {
    message: string;
}