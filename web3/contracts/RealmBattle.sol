// SPDX-License-Identifier: MIT
pragma solidity ^0.8.16;

contract BettingGame {
    address public owner;
    uint256 public betAmount = 0.01 ether;
    address[] public players;
    mapping(address => bool) public hasBet;

    event BetPlaced(address indexed player);
    event RoundEnded(address winner, uint256 amountWon);

    constructor() {
        owner = msg.sender;
    }

    // Fonction pour parier, chaque joueur doit envoyer exactement `betAmount`
    function placeBet() external payable {
        require(msg.value == betAmount * (10**18), "Incorrect bet amount");
        require(!hasBet[msg.sender], "You already placed a bet this round");

        players.push(msg.sender);
        hasBet[msg.sender] = true;

        emit BetPlaced(msg.sender);
    }

    // Fonction pour finir la manche et distribuer les gains
    function endRound(address winner) external onlyOwner {
        uint256 totalPrize = address(this).balance; // Montant total des paris
        require(totalPrize > 0, "No bets placed");

        // Reset des paris pour le prochain tour
        for (uint256 i = 0; i < players.length; i++) {
            hasBet[players[i]] = false;
        }
        delete players;

        // Transfert des gains au gagnant
        (bool success, ) = winner.call{value: totalPrize}("");
        require(success, "Transfer failed");

        emit RoundEnded(winner, totalPrize);
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner can end round");
        _;
    }
}
