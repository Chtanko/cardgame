'use client';

import { useTranslations } from "next-intl";
import { MainHeader } from "@/components/HeaderComponent";
import "./lobby.css";


const LobbyPage = () => {
    const t = useTranslations('Global');


    return(
        <div className="background-lobby">
            <MainHeader activate={3}/>
            <main className="game-body-lobby">
 
            </main>
        </div> 
    )
}

export default LobbyPage;