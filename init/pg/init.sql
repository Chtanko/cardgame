-- Table pour stocker l'état des parties
CREATE TABLE game_states (
    room_id UUID NOT NULL PRIMARY KEY,
    state JSONB NOT NULL,
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

-- Table pour stocker les actions des joueurs
CREATE TABLE game_logs (
    game_id UUID,
    player_id TEXT NOT NULL,
    action JSONB NOT NULL,
    timestamp TIMESTAMPTZ DEFAULT NOW()
);

CREATE TABLE decks (
  deckId UUID PRIMARY KEY,
  playerId UUID,
  name TEXT,
  updatedAt TIMESTAMPTZ DEFAULT NOW(),
  insertAt TIMESTAMPTZ DEFAULT NOW(),
  deleteLine INT DEFAULT 0
);

CREATE TABLE cards (
  cardId UUID PRIMARY KEY,
  name TEXT,
  description TEXT,
  mana INT,
  attack INT,
  life INT,
  updatedAt TIMESTAMPTZ DEFAULT NOW(),
  insertAt TIMESTAMPTZ DEFAULT NOW(),
  deleteLine INT DEFAULT 0
);

CREATE TABLE deck_card (
  cardId UUID REFERENCES cards(cardId),
  deckId UUID REFERENCES decks(deckId)
);

-- Convertir en hypertable pour TimescaleDB
SELECT create_hypertable('game_logs', 'timestamp');

CREATE OR REPLACE FUNCTION get_cards_of_deck(deckId UUID, playerId UUID)
RETURNS TABLE (
    cardId TEXT,
    mana INT,
    attack INT,
    life INT
) AS $$
BEGIN
    RETURN QUERY
    SELECT 
        c.cardId::TEXT, 
        c.mana, 
        c.attack, 
        c.life
    FROM 
        deck_card AS cd
    INNER JOIN 
        decks AS d ON d.deckId = cd.deckId AND d.playerId = get_cards_of_deck.playerId
    INNER JOIN 
        cards AS c ON c.cardId = cd.cardId
    WHERE 
        d.deckId = get_cards_of_deck.deckId;
END;
$$ LANGUAGE plpgsql;


INSERT INTO decks (deckId, playerId, name)
VALUES 
('11111111-1111-1111-1111-111111111111', '22222222-2222-2222-2222-222222222222', 'Mage Deck');

INSERT INTO cards (cardId, name, mana, attack, life)
VALUES 
('33333333-3333-3333-3333-333333333333', 'rhino', 3, 5, 4),
('44444444-4444-4444-4444-444444444444', 'spiderman', 2, 3, 3),
('55555555-5555-5555-5555-555555555555', 'camille', 4, 6, 5),
('66666666-6666-6666-6666-666666666666', 'chtanx', 1, 1, 2),
('77777777-7777-7777-7777-777777777777', 'kiki', 5, 7, 6);

INSERT INTO deck_card (cardId, deckId)
VALUES
('33333333-3333-3333-3333-333333333333', '11111111-1111-1111-1111-111111111111'),
('44444444-4444-4444-4444-444444444444', '11111111-1111-1111-1111-111111111111'),
('55555555-5555-5555-5555-555555555555', '11111111-1111-1111-1111-111111111111'),
('66666666-6666-6666-6666-666666666666', '11111111-1111-1111-1111-111111111111'),
('77777777-7777-7777-7777-777777777777', '11111111-1111-1111-1111-111111111111');
