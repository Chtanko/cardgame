import { Room, MessageResult, Card, ActionResult } from "@/models/gameModel";
import { MAX_PLAYER, MAX_MANA} from "@/models/constant";
import { v4 as uuidv4 } from 'uuid';

export const isTurn = (room: Room, playerId: string): MessageResult => {
  if (room.turn % MAX_PLAYER !== room.players.indexOf(playerId)) {
    return MessageResult.NotYourTurn;
  }
  return MessageResult.IsYourTurn
}

export const endTurn = (room: Room, playerId: string): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) 
    return { action: room, message: MessageResult.NotYourTurn};

  return { action: room, message: MessageResult.EndTurn};
};

export const playCard = (room: Room, playerId: string, card: Card): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) return { action: room, message: MessageResult.NotYourTurn };

  if (!room.playerStates[playerId].hand.some(c => c.id === card.id)) {
    return  { action: room, message: MessageResult.CardNotInHand };
  }

  if (room.playerStates[playerId].mana < card.mana) {
    return  { action: room, message: MessageResult.NotEnoughMana };
  }

  return { action: room, message: MessageResult.PlayCard };
};

export const attackCard = (room: Room, playerId: string, card: Card, targetCard: Card, targetPlayerId: string): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) 
    return { action: room, message: MessageResult.NotYourTurn };

  if (!room.playerStates[playerId].board.some(c => c.id === card.id)) {
    return { action: room, message: MessageResult.CardNotInBoard };
  }

  if (!room.playerStates[targetPlayerId].board.some(c => c.id === targetCard.id)) {
    return { action: room, message: MessageResult.CardNotInTargetBoard };
  }

  const cardIndex = room.playerStates[playerId].board.findIndex((c: Card) => c.id === card.id);

  if(room.playerStates[playerId].board[cardIndex].havePlayed === true){
    return { action: room, message: MessageResult.CardHaveAlreadyPlayed };
  }

  return { action: room, message: MessageResult.CardAttack };
};

export const attackPlayer = (room: Room, playerId: string, card: Card, targetPlayerId: string): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) 
    return { action: room, message: MessageResult.NotYourTurn};

  if (!room.playerStates[playerId].board.some(c => c.id === card.id)) {
    return { action: room, message: MessageResult.CardNotInBoard };
  }

  if (room.playerStates[targetPlayerId].board.length >= 1){
    return { action: room, message: MessageResult.CardInTargetBoard };
  }

  const cardIndex = room.playerStates[playerId].board.findIndex((c: Card) => c.id === card.id);

  if(room.playerStates[targetPlayerId].board[cardIndex].havePlayed === true){
    return { action: room, message: MessageResult.CardHaveAlreadyPlayed };
  }

  return { action: room, message: MessageResult.PlayerAttack };
};
