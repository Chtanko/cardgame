'use client';


import { ChatMessage } from "@/models/gameModel";
import { useState, SetStateAction, createContext, useContext }from "react";


export type ChatContextType = {
    setChatMessage: (message: string, sender: string) => void; 
    setLoadChatMessage: (chatMessage: ChatMessage[]) => void;
    setChat: (value: SetStateAction<ChatMessage[]>) => void;
    chat: ChatMessage[];
};

const ChatContext = createContext<ChatContextType | undefined>(undefined);


export const ChatProvider = ({ children }: { children: React.ReactNode }) => {
    const [chat, setChat] = useState<ChatMessage[]>([]);

    function setChatMessage(message: string, sender: string){
        setChat(prevChat => [...prevChat, {sender:sender, message: message, color: 'white'}]);
    }

    function setLoadChatMessage(chatMessage: ChatMessage[]){
        setChat(chatMessage);
    }

    return(
        <ChatContext.Provider value={{ setChatMessage, setLoadChatMessage, setChat, chat}}>
            {children}
        </ChatContext.Provider>
    )
};

export const useChat = () => {
    const context = useContext(ChatContext);
    if (!context) {
        throw new Error("useChat must be used within an ChatProvider");
    }
    return context;
};