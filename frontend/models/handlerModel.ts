import { Dispatch, SetStateAction } from "react";
import { ChatMessage, Room } from "./gameModel";
import { QueryParams } from "next-intl/navigation";
import { NavigateOptions } from "next/dist/shared/lib/app-router-context.shared-runtime";

export enum HandlerMessageToClient {
    ErrorToClient="errorToClient",
    NoRoomToClient="noRoomToClient",
    LoadGameToClient="loadGameToClient",
    RoomRejoinedToClient="roomRejoinedToClient",
    PlayerRejoinedToRoom="playerRejoinedToRoom",
    NoDeckToClient="noDeckToClient",
    RoomJoinedToClient="roomJoinedToClient",
    PlayerJoinedToRoom="playerJoinedToRoom",
    QuitGameToClient="quitGameToClient",
    PlayerLeftToRoom="playerLeftToRoom",
    EndTurnToClient="endTurnToClient",
    EndTurnToRoom="endTurnToRoom",
    NotYourTurnToClient="notYourTurnToClient",
    PlayCardToClient="playCardToClient",
    PlayCardToRoom="playCardToRoom",
    CardNotInHandToClient="cardNotInHandToClient",
    NotEnoughManaToClient="notEnoughManaToClient",
    CardAttackToClient="cardAttackToClient",
    CardAttackToRoom="cardAttackToRoom",
    CardNotInBoardToClient="cardNotInBoardToClient",
    CardNotInTargetBoardToClient="cardNotInTargetBoardToClient",
    CardHaveAlreadyPlayedToClient="cardHaveAlreadyPlayedToClient",
    CardInTargetBoardToClient="cardInTargetBoardToClient",
    EndGameToClient="endGameToClient",
    EndGameToRoom="endGameToRoom",
    PlayerAttackToClient="playerAttackToClient",
    PlayerAttackToRoom="playerAttackToRoom",
    ChatToRoom="chatToRoom",
    ChatToClient="chatToClient",
}

export enum HandlerMessageToServer {
    PlayerIsInGame="playerIsInGame",
    LoadGame="loadGame",
    JoinGame="joinGame",
    JoinDemoGame="joinDemoGame",
    JoinDemoAIGame="joinDemoAIGame",
    PlayCard="playCard",
    AttackPlayer="attackPlayer",
    AttackCard="attackCard",
    EndTurn="endTurn",
    LeaveGame="leaveGame",
    Disconnect="disconnect",
    Chat="chat"
}

export type RoomHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setPlayerTwoId: (value: SetStateAction<string>) => void;
    setIsYourTurn: (value: SetStateAction<boolean>) => void;
    playerId: string;
    router: {
        replace: (href: string | {
            pathname: string;
            query?: QueryParams;
        }, options?: (Partial<NavigateOptions> & {
            locale?: string;
        }) | undefined) => void
    }
}

export type GameHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setPlayerTwoId: (value: SetStateAction<string>) => void;
    playerId: string;
}

export type PlayCardHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setPlayerTwoId: (value: SetStateAction<string>) => void;
    playSoundEffect: (src: string) => void;
    playerId: string;
}

export type AttackPlayerHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setPlayerTwoId: (value: SetStateAction<string>) => void;
    playSoundEffect: (src: string) => void;
    playerId: string;
}

export type AttackCardHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setPlayerTwoId: (value: SetStateAction<string>) => void;
    playSoundEffect: (src: string) => void;
    playerId: string;
}

export type EndTurnHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setPlayerTwoId: (value: SetStateAction<string>) => void;
    setIsYourTurn: (value: SetStateAction<boolean>) => void;
    playSoundEffect: (src: string) => void;
    playerId: string;
}

export type NoRoomHandler = {
    router: {
        replace: (href: string | {
            pathname: string;
            query?: QueryParams;
        }, options?: (Partial<NavigateOptions> & {
            locale?: string;
        }) | undefined) => void
    }
}

export type DefaultLobbyHandler = {
    setRoomStatus: Dispatch<SetStateAction<string>>;
    setRoomId: Dispatch<SetStateAction<string>>;
    router: {
        replace: (href: string | {
            pathname: string;
            query?: QueryParams;
        }, options?: (Partial<NavigateOptions> & {
            locale?: string;
        }) | undefined) => void
    };
}

export type RoomJoinedHandler = {
    setRoomStatus: Dispatch<SetStateAction<string>>;
    setRoomId: Dispatch<SetStateAction<string>>;
    setCountDown: Dispatch<SetStateAction<boolean>>
}

export type LoadGameHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setPlayerTwoId: (value: SetStateAction<string>) => void;
    setIsYourTurn: (value: SetStateAction<boolean>) => void;
    playerId: string;
    router: {
        replace: (href: string | {
            pathname: string;
            query?: QueryParams;
        }, options?: (Partial<NavigateOptions> & {
            locale?: string;
        }) | undefined) => void
    };
    setLoadChatMessage: (chatMessage: ChatMessage[]) => void;
}

export type QuitGameToClientHandler = {
    setRoomStatus: Dispatch<SetStateAction<string>>;
    setRoomId: Dispatch<SetStateAction<string>>;
}

export type EndGameHandler = {
    setRoom: (value: SetStateAction<Room | undefined>) => void;
    setEndGame: (value: SetStateAction<boolean>) => void;
}

export type MessageHandler = {
    setMessage: (value: SetStateAction<string>) => void;
    playSoundEffect: (src: string) => void;
}

export type ChatMessageHandler = {
    setChatMessage: (message: string, sender: string) => void;
    playSoundEffect: (src: string) => void;
}

export type GameHandlers = {
    loadGameHandler?:LoadGameHandler;
    roomHandler?: RoomHandler;
    gameHandler?: GameHandler;
    noRoomHandler?: NoRoomHandler;
    endTurnHandler?: EndTurnHandler;
    attackCardToClientHandler?: AttackCardHandler;
    attackCardToRoomHandler?: AttackCardHandler;
    attackPlayerToClientHandler?: AttackPlayerHandler;
    attackPlayerToRoomHandler?: AttackPlayerHandler;
    playCardToClientHandler?: PlayCardHandler;
    playCardToRoomHandler?: PlayCardHandler;
    notYourTurnHandler?:MessageHandler;
    cardNotInBoardHandler?:MessageHandler;
    cardInTargetBoardHandler?:MessageHandler;
    cardHaveAlreadyPlayedHandler?:MessageHandler;
    cardNotInTargetBoardHandler?:MessageHandler;
    cardNotInHandHandler?:MessageHandler;
    notEnoughManaHandler?:MessageHandler;
    chatToClientHandler?:ChatMessageHandler;
    chatToRoomHandler?:ChatMessageHandler;
    endGameHandler:EndGameHandler;
}


export type RoomHandlers = {
    defaultLobbyHandler?:DefaultLobbyHandler;
    roomJoinedHandler?:RoomJoinedHandler;
    loadGameHandler?:LoadGameHandler;
    quitGameToClientHandler:QuitGameToClientHandler;
}
