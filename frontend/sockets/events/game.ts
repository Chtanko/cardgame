import room from '@/sockets/handlers/roomHandler';
import game from '@/sockets/handlers/gameHandler';
import lobby from '@/sockets/handlers/lobbyHandler';
import { errorHandler } from '@/sockets/handlers/errorHandler';
import { Socket } from 'socket.io-client';
import { MessagePayload, NoMessagePayload, PlayCardPayload, MessageErrorPayload, AttackPlayerCardPayload, AttackPlayerPayload, ChatMessagePayload, LoadGamePayload } from '@/models/payloadModel';
import { RoomHandlers, GameHandlers, HandlerMessageToClient} from '@/models/handlerModel';

export const roomEvents = (socket: Socket, handler: RoomHandlers) => {
   const onRoomJoined = (message: MessagePayload) => {
        if (handler.roomJoinedHandler) {
            lobby.roomJoinedHandler(message, handler.roomJoinedHandler);
        }
    }
    const onPlayerJoined = (message: MessagePayload) => {
        if (handler.roomJoinedHandler) {
            lobby.playerJoinedHandler(message, handler.roomJoinedHandler);
        }
    }
    const onRoomRejoined = (message: MessagePayload) => {
        if (handler.defaultLobbyHandler) {
            lobby.roomRejoinedHandler(message, handler.defaultLobbyHandler);
        }
    }
    const onPlayerRejoined = (message: MessagePayload) => {
        if (handler.defaultLobbyHandler) {
            lobby.playerRejoinedHandler(message, handler.defaultLobbyHandler);
        }
    }
    const onLoadGame = (message: MessagePayload) => {
        if (handler.defaultLobbyHandler) {
            lobby.loadGameHandler(message, handler.defaultLobbyHandler);
        }
    }
    const onPlayerLeft = (message: MessagePayload) => {
        if (handler.defaultLobbyHandler) {
            lobby.playerLeftHandler(message, handler.defaultLobbyHandler);
        }
    }
    const onQuitGameToClient = (message: NoMessagePayload) => {
        if (handler.quitGameToClientHandler){
            lobby.quitGameToClientHandler(message, handler.quitGameToClientHandler);
        }
    }
    
    const onError = (message: MessageErrorPayload) => {
        errorHandler(message);
    }
    const onNoRoom = (message: MessagePayload) => {
        lobby.noRoomHandler(message);
    }
    socket.on(HandlerMessageToClient.RoomJoinedToClient, onRoomJoined);
    socket.on(HandlerMessageToClient.PlayerJoinedToRoom, onPlayerJoined);
    socket.on(HandlerMessageToClient.RoomRejoinedToClient, onRoomRejoined);
    socket.on(HandlerMessageToClient.PlayerRejoinedToRoom, onPlayerRejoined);
    socket.on(HandlerMessageToClient.PlayerLeftToRoom, onPlayerLeft);
    socket.on(HandlerMessageToClient.ErrorToClient, onError);
    socket.on(HandlerMessageToClient.NoRoomToClient, onNoRoom);
    socket.on(HandlerMessageToClient.LoadGameToClient, onLoadGame);
    socket.on(HandlerMessageToClient.QuitGameToClient, onQuitGameToClient)

    return {
        cleanup: () => {
            // Nettoyer les événements pour éviter les doublons
            socket.off(HandlerMessageToClient.RoomJoinedToClient, onRoomJoined);
            socket.off(HandlerMessageToClient.PlayerJoinedToRoom, onPlayerJoined);
            socket.off(HandlerMessageToClient.RoomRejoinedToClient, onRoomRejoined);
            socket.off(HandlerMessageToClient.PlayerRejoinedToRoom, onPlayerRejoined);
            socket.off(HandlerMessageToClient.PlayerLeftToRoom, onPlayerLeft);
            socket.off(HandlerMessageToClient.ErrorToClient, onError);
            socket.off(HandlerMessageToClient.NoRoomToClient, onNoRoom);
            socket.off(HandlerMessageToClient.LoadGameToClient, onLoadGame);
            socket.off(HandlerMessageToClient.QuitGameToClient, onQuitGameToClient)
        }
    };
}

export const gameEvents = (socket: Socket, handler: GameHandlers) => {
   const onRoomJoined = (message: MessagePayload) => {
        if (handler.roomHandler){
            room.roomJoinedHandler(message, handler.roomHandler);
        }
        
    }
    const onPlayerJoined = (message: MessagePayload) => {
        if (handler.roomHandler){
            room.playerJoinedHandler(message, handler.roomHandler);
        }
    }
    const onRoomRejoined = (message: MessagePayload) => {
        if (handler.roomHandler){
            room.roomRejoinedHandler(message, handler.roomHandler);
        }
    }
    const onPlayerRejoined = (message: MessagePayload) => {
        if (handler.roomHandler){
            room.playerRejoinedHandler(message, handler.roomHandler);
        }
    }
    const onLoadGame = (message: LoadGamePayload) => {
        if (handler.loadGameHandler){
            room.loadGameHandler(message, handler.loadGameHandler);
        }
    }
    const onNoDeck = (message: MessagePayload) => {
        game.noDeckHandler(message);
    }
    const onPlayerLeft = (message: MessagePayload) => {
        if (handler.roomHandler){
            room.playerLeftHandler(message, handler.roomHandler);
        }
    }
    const onEndTurnToClient = (message: MessagePayload) => {
        if (handler.endTurnHandler){
            game.endTurnToClientHandler(message, handler.endTurnHandler);
        }
    }
    const onEndTurnToRoom = (message: MessagePayload) => {
        if (handler.endTurnHandler){
            game.endTurnToRoomHandler(message, handler.endTurnHandler);
        }
    }
    const onEndGameToClient = (message: MessagePayload) => {
        if (handler.endGameHandler){
            game.endGameToClientHandler(message, handler.endGameHandler);
        }
    }
    const onEndGameToRoom = (message: MessagePayload) => {
        if (handler.endGameHandler){
            game.endGameToRoomHandler(message, handler.endGameHandler);
        }
    }
    const onError = (message: MessageErrorPayload) => {
        errorHandler(message);
    }
    const onNotYourTurn = (message: MessagePayload) => {
        if (handler.notYourTurnHandler){
            game.notYourTurnHandler(message, handler.notYourTurnHandler);
        }
    }
    const onCardNotInBoard = (message: MessagePayload) => {
        if (handler.cardNotInBoardHandler){
            game.cardNotInBoardHandler(message, handler.cardNotInBoardHandler);
        }
    }
    const onCardNotInTargetBoard = (message: MessagePayload) => {
        if (handler.cardNotInTargetBoardHandler){
            game.cardNotInTargetBoardHandler(message, handler.cardNotInTargetBoardHandler);
        }
    }
    const onCardInTargetBoard = (message: MessagePayload) => {
        if (handler.cardInTargetBoardHandler){
            game.cardInTargetBoardHandler(message, handler.cardInTargetBoardHandler);
        }
    }
    const onCardAttackToClient = (message: AttackPlayerCardPayload) => {
        if (handler.attackCardToClientHandler){
            game.cardAttackToClientHandler(message, handler.attackCardToClientHandler);
        }
    }
    const onCardAttackToRoom = (message: AttackPlayerCardPayload) => {
        if (handler.attackCardToRoomHandler){
            game.cardAttackToRoomHandler(message, handler.attackCardToRoomHandler);
        }
    }
    const onPlayerAttackToClient = (message: AttackPlayerPayload) => {
        if (handler.attackPlayerToClientHandler){
            game.playerAttackToClientHandler(message, handler.attackPlayerToClientHandler);
        }
    }
    const onPlayerAttackToRoom = (message: AttackPlayerPayload) => {
        if (handler.attackPlayerToRoomHandler){
            game.playerAttackToRoomHandler(message, handler.attackPlayerToRoomHandler);
        }
    }
    const onPlayCardToClient = (message: PlayCardPayload) => {
        if (handler.playCardToClientHandler){
            game.playCardToClientHandler(message, handler.playCardToClientHandler);
        }
    }
    const onPlayCardToRoom = (message: PlayCardPayload) => {
        if (handler.playCardToRoomHandler){
            game.playCardToRoomHandler(message, handler.playCardToRoomHandler);
        }
    }
    const onNoRoom = (message: NoMessagePayload) => {
        if (handler.noRoomHandler){
            room.noRoomHandler(message, handler.noRoomHandler);
        }
    }
    const onNotEnoughMana = (message: NoMessagePayload) => {
        if (handler.notEnoughManaHandler){
            game.notEnoughManaHandler(message, handler.notEnoughManaHandler);
        }
    }
    const onCardNotInHand = (message: NoMessagePayload) => {
        if (handler.cardNotInHandHandler){
            game.cardNotInHandHandler(message, handler.cardNotInHandHandler);
        }
    }
    const onCardHaveAlreadyPlayedToClient = (message: NoMessagePayload) => {
        if (handler.cardHaveAlreadyPlayedHandler){
            game.cardHaveAlreadyPlayedToClientHandler(message, handler.cardHaveAlreadyPlayedHandler);
        }
    }
    const onQuitGameToClient = (message: MessagePayload) => {
        if (handler.gameHandler){
            game.quitGameToClientHandler(message, handler.gameHandler); //Erreur ici NoMessagePayload
        }
    }

    const onChatToRoomHandler = (message: ChatMessagePayload) => {
        if (handler.chatToRoomHandler){
            game.chatToRoomHandler(message, handler.chatToRoomHandler);
        }
    }

    const onChatToClientHandler = (message: ChatMessagePayload) => {
        if (handler.chatToClientHandler){
            game.chatToClientHandler(message, handler.chatToClientHandler);
        }
    }

    socket.on(HandlerMessageToClient.RoomJoinedToClient, onRoomJoined);
    socket.on(HandlerMessageToClient.PlayerJoinedToRoom, onPlayerJoined);
    socket.on(HandlerMessageToClient.RoomRejoinedToClient, onRoomRejoined);
    socket.on(HandlerMessageToClient.PlayerRejoinedToRoom, onPlayerRejoined);
    socket.on(HandlerMessageToClient.NoDeckToClient, onNoDeck);
    socket.on(HandlerMessageToClient.PlayerLeftToRoom, onPlayerLeft);
    socket.on(HandlerMessageToClient.EndTurnToClient, onEndTurnToClient);
    socket.on(HandlerMessageToClient.EndTurnToRoom, onEndTurnToRoom);
    socket.on(HandlerMessageToClient.EndGameToClient, onEndGameToClient);
    socket.on(HandlerMessageToClient.EndGameToRoom, onEndGameToRoom);
    socket.on(HandlerMessageToClient.ErrorToClient, onError);
    socket.on(HandlerMessageToClient.NotYourTurnToClient, onNotYourTurn);
    socket.on(HandlerMessageToClient.NotEnoughManaToClient, onNotEnoughMana);
    socket.on(HandlerMessageToClient.CardNotInHandToClient, onCardNotInHand);
    socket.on(HandlerMessageToClient.CardNotInBoardToClient, onCardNotInBoard);
    socket.on(HandlerMessageToClient.CardNotInTargetBoardToClient, onCardNotInTargetBoard);
    socket.on(HandlerMessageToClient.CardInTargetBoardToClient, onCardInTargetBoard);
    socket.on(HandlerMessageToClient.CardAttackToClient, onCardAttackToClient);
    socket.on(HandlerMessageToClient.CardAttackToRoom, onCardAttackToRoom);
    socket.on(HandlerMessageToClient.PlayerAttackToClient, onPlayerAttackToClient);
    socket.on(HandlerMessageToClient.PlayerAttackToRoom, onPlayerAttackToRoom);
    socket.on(HandlerMessageToClient.PlayCardToClient, onPlayCardToClient);
    socket.on(HandlerMessageToClient.PlayCardToRoom, onPlayCardToRoom);
    socket.on(HandlerMessageToClient.NoRoomToClient, onNoRoom);
    socket.on(HandlerMessageToClient.LoadGameToClient, onLoadGame);
    socket.on(HandlerMessageToClient.CardHaveAlreadyPlayedToClient, onCardHaveAlreadyPlayedToClient);
    socket.on(HandlerMessageToClient.QuitGameToClient, onQuitGameToClient);
    socket.on(HandlerMessageToClient.ChatToRoom, onChatToRoomHandler);
    socket.on(HandlerMessageToClient.ChatToClient, onChatToClientHandler);


    return {
        cleanup: () => {
            socket.off(HandlerMessageToClient.RoomJoinedToClient, onRoomJoined);
            socket.off(HandlerMessageToClient.PlayerJoinedToRoom, onPlayerJoined);
            socket.off(HandlerMessageToClient.RoomRejoinedToClient, onRoomRejoined);
            socket.off(HandlerMessageToClient.PlayerRejoinedToRoom, onPlayerRejoined);
            socket.off(HandlerMessageToClient.NoDeckToClient, onNoDeck);
            socket.off(HandlerMessageToClient.PlayerLeftToRoom, onPlayerLeft);
            socket.off(HandlerMessageToClient.EndTurnToClient, onEndTurnToClient);
            socket.off(HandlerMessageToClient.EndTurnToRoom, onEndTurnToRoom);
            socket.off(HandlerMessageToClient.EndGameToClient, onEndGameToClient);
            socket.off(HandlerMessageToClient.EndGameToRoom, onEndGameToRoom);
            socket.off(HandlerMessageToClient.ErrorToClient, onError);
            socket.off(HandlerMessageToClient.NotYourTurnToClient, onNotYourTurn);
            socket.off(HandlerMessageToClient.NotEnoughManaToClient, onNotEnoughMana);
            socket.off(HandlerMessageToClient.CardNotInHandToClient, onCardNotInHand);
            socket.off(HandlerMessageToClient.CardNotInBoardToClient, onCardNotInBoard);
            socket.off(HandlerMessageToClient.CardNotInTargetBoardToClient, onCardNotInTargetBoard);
            socket.off(HandlerMessageToClient.CardInTargetBoardToClient, onCardInTargetBoard);
            socket.off(HandlerMessageToClient.CardAttackToClient, onCardAttackToClient);
            socket.off(HandlerMessageToClient.CardAttackToRoom, onCardAttackToRoom);
            socket.off(HandlerMessageToClient.PlayerAttackToClient, onPlayerAttackToClient);
            socket.off(HandlerMessageToClient.PlayerAttackToRoom, onPlayerAttackToRoom);
            socket.off(HandlerMessageToClient.PlayCardToClient, onPlayCardToClient);
            socket.off(HandlerMessageToClient.PlayCardToRoom, onPlayCardToRoom);
            socket.off(HandlerMessageToClient.NoRoomToClient, onNoRoom);
            socket.off(HandlerMessageToClient.LoadGameToClient, onLoadGame);
            socket.off(HandlerMessageToClient.CardHaveAlreadyPlayedToClient, onCardHaveAlreadyPlayedToClient);
            socket.off(HandlerMessageToClient.QuitGameToClient, onQuitGameToClient);
            socket.off(HandlerMessageToClient.ChatToRoom, onChatToRoomHandler);
            socket.off(HandlerMessageToClient.ChatToClient, onChatToClientHandler);
        }
    };
}