import { ServerOptions } from "socket.io";
import {redisClient} from "@database/redis";

import { createAdapter } from "@socket.io/redis-adapter";
import { getLocalIPAdress } from "@services/localService";

const subClient = redisClient.duplicate();
export const socketOptions: Partial<ServerOptions> = {
  adapter: createAdapter(redisClient, subClient),
  cors: {
    origin: ["https://admin.socket.io", "http://localhost:3000",getLocalIPAdress(), "http://localhost:5173", process.env.CORS_ORIGIN || "*" ], 
    methods: ["GET", "POST"],
    credentials: true, 
  },
  allowEIO3: false, 
  transports: ["websocket", "polling"], 
  maxHttpBufferSize: 1e6, 
  pingInterval: 25000, // Intervalle entre les pings en ms
  pingTimeout: 20000, // Timeout pour le ping en ms
};

export default socketOptions;
