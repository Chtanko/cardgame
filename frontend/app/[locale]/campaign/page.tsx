'use client';

import { useTranslations } from "next-intl";
import { MainHeader } from "@/components/HeaderComponent";
import "./campaign.css";


const CampaignPage = () => {
    const t = useTranslations('Global');


    return(
        <div className="background-campaign">
            <MainHeader activate={0}/>
            <main className="game-body-campaign">
 
            </main>
        </div> 
    )
}

export default CampaignPage;