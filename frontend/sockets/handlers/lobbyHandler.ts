import { RoomStatus } from "@/models/gameModel";
import { NoRoomHandler, QuitGameToClientHandler, RoomJoinedHandler, DefaultLobbyHandler } from "@/models/handlerModel";
import { MessagePayload, NoMessagePayload } from "@/models/payloadModel"

const defaultLobbyHandler = (message: MessagePayload, handler: DefaultLobbyHandler) => {
    handler.setRoomStatus(message.room.status);
    handler.setRoomId(message.room.roomId);
    if (message.room.status === RoomStatus.Full) {
        handler.router.replace(`/duel/${message.roomId}`);
    }
}

const roomJoinedHandler = (message: MessagePayload, handler: RoomJoinedHandler) => {
    handler.setRoomStatus(message.room.status);
    handler.setRoomId(message.room.roomId);
    if (message.room.status === RoomStatus.Full) {
        handler.setCountDown(true);
    }
}


const playerJoinedHandler = (message: MessagePayload, handler: RoomJoinedHandler) => {
    handler.setRoomStatus(message.room.status);
    handler.setRoomId(message.room.roomId);
    if (message.room.status === RoomStatus.Full) {
        handler.setCountDown(true);
    }
}

const playerLeftHandler = (message: MessagePayload, handler: DefaultLobbyHandler) => {
    //defaultLobbyHandler(message, handler);
}

const roomRejoinedHandler = (message: MessagePayload, handler: DefaultLobbyHandler) => {
    defaultLobbyHandler(message, handler);
}

const playerRejoinedHandler = (message: MessagePayload, handler: DefaultLobbyHandler) => {
    //defaultLobbyHandler(message, handler);
}

const noRoomHandler = (message: MessagePayload) => {
}

const loadGameHandler = (message: MessagePayload, handler: DefaultLobbyHandler) => {
    defaultLobbyHandler(message, handler);
}
 
const quitGameToClientHandler = (message: NoMessagePayload, handler: QuitGameToClientHandler) => {
    handler.setRoomStatus(RoomStatus.Empty);
    handler.setRoomId("");
}

const lobby = {
    roomJoinedHandler,
    playerJoinedHandler,
    playerLeftHandler,
    roomRejoinedHandler,
    playerRejoinedHandler,
    noRoomHandler,
    loadGameHandler,
    quitGameToClientHandler,
}

export default lobby;