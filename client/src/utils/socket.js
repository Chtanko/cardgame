import { io } from "socket.io-client";

const socket = io("http://localhost:3001");

// Événements par défaut
socket.on("connect", () => console.log(`Connecté au WebSocket : ${socket.id}`));
socket.on("disconnect", () => console.log("Déconnecté du WebSocket"));

// Exporter le socket pour une utilisation dans tout le projet
export default socket;
