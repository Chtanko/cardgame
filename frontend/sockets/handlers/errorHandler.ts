import { MessageErrorPayload } from "@/models/payloadModel";

export const errorHandler = (message: MessageErrorPayload) => {
    console.log(message.message);
}