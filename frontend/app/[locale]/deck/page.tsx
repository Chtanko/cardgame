'use client';

import { useTranslations } from "next-intl";
import { MainHeader, MarketPlaceHeader } from "@/components/HeaderComponent";
import "./deck.css";

const DeckPage = () => {
    const t = useTranslations('Global');

    const FAKE_DECK = [
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
        {"id": "33333333-3333-3333-3333-333333333333"},
        {"id": "44444444-4444-4444-4444-444444444444"},
        {"id": "55555555-5555-5555-5555-555555555555"},
        {"id": "66666666-6666-6666-6666-666666666666"},
        {"id": "77777777-7777-7777-7777-777777777777"},
    ]

    return(
        <div className="background-deck">
            <MainHeader activate={4}/>
            <MarketPlaceHeader activate={0}/>   
            <main className="game-body-deck">
                <div className="deck-deck-display h-96 overflow-y-auto p-4 bg-gray-100 rounded-xl shadow-md">
                    <div className="grid grid-cols-5 gap-5">
                        {FAKE_DECK.map((card, id: number ) => (

                            <div key={id} className="deck-card-display" style={{backgroundImage: `url(/cards/${card.id}.png)`}}>
                                
                            </div>
                            
                        ))}
                    </div>
                </div>
                <div className="deck-button-container">
                    <button className="deck-button">
                        Ajouter des cartes
                    </button>
                    <button className="deck-button">
                        Test
                    </button>
                </div>
            </main>
        </div> 
    )
}

export default DeckPage;