import { PoolConfig } from "pg";

export const timescaleConfig: PoolConfig = {
    user: process.env.TSDB_USER || "admin",
    host: process.env.TSDB_HOST || "127.0.0.1",
    database: process.env.TSDB_DATABASE || "rf",
    password: process.env.TSDB_PASSWORD || "admin",
    port: Number(process.env.TSDB_PORT) || 7363, 
};

export default timescaleConfig;