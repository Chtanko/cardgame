'use client';

import React from "react";
import { ReactNode } from "react";
import { AnimatePresence, motion } from 'framer-motion';
import { usePathname } from "@/i18n/routing";


export const AnimationProvider : React.FC<{ children: ReactNode }> = ({ children }) => {
    const pathname = usePathname();

    return(
        <AnimatePresence mode="wait">
            <motion.div
                key={pathname}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ duration: 0.8 }} // Durée du fondu
                className="relative"
            >
                {children}
            </motion.div>
        </AnimatePresence>
    );
} 