import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useGlobalContext } from '../context';
import { CustomButton, PageHOC } from '../components';
import styles from '../styles';
import { io } from "socket.io-client";

const socket = io('http://localhost:3001');

const JoinBattle = () => {
  const navigate = useNavigate();
  const { contract, gameData, setShowAlert, setBattleName, setErrorMessage, walletAddress } = useGlobalContext();

  useEffect(() => {
    if (gameData?.activeBattle?.battleStatus === 1) {
        navigate(`/battle/${gameData.activeBattle.name}`);
    }

  socket.on('battleStarted', (data) => {
      if (data.players.some(player => player.socketId === socket.id)) {
        setShowAlert({ status: true, type: 'success', message: 'Battle started!' });
        navigate(`/battle/${data.battleName}`);
      }
  });

  socket.on('battleFull', (data) => {
        setShowAlert({ status: true, type: 'error', message: data.message });

  });

      return () => {
          socket.off('battleStarted');
          socket.off('battleFull');
      };
  }, [gameData, navigate, setShowAlert]);

  const handleClick = async (battleName) => {
    setBattleName(battleName);

    try {
      await contract.joinBattle(battleName);

      socket.emit('joinBattle', {
        battleName,
        playerName: walletAddress, // Utilise l'adresse du portefeuille comme nom du joueur
      });

      setShowAlert({ status: true, type: 'success', message: `Joining ${battleName}` });

    } catch (error) {
      setErrorMessage(error);
    }
  };

  return (
    <>
      <h2 className={styles.joinHeadText}>Available Battles:</h2>

      <div className={styles.joinContainer}>
        {gameData.pendingBattles.length
          ? gameData.pendingBattles
            .filter((battle) => !battle.players.includes(walletAddress) && battle.battleStatus !== 1)
            .map((battle, index) => (
              <div key={battle.name + index} className={styles.flexBetween}>
                <p className={styles.joinBattleTitle}>{index + 1}. {battle.name}</p>
                <CustomButton
                  title="Join"
                  handleClick={() => handleClick(battle.name)}
                />
              </div>
            )) : (
              <p className={styles.joinLoading}>Reload the page to see new battles</p>
          )}
      </div>

      <p className={styles.infoText} onClick={() => navigate('/create-battle')}>
        Or create a new battle
      </p>
    </>
  );
};

export default PageHOC(
  JoinBattle,
  <>Join <br /> a Battle</>,
  <>Join already existing battles</>,
);