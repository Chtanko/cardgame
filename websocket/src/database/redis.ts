import Redis from "ioredis";
import {redisConfig} from '@config/redisOptions';

export const redisClient = new Redis(redisConfig);

redisClient.on("connect", () => {
  console.log("Connected to Redis");
});

redisClient.on("error", (err: Error) => {
  console.error("Redis connection error:", err);
});


redisClient.on('end', () => {
  console.error('Redis connection lost.');
  setTimeout(() => redisClient.connect(), 5000);
});

export default redisClient;