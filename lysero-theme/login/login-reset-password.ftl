<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reset Password</title>
</head>
<body>
    <h1>Reset Your Password</h1>
    <form id="kc-reset-password-form" action="${url.loginAction}" method="post">
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" placeholder="Enter your email" required>
        <button type="submit">Send Reset Email</button>
    </form>
</body>
</html>
