import { getDefaultConfig } from '@rainbow-me/rainbowkit';
import { polygon } from "wagmi/chains";

export const config = getDefaultConfig({
  appName: 'RealmFactions',
  projectId: process.env.PROJECT_ID || "510c7b934b0e80c5f2ba305015161983",
  chains: [polygon],
  ssr: true, // If your dApp uses server side rendering (SSR)
});