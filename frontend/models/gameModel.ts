export interface Card {
    id: string,
    cardId: string,
    mana: number,
    attack: number,
    life: number,
    lifeMax: number,
    havePlayed: boolean
}

export interface CardRaw {
    cardid: string,
    mana: number,
    attack: number,
    life: number
}

export interface PlayerState {
    playerId: string;
    mana: number;
    life: number,
    deck: Card[]; 
    deckLength: number;
    hand: Card[]; 
    handLength: number;
    board: Card[]; 
    grave: Card[];
    graveLength: number;
}
  
export enum RoomStatus {
    Empty="empty",
    Incomplete="incomplete",
    Full="full",
    Finish="finish"
}

export interface ActionResult {
    action: any;
    message: MessageResult;
}

export enum MessageResult {
    NotYourTurn="notYourTurn",
    IsYourTurn="isYourTurn",
    EndTurn="endTurn",
    CardNotInHand="cardNotInHand",
    NotEnoughMana="notEnoughMana",
    PlayCard="playCard",
    CardNotInBoard="cardNotInBoard",
    CardNotInTargetBoard="cardNotInTargetBoard",
    CardAttack="cardAttack",
    CardInTargetBoard="cardInTargetBoard",
    PlayerAttack="playerAttack",
    EndGame="endGame",
    NoDeck="noDeck",
    NoRoom="noRoom",
    HaveRoom="haveRoom",

    GetDeck="getDeck",
    PlayerIsInRoom="playerIsInRoom",
    RoomNotFound="roomNotFound",
    CardHaveAlreadyPlayed="cardHaveAlreadyPlayed"
}

export interface Room {
    roomId: string;
    players: string[];
    status: RoomStatus;
    turn: number;
    playerStates: Record<string, PlayerState>; 
    dateInsert: Date,
    dateUpdate: Date
}

export interface ChatMessage{
    message: string;
    color?: string;
    date?: Date;
    sender: string;
}
