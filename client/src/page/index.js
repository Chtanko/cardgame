import Home from "./Home";
import CreateBattle from "./CreateBattle";
import JoinBattle from "./JoinBattle";
import Battle from "./Battle";
import Battleground from "./Battleground";
import Lobby from "./Lobby";
import Duel from "./Duel";

export{
    Home,
    CreateBattle,
    JoinBattle,
    Battle,
    Battleground,
    Lobby,
    Duel
}