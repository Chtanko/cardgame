<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Terms and Conditions</title>
</head>
<body>
    <h1>Terms and Conditions</h1>
    <p>You must accept the terms and conditions to continue.</p>
    <form id="kc-terms-form" action="${url.loginAction}" method="post">
        <button type="submit" name="accept" value="yes">Accept</button>
        <button type="submit" name="decline" value="no">Decline</button>
    </form>
</body>
</html>
