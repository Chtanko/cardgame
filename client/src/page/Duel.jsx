import { useEffect, useState } from "react";
import { useParams, useNavigate } from 'react-router-dom';
import { useSocket } from '../context/socketContext';
import { gameEvents } from '../sockets/events/game'
import backgroundImage from '../assets/background/astral.jpg';
import {CardComponent, CardComponentEnnemy, CardComponentBoard, CardComponentBoardEnnemy} from "../components/CardComponent";

const containerStyle = {
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: '200vh 100vh',
    display: 'flex',
    flexDirection: 'column',
    height: '100vh',
    width: '200vh',
    color: "white"
};

const boardStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '5px',
    flexWrap: 'wrap',
    height: '100%',
};
  
const cardStyle = {
    width: '100px',
    height: '125px',
    borderRadius: '5px',
    backgroundSize: 'cover',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '12px',
    textAlign: 'center',
    cursor: 'pointer',
    color: 'black',
};
  
const statsBarStyle = {
    display: 'flex',
    flexDirection: 'row', // Alignement en ligne
    justifyContent: 'space-around', // Espacement uniforme
    alignItems: 'center', // Alignement vertical centré
    height: '100%',
    color: 'white',
    gap: '20px', // Espace entre les éléments
  };
  
  const progressBarContainer = {
    width: '150px', // Largeur de chaque barre de progression
    height: '20px',
    backgroundColor: '#ccc',
    borderRadius: '10px',
    margin: '0 10px', // Marges horizontales pour espacer les barres
  };

  const endTurnStyle = {
    width: '100px', // Largeur de chaque barre de progression
    height: '100px',
    backgroundColor: '#ccc',
    borderRadius: '10px',
  };
  
  const progressBar = (percentage, color) => ({
    height: '100%',
    width: `${percentage}%`,
    backgroundColor: color,
    borderRadius: '10px',
  });

const isValidUUID = (roomId) => {
    const uuidRegex = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/;
    return uuidRegex.test(roomId);
};

const Duel = () => {
    const { roomId } = useParams();
    const { socket, isConnected } = useSocket();
    const navigate = useNavigate();

    const [room, setRoom] = useState(null);

    const [playerOneLifePercent, setPlayerOneLifePercent] = useState(100);
    const [playerTwoLifePercent, setPlayerTwoLifePercent] = useState(100);
    const [playerOneLife, setPlayerOneLife] = useState(30);
    const [playerTwoLife, setPlayerTwoLife] = useState(30);

    const [playerOneManaPercent, setPlayerOneManaPercent] = useState(100);
    const [playerTwoManaPercent, setPlayerTwoManaPercent] = useState(100);
    const [playerOneMana, setPlayerOneMana] = useState(10);
    const [playerTwoMana, setPlayerTwoMana] = useState(10);

    const [playerOneHand, setPlayerOneHand] = useState([]);
    const [playerTwoHand, setPlayerTwoHand] = useState([]);

    const [playerOneBoard, setPlayerOneBoard] = useState([]);
    const [playerTwoBoard, setPlayerTwoBoard] = useState([]);
  
    const [isYourTurn, setIsYourTurn] = useState(true);

    const playerId = "22222222-2222-2222-2222-222222222222";
    const [playerTwoId, setPlayerTwoId] = useState("");

    const [cardSelected, setCardSelected] = useState(undefined);

    const handlers = {
        roomJoinedHandler: (message) => {
            if (message.room.status === "full") {
                setRoom(message.room);

                
                setPlayerOneLifePercent(message.room.playerStates[playerId].life * 100 / 30);
                setPlayerOneManaPercent(message.room.playerStates[playerId].mana * 100 / 10);
                setPlayerOneHand(message.room.playerStates[playerId].hand);
                setPlayerOneBoard(message.room.playerStates[playerId].board);
                setPlayerOneLife(message.room.playerStates[playerId].life);
                setPlayerOneMana(message.room.playerStates[playerId].mana);

                const defaultPlayerTwoId = message.room.players.filter(item => item !== playerId)[0];

                setPlayerTwoId(defaultPlayerTwoId);

                setPlayerTwoLifePercent(message.room.playerStates[defaultPlayerTwoId].life * 100 / 30);
                setPlayerTwoManaPercent(message.room.playerStates[defaultPlayerTwoId].mana * 100 / 10);
                setPlayerTwoHand(message.room.playerStates[defaultPlayerTwoId].hand);
                setPlayerTwoBoard(message.room.playerStates[defaultPlayerTwoId].board);
                setPlayerTwoLife(message.room.playerStates[defaultPlayerTwoId].life);
                setPlayerTwoMana(message.room.playerStates[defaultPlayerTwoId].mana);
                

                setIsYourTurn(message.room.turn % 2 === 0);

            }else {
                navigate("/lobby");
            }
        },
        gameActionHandler : (message) => {
            console.log("handler");
            setRoom(message.room);


            setPlayerOneLifePercent(message.room.playerStates[playerId].life * 100 / 30);
            setPlayerOneManaPercent(message.room.playerStates[playerId].mana * 100 / 10);
            setPlayerOneHand(message.room.playerStates[playerId].hand);
            setPlayerOneBoard(message.room.playerStates[playerId].board);
            setPlayerOneLife(message.room.playerStates[playerId].life);
            setPlayerOneMana(message.room.playerStates[playerId].mana);

            const defaultPlayerTwoId = message.room.players.filter(item => item !== playerId)[0];

            setPlayerTwoId(defaultPlayerTwoId);

            setPlayerTwoLifePercent(message.room.playerStates[defaultPlayerTwoId].life * 100 / 30);
            setPlayerTwoManaPercent(message.room.playerStates[defaultPlayerTwoId].mana * 100 / 10);
            setPlayerTwoHand(message.room.playerStates[defaultPlayerTwoId].hand);
            setPlayerTwoBoard(message.room.playerStates[defaultPlayerTwoId].board);
            setPlayerTwoLife(message.room.playerStates[defaultPlayerTwoId].life);
            setPlayerTwoMana(message.room.playerStates[defaultPlayerTwoId].mana);

            setIsYourTurn(message.room.turn % 2 === 0);
        },
        noRoomHandler : (message) => {
            navigate("/lobby");
        }
    };

    useEffect(() => {
        if (!socket) return;
        
        const {cleanup} = gameEvents(socket, handlers);

        console.log(socket);
        return () => {
            cleanup();
        };
    }, [socket]);

    useEffect(() => {
        if(!isConnected) return;
        if(!isValidUUID(roomId)) {
            navigate("/lobby");
            return;
        }
        socket.emit("loadGame", {"playerId": playerId, "roomId":roomId});

    }, [isConnected]);

    const placeCard = (card) => {
      if(room.playerStates[playerId].mana < card.mana){
        console.log(`mana: ${room.playerStates[playerId].mana}, card: ${card.mana}`);
        return false;
      }
      console.log(card);
      socket.emit("playCard", {"playerId": playerId, "roomId":roomId, "card": card});
      return true;
    }

    const endTurn = () => {
      socket.emit("endTurn", {"playerId": playerId, "roomId":roomId});
    }

    const attackCard = (targetCard) => {
      if(targetCard === undefined) return;
      if(cardSelected === undefined) return;
      socket.emit("attackCard", {"playerId" : playerId,"roomId": roomId, "card":cardSelected, "targetPlayerId": playerTwoId, "targetCard": targetCard});
      setCardSelected(undefined);
    }

    const selectCard = (card) => {
      console.log(card)
      if(card === undefined) return;
      setCardSelected(card)
    }

    return (
      <div style={containerStyle}>
      {/* Barre verte en haut : Stats */}
      <div id="player_2" style={{ backgroundColor: 'transparent', height: '5%' }}>
        <div style={statsBarStyle}>
          <div>
            <br/>
            <br/>
            <div style={progressBarContainer}>
              <div style={progressBar(playerTwoLifePercent, 'red')}>{playerTwoLife}/30</div>
            </div>
            <br/>
            <div style={progressBarContainer}>
              <div style={progressBar(playerTwoManaPercent, 'blue')}>{playerTwoMana}/10</div>
            </div>
          </div>
          <div></div>
          
        </div>
      </div>

      {/* Barre bleue : Cartes en main */}
      <div id="hand_2" style={{ backgroundColor: 'transparent', height: '15%' }}>
        <div style={boardStyle}>
          {playerTwoHand.map((card, index) => (
            <CardComponentEnnemy key={index} card={card} position={{x: 800+index*60, y: -40}}/>
          ))}
        </div>
      </div>

      {/* Barre rouge : Cartes sur le board */}
      <div id="board_2" style={{ backgroundImage: `url(/grid.png)`, width: "80%", marginLeft:"10%", backgroundColor: 'transparent', height: '30%' }}>
        <div style={boardStyle}>
          {playerTwoBoard.map((card, index) => (
            <CardComponentBoardEnnemy key={index} card={card} position={{x: 700+index*140, y: 250}}  attackCard={attackCard}/>
          ))}
        </div>
      </div>

      {/* Deuxième barre rouge : Éventuellement pour autre chose */}
      <div id="board" style={{ backgroundImage: `url(/grid.png)`, width: "80%", marginLeft:"10%", backgroundColor: 'transparent', height: '30%' }}>
        <div style={boardStyle}>
          {playerOneBoard.map((card, index) => (
            <CardComponentBoard key={index} card={card} position={{x: 700+index*140, y: 500}} selectCard={selectCard}/>
          ))}
        </div>
      </div>

      {/* Deuxième barre bleue : Cartes en main (seconde zone) */}
      <div id="hand" style={{ backgroundColor: 'transparent', height: '15%' }}>
        <div style={boardStyle}>
          {playerOneHand.map((card, index) => (
            <CardComponent key={index} card={card} position={{x: 650+index*110, y: 720}} placeCard={placeCard}/>
          ))}
        </div>
      </div>

      {/* Deuxième barre verte en bas : Stats */}
      <div id="player" style={{ backgroundColor: 'transparent', height: '5%' }}>
        <div style={statsBarStyle}>
          <div>
            <div style={progressBarContainer}>
              <div style={progressBar(playerOneLifePercent, 'red')}>{playerOneLife}/30</div>
            </div>
            <br/>
            <div style={progressBarContainer}>
              <div style={progressBar(playerOneManaPercent, 'blue')}>{playerOneMana}/10</div>
            </div>
            <br/>
            <br/>
          </div>
          <div>
            {
              isYourTurn && (
                <button style={endTurnStyle} onClick={endTurn}>End turn</button>
              )
            }
            <br/>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
      </div>
    </div>
    );
}

export default Duel;