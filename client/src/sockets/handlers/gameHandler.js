export const noDeckHandler = (message) => {
    console.log(message);
}
export const endTurnHandler = (message) => {
    console.log(message);
}
export const notYourTurnHandler = (message) => {
    console.log(message);
}
export const endGameHandler = (message) => {
    console.log(message);
}
export const cardNotInBoardHandler = (message) => {
    console.log(message);
}
export const cardNotInTargetBoardHandler = (message) => {
    console.log(message);
}
export const cardInTargetBoardHandler = (message) => {
    console.log(message);
}
export const cardAttackHandler = (message) => {
    console.log(message);
}
export const playerAttackHandler = (message) => {
    console.log(message);
}
export const playCardHandler = (message) => {
    console.log(message);
}

export const notEnoughManaHandler = (message) => {
    console.log(message);
}

export const cardNotInHandHandler = (message) => {
    console.log(message);
}