'use client';

import { useEffect, useState } from "react";
import { useSocket } from '@/context/socketContext';
import { roomEvents } from '@/sockets/events/game'
import { useRouter } from '@/i18n/routing';
import { RoomStatus } from "@/models/gameModel";
import { RoomHandlers, HandlerMessageToServer } from "@/models/handlerModel";
import { useTranslations } from "next-intl";
import "../multiplayer.css";
import { MultiplayerHeader, MainHeader } from "@/components/HeaderComponent";
import { MusicEnum, SoundEnum, useAudio } from "@/context/audioContext";
import { SlideButtonContainer } from "@/components/SlideButton";
import { motion } from "framer-motion";
import { useAccount } from "wagmi";
import {FAKE_DECK} from "@/models/constant";

const VersusPage = () => {
    const t = useTranslations('Global');
    const { socket, isConnected } = useSocket();
    const [roomStatus, setRoomStatus] = useState<string>(RoomStatus.Empty);
    const [roomId, setRoomId] = useState<string>("");
    const account = useAccount();    
    const router = useRouter();
    const [startText, setStartText] = useState(t('RESEARCH'));
    const [activeIndex, setActiveIndex] = useState<number>(0); 
    const { playSoundEffect, playSoundInterfaceEffect, playMusic, stopMusic } = useAudio();
    const [countDown, setCountDown] = useState(false);
    const [animate, setAnimate] = useState(false);

    const [countdownTime, setCountdownTime] = useState(8);

    const slideButton = {
        height: 50,
        width: 600,
        button: [{
            text: "AI_MODE",
            action: changeMode
        }, {
            text: "CLASSIC_MODE",
            action: changeMode
        }, {
            text: "PLAY_TO_EARN",
            action: changeMode
        }]
    }
    function changeMode(idx: number){
        setActiveIndex(idx)
        playSoundInterfaceEffect(SoundEnum.INTERFACE_0);
    }

    const handlers : RoomHandlers = {
        defaultLobbyHandler: {
            setRoomStatus: setRoomStatus,
            setRoomId: setRoomId,
            router: router
        },
        roomJoinedHandler: {
            setRoomStatus: setRoomStatus,
            setRoomId: setRoomId,
            setCountDown: setCountDown
        },
        quitGameToClientHandler: {
            setRoomStatus: setRoomStatus,
            setRoomId: setRoomId,
        }
    };

    useEffect(() => {
        if (!socket) return;
        const { cleanup } = roomEvents(socket, handlers);
            
        return () => {
            cleanup();
        };
    }, [socket]);

    useEffect(() => {
        if(!isConnected) return;
        if(!socket) return;
        if(!account.isConnected) return;
        socket.emit(HandlerMessageToServer.PlayerIsInGame, {"playerId": account.address});
    }, [isConnected, account.isConnected]);
    
    useEffect(() =>{
        if(countDown === false)return;
        stopMusic();
        playMusic(MusicEnum.TRANSITION);
        playSoundEffect(SoundEnum.TRANSITION_1);
        const timer = setTimeout(() => {
            playSoundEffect(SoundEnum.TRANSITION_0);
            router.replace(`/duel/${roomId}`);
        }, 8000);

        const interval = setInterval(() => {
            setCountdownTime((prev) => {
              if (prev <= 1) {
                clearInterval(interval); // Stop l'intervalle à 0
              }
              return prev - 1;
            });
          }, 1000);
    
        return () => {
            clearTimeout(timer);
            clearInterval(interval);
        }
    }, [countDown]);

    useEffect(() => {
        if(countDown === false)return;
        if (countdownTime > 0) {
          const timer = setTimeout(() => {
            setCountdownTime((prev) => prev - 1);
          }, 1000);
    
          // Déclenche l'animation à chaque changement
          setAnimate(true);
          const resetAnimation = setTimeout(() => setAnimate(false), 500); // Réinitialisation après 0.5s
    
          return () => {
            clearTimeout(timer);
            clearTimeout(resetAnimation);
          };
        }
      }, [countdownTime]);

    const handleJoinGame = () => {

        if (!isConnected || !socket) {
            playSoundEffect(SoundEnum.ERROR);
            console.error('Socket not connected!');
            return;
        }

        if(account.address === undefined){
            playSoundEffect(SoundEnum.ERROR);
            console.error('Account not connected!');
            return;
        }
        const send = {
            "playerId": account.address,
        }
        playSoundEffect(SoundEnum.START_1);

        // Envoyer le message 'join game' au serveur
        socket.emit(HandlerMessageToServer.JoinDemoGame, send);
        console.log('Join game message sent:', send);

    };

    const handleLeaveGame = () => {
        if(roomId === ""){ 
            playSoundEffect(SoundEnum.ERROR);
            return; // afficher une erreur
        }
        if (isConnected && socket) {
            playSoundEffect(SoundEnum.CANCEL);
            const send = {
                "playerId": account.address,
                "roomId": roomId
            }
        
            // Envoyer le message 'join game' au serveur
            socket.emit(HandlerMessageToServer.LeaveGame, send);
            console.log('Join game message sent:', send);
        } else {
            playSoundEffect(SoundEnum.ERROR);
            console.error('Socket not connected!');
        }
      };

    return(
        <div className="background">
            <MainHeader activate={1}/>
            <MultiplayerHeader activate={1}/>
            <main className="game-body-multiplayer">
                <div className={`turn-indicator-container ${countDown ? "show" : ""}`}>
                    <h1 className={`turn-indicator text-9xl font-bold mb-4 ${animate ? "animate-forward" : ""}`}>
                    {countdownTime}
                    </h1>
                </div>
                <div className="deck-menu">
                    <div>
                        <h1 className="h1-menu">
                           {t("CLASSIC_MODE")}
                        </h1>
                        <p className="p-menu">
                            {t("VERSUS_DESCRIPTION")}
                        </p>
                    </div>
                    <div className="deck-display">
                        {FAKE_DECK.map((card, id: number ) => (
                            <div key={id} className="card-display" style={{backgroundImage: `url(/cards/${card.id}.png)`}}>
                                
                            </div>
                        ))}
                    </div>
                    <div className="menu-button-container">
                        <button className="menu-button" onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_0)}>
                            {t("CHOOSE_DECK")}
                        </button>
                        <button className="menu-button" onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_0)}>
                            {t("MODIFY_DECK")}
                        </button>
                    </div>
                    <SlideButtonContainer slideButton={slideButton} activeIndex={activeIndex}/>

                    {roomStatus === RoomStatus.Empty && (
                        <button className="start-button" onClick={handleJoinGame}>
                            {t('JOIN_BATTLE')}
                        </button>
                    )}
                    {roomStatus === RoomStatus.Incomplete && (
                        <button className={`cancel-button`} onClick={handleLeaveGame}
                        onMouseEnter={() => { setStartText(t('CANCEL'));}}
                        onMouseLeave={() => { setStartText(t('RESEARCH')); }}>
                            <>
                            {startText} 
                            <span className="flex">
                                <motion.span
                                className="inline-block"
                                animate={{ y: [0, -2, 0] }} // Animation de saut
                                transition={{ repeat: Infinity, duration: 0.5, delay: 0.1 }}
                                >
                                .
                                </motion.span>
                                <motion.span
                                className="inline-block"
                                animate={{ y: [0, -2, 0] }} // Animation de saut
                                transition={{ repeat: Infinity, duration: 0.5, delay: 0.3 }}
                                >
                                .
                                </motion.span>
                                <motion.span
                                className="inline-block"
                                animate={{ y: [0, -2, 0] }} // Animation de saut
                                transition={{ repeat: Infinity, duration: 0.5, delay: 0.4 }}
                                >
                                .
                                </motion.span>
                            </span>
                            </>
                        </button>
                    )}
                    {roomStatus === RoomStatus.Full && (
                        <p className={`full-button`}>{t('FULL')}</p>
                    )}         
                </div>
                <div className="character-menu">
                    {/* <img 
                        src="/animations/an_citizen.gif" 
                        alt="GIF animé" 
                        className="gif-container"
                    /> */}
                </div>
            </main>
        </div> 
    )
}

export default VersusPage;