import './polyfills';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { OnboardModal } from './components';
import {Home, CreateBattle, JoinBattle, Battle, Battleground, Lobby, Duel} from './page';
import { GlobalContextProvider } from './context';
import { SocketProvider } from './context/socketContext';

import './index.css';
import 'core-js/features/bigint';

ReactDOM.createRoot(document.getElementById('root')).render(
  <SocketProvider>
    <BrowserRouter>
      <GlobalContextProvider>
        <OnboardModal />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/lobby" element={<Lobby/>}/>
          <Route path="/duel/:roomId" element={<Duel/>}/>
          <Route path="/create-battle" element={<CreateBattle />} />
          <Route path="/join-battle" element={<JoinBattle />} />
          <Route path="/battleground" element={<Battleground />} />
          <Route path="/battle/:battleName" element={<Battle />} />
        </Routes>
      </GlobalContextProvider>
    </BrowserRouter>
  </SocketProvider>
);
