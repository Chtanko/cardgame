import { SlideButtonDefinition, SlideButtonType } from "@/models/componentModel";
import { useTranslations } from "next-intl";
import "./slideButton.css";
import { useEffect } from "react";

type SlideButtonContainerProps = {
    slideButton: SlideButtonType;
    activeIndex: number;
}

export const SlideButtonContainer = ({slideButton, activeIndex}: SlideButtonContainerProps) => {
    useEffect(() => {
        if(slideButton.button.length <= 1){
            console.error("MORE 1 BUTTON");
        }
    }, [slideButton]);

    

    // const _width = (slideButton.width - (slideButton.button.length - 1) * slideButton.button.length) / slideButton.button.length;
    // const width = _width + (_width * (0.15 / 2)) *(slideButton.button.length-1);
    // const left = ((slideButton.button.length - 2) * 4) + _width - (_width * (0.15 / 2));
    // const y = slideButton.button.length * _width + (slideButton.button.length - 1) * 2;
    // console.log(y);
    // console.log(_width);

    const width = slideButton.width /  slideButton.button.length;
    //const _width = (slideButton.width - (slideButton.button.length - 1) * 2) / slideButton.button.length;

    return(
        <div className="button-container"
            style={{width: `${slideButton.width}px`, height: `${slideButton.height}px`}}>
            {
                slideButton.button.map((btn, idx) => (
                    <SlideButton key={idx} slideButton={btn} 
                        position={idx} size={slideButton.button.length} 
                        left={idx * (width)}
                        width={width} activeIndex={activeIndex}/>
                ))
            }
        </div>
    )
}

type SlideButtonProps = {
    size: number;
    position: number;
    left: number;
    width: number;
    activeIndex: number;
    slideButton: SlideButtonDefinition;
}

const SlideButton = ({slideButton, size, position, left, width, activeIndex}: SlideButtonProps) => {
    const t = useTranslations('Global');
    let value = "left";
    if(position == 0){
        value = "left";
    }else if(position == size-1){
        value = "right";
    }else {
        value = "middle";
    }
    return(
        <button className={`${activeIndex === position ? "custom-button-activate" : "custom-button"} ${value}`} 
                style={{width: `${width}px`, zIndex: position+1, left: `${left}px`}}
                onClick={() => slideButton.action(position)}>
            {t(slideButton.text)}
        </button>
    )
}