import type { Config } from "jest";

const config: Config = {
    preset: "ts-jest", // Utiliser ts-jest pour compiler les fichiers TS
    testEnvironment: "node", // Environnement Node.js pour Jest
    testMatch: ["**/tests/**/*.test.ts"], // Rechercher les fichiers de test
    moduleFileExtensions: ["ts", "js"],
    moduleDirectories: ['node_modules', 'src'],
    clearMocks: true, // Nettoyer les mocks après chaque test
    verbose: true, // Activer la sortie détaillée
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/src/$1", 
        '^@models/(.*)$': '<rootDir>/src/models/$1',
        '^@services/(.*)$': '<rootDir>/src/services/$1',
        '^@database/(.*)$': '<rootDir>/src/database/$1',
        '^@config/(.*)$': '<rootDir>/src/config/$1',
        '^@sockets/(.*)$': '<rootDir>/src/sockets/$1'
    },

    modulePaths: [
        "<rootDir>/src"
    ],

    roots: ['<rootDir>/src', '<rootDir>/tests'],
    transform: {
    '^.+\\.tsx?$': 'ts-jest'
    },
};

export default config;
