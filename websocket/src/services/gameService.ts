import { Room, MessageResult, Card, ActionResult, RoomStatus, PlayerState } from "@models/gameModel";
import { v4 as uuidv4 } from 'uuid';

export const MAX_PLAYER = 2;
export const MAX_MANA = 10;

export const isTurn = (room: Room, playerId: string): MessageResult => {
  if (room.turn % MAX_PLAYER !== room.players.indexOf(playerId)) {
    return MessageResult.NotYourTurn;
  }
  return MessageResult.IsYourTurn
}

export function resetPlayerStatesExcept(
  playerStates: Record<string, PlayerState>,
  currentPlayerId: string
): Record<string, PlayerState> {
  const updatedStates = { ...playerStates };

  for (const playerId in updatedStates) {
    if (playerId !== currentPlayerId) {
      updatedStates[playerId] = {
        ...updatedStates[playerId],
        deck: [],
        hand: [],
        grave: [],
      };
    }
  }

  return updatedStates;
}

export function resetPlayerStatesWithout(
  playerStates: Record<string, PlayerState>,
  currentPlayerId: string
): Record<string, PlayerState> {
  const updatedStates = { ...playerStates };

  for (const playerId in updatedStates) {
    if (playerId === currentPlayerId) {
      updatedStates[playerId] = {
        ...updatedStates[playerId],
        deck: [],
        hand: [],
        grave: [],
      };
    }
  }

  return updatedStates;
}


export const drawCards = (deck: Card[], cardsToDraw: number): { drawnCards: Card[], updatedDeck: Card[] } => {
  const deckCopy = [...deck];
  
  deckCopy.forEach(card => {
    card.id = uuidv4(); 
    card.lifeMax = card.life;
    card.havePlayed = false
  });

  for (let i = deckCopy.length - 1; i > 0; i--) {
    const randomIndex = Math.floor(Math.random() * (i + 1));
    [deckCopy[i], deckCopy[randomIndex]] = [deckCopy[randomIndex], deckCopy[i]];
  }

  const drawnCards = deckCopy.slice(0, cardsToDraw);
  const updatedDeck = deckCopy.slice(cardsToDraw);

  return { drawnCards, updatedDeck };
};

export const endTurn = (room: Room, playerId: string): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) 
    return { action: room, message: MessageResult.NotYourTurn};

  room.playerStates[playerId].mana = Math.min(room.playerStates[playerId].mana + 1, MAX_MANA);

  if(room.playerStates[playerId].deck.length === 0){
    room.playerStates[playerId].deck = [...room.playerStates[playerId].grave]
    room.playerStates[playerId].grave = []
    room.playerStates[playerId].graveLength = 0;
  }
  const { drawnCards, updatedDeck } = drawCards(room.playerStates[playerId].deck, 1);

  room.playerStates[playerId].hand = room.playerStates[playerId].hand.concat(drawnCards);
  room.playerStates[playerId].deck = updatedDeck;
  room.playerStates[playerId].deckLength = room.playerStates[playerId].deck.length;
  room.playerStates[playerId].handLength = room.playerStates[playerId].hand.length;

  room.playerStates[playerId].board.map((element: Card, index: number) => {
    room.playerStates[playerId].board[index].havePlayed = false;
  });

  room.turn += 1;

  return { action: room, message: MessageResult.EndTurn};
};

export const playCard = (room: Room, playerId: string, cardId: string): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) return { action: room, message: MessageResult.NotYourTurn };

  const card = room.playerStates[playerId].hand.find(c => c.id === cardId);

  if (card === undefined) {
    return  { action: room, message: MessageResult.CardNotInHand };
  }

  if (room.playerStates[playerId].mana < card.mana) {
    return  { action: room, message: MessageResult.NotEnoughMana };
  }

  room.playerStates[playerId].hand = room.playerStates[playerId].hand.filter((c: Card) => c.id !== card.id);
  room.playerStates[playerId].board.push(card);
  room.playerStates[playerId].mana -= card.mana;
  room.playerStates[playerId].handLength = room.playerStates[playerId].hand.length;

  console.log(`${playerId} a joué la carte ${card}`);

  return { action: room, message: MessageResult.PlayCard };
};

export const attackCard = (room: Room, playerId: string, cardId: string, targetCardId: string, targetPlayerId: string): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) 
    return { action: room, message: MessageResult.NotYourTurn };

  const card = room.playerStates[playerId].board.find(c => c.id === cardId);

  if (card === undefined) {
    return { action: room, message: MessageResult.CardNotInBoard };
  }

  const targetCard = room.playerStates[targetPlayerId].board.find(c => c.id === targetCardId);

  if (targetCard === undefined) {
    return { action: room, message: MessageResult.CardNotInTargetBoard };
  }

  const cardIndex = room.playerStates[playerId].board.findIndex((c: Card) => c.id === cardId);

  if(room.playerStates[playerId].board[cardIndex].havePlayed === true){
    return { action: room, message: MessageResult.CardHaveAlreadyPlayed };
  }

  const targetIndex = room.playerStates[targetPlayerId].board.findIndex((c: Card) => c.id === targetCard.id);

  room.playerStates[targetPlayerId].board[targetIndex].life -= card.attack;
  room.playerStates[playerId].board[cardIndex].havePlayed = true;

  if(room.playerStates[targetPlayerId].board[targetIndex].life <= 0){
    if (targetIndex > -1) { 
      room.playerStates[targetPlayerId].board.splice(targetIndex, 1);
      targetCard.life = targetCard.lifeMax;
      room.playerStates[targetPlayerId].grave.push(targetCard);
      room.playerStates[targetPlayerId].graveLength = room.playerStates[targetPlayerId].grave.length;
    }
  }

  console.log(`${playerId} attaque avec ${card} contre ${targetCard}`);
  return { action: room, message: MessageResult.CardAttack };
};

export const attackPlayer = (room: Room, playerId: string, cardId: string, targetPlayerId: string): ActionResult => {
  if(isTurn(room, playerId) === MessageResult.NotYourTurn) 
    return { action: room, message: MessageResult.NotYourTurn};

  const card = room.playerStates[playerId].board.find(c => c.id === cardId);

  if (card === undefined) {
    return { action: room, message: MessageResult.CardNotInBoard };
  }

  if (room.playerStates[targetPlayerId].board.length >= 1){
    return { action: room, message: MessageResult.CardInTargetBoard };
  }

  const cardIndex = room.playerStates[playerId].board.findIndex((c: Card) => c.id === cardId);

  if(room.playerStates[playerId].board[cardIndex].havePlayed === true){
    return { action: room, message: MessageResult.CardHaveAlreadyPlayed };
  }

  room.playerStates[targetPlayerId].life -= card.attack;
  room.playerStates[playerId].board[cardIndex].havePlayed = true;
  console.log(`${playerId} attaque avec ${card} contre ${targetPlayerId}`);

  if (room.playerStates[targetPlayerId].life <= 0) {
    room.status = RoomStatus.Finish;
    return { action: room, message:  MessageResult.EndGame };
  }

  return { action: room, message: MessageResult.PlayerAttack };
};

export default undefined;
