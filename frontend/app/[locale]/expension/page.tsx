'use client';

import { useTranslations } from "next-intl";
import { MainHeader } from "@/components/HeaderComponent";
import "./expension.css";
import { Scene } from "@/components/Scene";

const ExpensionPage = () => {
    const t = useTranslations('Global');

    return(
        <div className="background-expension">
            <MainHeader activate={2}/>
            <main className="game-body-expension">
              <div className="div-expension">
              <Scene />
              </div>
            </main>
        </div> 
    )
}

export default ExpensionPage;