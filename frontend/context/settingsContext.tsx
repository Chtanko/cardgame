'use client';

import { useState, SetStateAction, createContext, useContext, useEffect }from "react";


export type SettingsContextType = {
    masterVolume: number;
    musicVolume: number;
    ambiantVolume: number;
    interfaceVolume: number;
    effectVolume: number;
    voiceVolume: number;
    updateMasterVolume: (volume: number) => void;
    updateMusicVolume: (volume: number) => void;
    updateAmbiantVolume: (volume: number) => void;
    updateInterfaceVolume: (volume: number) => void;
    updateEffectVolume: (volume: number) => void;
    updateVoiceVolume: (volume: number) => void;

};

const SettingsContext = createContext<SettingsContextType | undefined>(undefined);


export const SettingsProvider = ({ children }: { children: React.ReactNode }) => {
    const [masterVolume, setMasterVolume] = useState<number>(1);
    const [musicVolume, setMusicVolume] = useState<number>(1);
    const [ambiantVolume, setAmbiantVolume] = useState<number>(1);
    const [interfaceVolume, setInterfaceVolume] = useState<number>(1);
    const [effectVolume, setEffectVolume] = useState<number>(1);
    const [voiceVolume, setVoiceVolume] = useState<number>(1);

    const updateMasterVolume = (volume: number) => {
        const rounded = Math.round((volume + Number.EPSILON) * 100) / 100;
        setMasterVolume(Math.min(Math.max(volume, 0), 1));
    }

    const updateMusicVolume = (volume: number) => {
        const rounded = Math.round((volume + Number.EPSILON) * 100) / 100;
        setMusicVolume(Math.min(Math.max(rounded, 0), 1));
    }

    const updateAmbiantVolume = (volume: number) => {
        const rounded = Math.round((volume + Number.EPSILON) * 100) / 100;
        setAmbiantVolume(Math.min(Math.max(rounded, 0), 1));
    }

    const updateInterfaceVolume = (volume: number) => {
        const rounded = Math.round((volume + Number.EPSILON) * 100) / 100;
        setInterfaceVolume(Math.min(Math.max(rounded, 0), 1));
    }

    const updateEffectVolume = (volume: number) => {
        const rounded = Math.round((volume + Number.EPSILON) * 100) / 100;
        setEffectVolume(Math.min(Math.max(rounded, 0), 1));
    }

    const updateVoiceVolume = (volume: number) => {
        const rounded = Math.round((volume + Number.EPSILON) * 100) / 100;
        setVoiceVolume(Math.min(Math.max(rounded, 0), 1));
    }

    useEffect(() => {
        updateMusicVolume(musicVolume)
        updateAmbiantVolume(ambiantVolume);
        updateInterfaceVolume(interfaceVolume);
        updateEffectVolume(effectVolume);
        updateVoiceVolume(voiceVolume);
    }, [masterVolume]);

    return(
        <SettingsContext.Provider value={{ 
            masterVolume,
            musicVolume,
            ambiantVolume,
            interfaceVolume,
            effectVolume,
            voiceVolume,
            updateMasterVolume,
            updateMusicVolume,
            updateAmbiantVolume,
            updateInterfaceVolume,
            updateEffectVolume,
            updateVoiceVolume,
        }}>
            {children}
        </SettingsContext.Provider>
    )
};

export const useSettings = () => {
    const context = useContext(SettingsContext);
    if (!context) {
        throw new Error("useSettings must be used within an SettingsProvider");
    }
    return context;
};