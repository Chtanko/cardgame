import {IconMessageCircleFilled, IconSettingsFilled, IconMenu2} from '@tabler/icons-react';
import "./menu.css";
import { useMenu } from '@/context/menuContext';
import { useEffect } from 'react';
import { useTranslations } from 'next-intl';


export const Menu = () => {
    const { openMenu, openSettings, openChat } = useMenu();

    return (
        <div className="mini-menu-container">
            <button className="mini-menu-button" onClick={() => openMenu()}><IconMenu2/></button>
            <button className="mini-menu-button" onClick={() => openSettings()}><IconSettingsFilled/></button>
            <button className="mini-menu-button" onClick={() => openChat()}><IconMessageCircleFilled/></button>
        </div>
    );
}




export const EscapeMenu = () => {

    const { openMenu, openedMenu, openSettings } = useMenu();
    const t  = useTranslations("Global");

    useEffect(() => {
        const handleKeyDown = (event: KeyboardEvent) => {
            if (event.key === "Escape") {
                openMenu();
            }
        };
    
        window.addEventListener("keydown", handleKeyDown);
        
        return () => {
            window.removeEventListener("keydown", handleKeyDown);
        };
    }, []);
    
    return(
        <div className={`${openedMenu ? "escape-menu-container" : "escape-menu-close"}`}>
            <div className='escape-menu'>
                <h1>{t("MENU")}</h1>
                <hr />
                <button className='escape-menu-button' onClick={openSettings}>{t("SETTINGS")}</button>
                <button className='escape-menu-button'>{t("ACCOUNT")}</button>
                <button className='escape-menu-button'>{t("DISCONNECTED")}</button>
                <br/>
                <button className='escape-menu-button' onClick={openMenu}>{t("BACK")}</button>
            </div>
        </div>
    );
}