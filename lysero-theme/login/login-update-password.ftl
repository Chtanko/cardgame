<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Password</title>
</head>
<body>
    <h1>Update Your Password</h1>
    <form id="kc-update-password-form" action="${url.loginAction}" method="post">
        <label for="password-new">New Password:</label>
        <input type="password" id="password-new" name="password-new" required>
        <br>
        <label for="password-confirm">Confirm Password:</label>
        <input type="password" id="password-confirm" name="password-confirm" required>
        <br>
        <button type="submit">Update Password</button>
    </form>
</body>
</html>
