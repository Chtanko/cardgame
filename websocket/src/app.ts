import express, { Request, Response } from "express";
import http from "http";
import dotenv from "dotenv";
import initializeSocketIO from "@sockets/index"; // Gestionnaire Socket.IO

dotenv.config();

const app = express();
app.use(express.json());

// Création du serveur HTTP
const server = http.createServer(app);

// Intégration de Socket.IO
const io = initializeSocketIO(server);

export {server, io};

