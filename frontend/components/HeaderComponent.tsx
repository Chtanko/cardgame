import { ConnectButton } from '@rainbow-me/rainbowkit';
import {Link} from '@/i18n/routing';
import { useTranslations } from 'next-intl';
import "./headerComponent.css";
import { SoundEnum, useAudio } from '@/context/audioContext';

type HeaderProps = {
    activate: number;
}

export const MainHeader = ({activate}: HeaderProps) => {
    const t = useTranslations('Global');
    const { playSoundInterfaceEffect } = useAudio();
    
    return (
        <header className="header">
            <div className="logo">
                <img src="/logo.svg" alt="Logo" className="logo-image"/>
            </div>
            
            <nav className="menu">
                <Link href="/campaign" 
                        className={`menu-item ${activate === 0 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("CAMPAIGN")}
                </Link>
                <Link href="/multiplayer/demo" 
                        className={`menu-item ${activate === 1 ? 'active' : ''}`} 
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("MULTIPLAYER")}
                </Link>
                <Link href="/expension" 
                        className={`menu-item ${activate === 2 ? 'active' : ''}`} 
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("CONQUEST_MODE")}
                </Link>
                <Link href="/lobby" 
                        className={`menu-item ${activate === 3 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("LOBBY")}
                </Link>
                <Link href="/marketplace" 
                        className={`menu-item ${activate === 4 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("MARKETPLACE")}
                </Link>
                <Link href="/factions" 
                        className={`menu-item ${activate === 5 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("FACTIONS")}
                </Link>
            </nav>
            <ConnectButton />
        </header>
    );
}

export const MultiplayerHeader = ({activate}: HeaderProps) => {
    const t = useTranslations('Global');
    const { playSoundInterfaceEffect } = useAudio();
    return (
        <header className="header-game">
            <nav className="menu">
                <Link href="/multiplayer/demo" 
                        className={`menu-item ${activate === 0 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("DEMO_MODE")}
                </Link>
                <Link href="/multiplayer/versus" 
                        className={`menu-item ${activate === 1 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("CLASSIC_MODE")}
                </Link>
                <Link href="/multiplayer/customized" 
                        className={`menu-item ${activate === 2 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("CUSTOMIZED_MODE")}
                </Link>
            </nav>
        </header>
    );
}

export const MarketPlaceHeader = ({activate}: HeaderProps) => {
    const t = useTranslations('Global');
    const { playSoundInterfaceEffect } = useAudio();
    return (
         <header className="header-game">
            <nav className="menu">
                <Link href="/deck" 
                        className={`menu-item ${activate === 0 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("DECK")}
                </Link>
                <Link href="/inventory" 
                        className={`menu-item ${activate === 1 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("INVENTORY")}
                </Link>
                <Link href="/marketplace" 
                        className={`menu-item ${activate === 2 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("MARKETPLACE")}
                </Link>
            </nav>
        </header>
    );
}

export const FactionsHeader = ({activate}: HeaderProps) => {
    const t = useTranslations('Global');
    const { playSoundInterfaceEffect } = useAudio();
    return (
         <header className="header-game">
            <nav className="menu">
                <Link href="/factions" 
                        className={`menu-item ${activate === 0 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("FACTIONS")}
                </Link>
                <Link href="/guild" 
                        className={`menu-item ${activate === 1 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("GUILD")}
                </Link>
                <Link href="/lab" 
                        className={`menu-item ${activate === 2 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("LAB")}
                </Link>
                <Link href="/community" 
                        className={`menu-item ${activate === 3 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("COMMUNITY")}
                </Link>
                <Link href="/lore" 
                        className={`menu-item ${activate === 4 ? 'active' : ''}`}
                        onClick={() => playSoundInterfaceEffect(SoundEnum.INTERFACE_1)}>
                    {t("LORE")}
                </Link>
            </nav>
        </header>
    );
}