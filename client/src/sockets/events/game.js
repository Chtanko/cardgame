import { roomJoinedHandler, playerJoinedHandler, playerLeftHandler, roomRejoinedHandler, 
    playerRejoinedHandler, noRoomHandler, loadGameHandler } from '../handlers/roomHandler';
import { noDeckHandler, endTurnHandler, notYourTurnHandler, endGameHandler,
    cardNotInBoardHandler, cardNotInTargetBoardHandler, cardInTargetBoardHandler, 
    cardAttackHandler, playerAttackHandler, playCardHandler,
    cardNotInHandHandler, notEnoughManaHandler} from '../handlers/gameHandler';
import { errorHandler } from '../handlers/errorHandler';

export const gameEvents = (socket, setStateHandlers) => {
   const onRoomJoined = (message) => {
        if (setStateHandlers.roomJoinedHandler) {
            setStateHandlers.roomJoinedHandler(message);
        }
        roomJoinedHandler(message);
    }
    const onPlayerJoined = (message) => {
        if (setStateHandlers.roomJoinedHandler) {
            setStateHandlers.roomJoinedHandler(message);
        }
        playerJoinedHandler(message);
    }
    const onRoomRejoined = (message) => {
        if (setStateHandlers.roomJoinedHandler) {
            setStateHandlers.roomJoinedHandler(message);
        }
        roomRejoinedHandler(message);
    }
    const onPlayerRejoined = (message) => {
        if (setStateHandlers.roomJoinedHandler) {
            setStateHandlers.roomJoinedHandler(message);
        }
        playerRejoinedHandler(message);
    }
    const onLoadGame = (message) => {
        if (setStateHandlers.roomJoinedHandler) {
            setStateHandlers.roomJoinedHandler(message);
        }
        loadGameHandler(message);
    }
    const onNoDeck = (message) => {
        noDeckHandler(message);
    }
    const onPlayerLeft = (message) => {
        playerLeftHandler(message);
    }
    const onEndTurn = (message) => {
        if (setStateHandlers.gameActionHandler) {
            setStateHandlers.gameActionHandler(message);
        }
        endTurnHandler(message);
    }
    const onEndGame = (message) => {
        endGameHandler(message);
    }
    const onError = (message) => {
        errorHandler(message);
    }
    const onNotYourTurn = (message) => {
        notYourTurnHandler(message);
    }
    const onCardNotInBoard = (message) => {
        cardNotInBoardHandler(message);
    }
    const onCardNotInTargetBoard = (message) => {
        cardNotInTargetBoardHandler(message);
    }
    const onCardInTargetBoard = (message) => {
        cardInTargetBoardHandler(message);
    }
    const onCardAttack = (message) => {
        if (setStateHandlers.gameActionHandler) {
            setStateHandlers.gameActionHandler(message);
        }
        cardAttackHandler(message);
    }
    const onPlayerAttack = (message) => {
        if (setStateHandlers.gameActionHandler) {
            setStateHandlers.gameActionHandler(message);
        }
        playerAttackHandler(message);
    }
    const onPlayCard = (message) => {
        if (setStateHandlers.gameActionHandler) {
            setStateHandlers.gameActionHandler(message);
        }
        playCardHandler(message);
    }
    const onNoRoom = (message) => {
        if (setStateHandlers.noRoomHandler) {
            setStateHandlers.noRoomHandler(message);
        }
        noRoomHandler(message);
    }
    const onNotEnoughMana = (message) => {
        if (setStateHandlers.gameActionHandler) {
            setStateHandlers.gameActionHandler(message);
        }
        notEnoughManaHandler(message);
    }
    const onCardNotInHand = (message) => {
        if (setStateHandlers.noRoomHandler) {
            setStateHandlers.noRoomHandler(message);
        }
        cardNotInHandHandler(message);
    }

    socket.on("roomJoined", onRoomJoined);
    socket.on("playerJoined", onPlayerJoined);
    socket.on("roomRejoined", onRoomRejoined);
    socket.on("playerRejoined", onPlayerRejoined);
    socket.on("noDeck", onNoDeck);
    socket.on("playerLeft", onPlayerLeft);
    socket.on("endTurn", onEndTurn);
    socket.on("endGame", onEndGame);
    socket.on("error", onError);
    socket.on("notYourTurn", onNotYourTurn);
    socket.on("notEnoughMana", onNotEnoughMana);
    socket.on("cardNotInHand", onCardNotInHand);
    socket.on("cardNotInBoard", onCardNotInBoard);
    socket.on("cardNotInTargetBoard", onCardNotInTargetBoard);
    socket.on("cardInTargetBoard", onCardInTargetBoard);
    socket.on("cardAttack", onCardAttack);
    socket.on("playerAttack", onPlayerAttack);
    socket.on("playCard", onPlayCard);
    socket.on("noRoom", onNoRoom);
    socket.on("loadGame", onLoadGame);

    return {
        cleanup: () => {
            // Nettoyer les événements pour éviter les doublons
            socket.off("roomJoined", onRoomJoined);
            socket.off("playerJoined", onPlayerJoined);
            socket.off("roomRejoined", onRoomRejoined);
            socket.off("playerRejoined", onPlayerRejoined);
            socket.off("noDeck", onNoDeck);
            socket.off("playerLeft", onPlayerLeft);
            socket.off("endTurn", onEndTurn);
            socket.off("endGame", onEndGame);
            socket.off("error", onError);
            socket.off("notYourTurn", onNotYourTurn);
            socket.off("notEnoughMana", onNotEnoughMana);
            socket.off("cardNotInHand", onCardNotInHand);
            socket.off("cardNotInBoard", onCardNotInBoard);
            socket.off("cardNotInTargetBoard", onCardNotInTargetBoard);
            socket.off("cardInTargetBoard", onCardInTargetBoard);
            socket.off("cardAttack", onCardAttack);
            socket.off("playerAttack", onPlayerAttack);
            socket.off("playCard", onPlayCard);
            socket.off("noRoom", onNoRoom);
            socket.off("loadGame", onLoadGame);
        }
    };
}