import { redisClient } from "@database/redis";
import {persistRoomState} from "@database/timescale";
import { MessageResult, Room } from "@models/gameModel";
import {getRoomState} from "@services/roomService";

export const synchronizeRoomStates = async () => {
    const roomKeys = await redisClient.keys("room:*");
    for (const key of roomKeys) {
        const roomId = key.split(":")[1];
        const actionResult = await getRoomState(roomId, redisClient);

        if(actionResult.message === MessageResult.NoRoom){
            continue;
        }
        
        const roomState : Room = actionResult.action;

        if (roomState) {
            await persistRoomState(roomId, roomState);
        }
    }
};