'use client';

import { useEffect } from 'react';
import { useRouter } from '@/i18n/routing';

export default function DefaultPage() {
  const router = useRouter();

  useEffect(() => {
      router.replace('/lobby');
  }, [router]);

  return (
<></>
  );
}
