import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      crypto: 'crypto-browserify',
      stream: 'readable-stream',
      buffer: 'buffer',
      assert: 'assert',
      http: 'stream-http',
      https: 'https-browserify',
      os: 'os-browserify/browser',
      zlib: 'browserify-zlib',
      path: 'path-browserify',
    },
  },
  define: {
    global: 'globalThis',
    'process.env': {},
    process: {
      env: {},
      nextTick: (callback, ...args) => {
        Promise.resolve().then(() => callback(...args));
      },
    },
  },
  build: {
    rollupOptions: {
      output: {
        manualChunks: {
          vendor: ['react', 'react-dom', 'ethers'], // Divise les dépendances communes
        },
      },
    },
  },
});
