import { ActionResult, ChatMessage, MessageResult } from "@models/gameModel";
import Redis from "ioredis";

export async function getChatRoomState(roomId: string, redisClient: Redis) : Promise<ActionResult> {
    let chatRoom = await redisClient.get(`chat:${roomId}`);
    
    if (chatRoom === null){
        await redisClient.set(`chat:${roomId}`, JSON.stringify([]), 'EX', 2000);
        chatRoom = await redisClient.get(`chat:${roomId}`);
        
        if (chatRoom === null){
            return { action:null, message: MessageResult.NoChatRoom};
        }
    }
    return { action:JSON.parse(chatRoom), message: MessageResult.GetChatRoom};
}

export async function insertChatRoom(roomId: string, message: string, sender: string, redisClient: Redis) : Promise<ActionResult>{
    const chatRoomResult = await getChatRoomState(roomId, redisClient);

    if(chatRoomResult.message !== MessageResult.GetChatRoom){
        return { action: null, message: MessageResult.NoChatRoom };
    }

    const value = chatRoomResult.action as ChatMessage[];

    const date = new Date();
    value.push({message: message, sender: sender, date: date});

    await redisClient.set(`chat:${roomId}`, JSON.stringify(value), 'EX', 2000);

    return { action: {sender: sender, message: message, date: date} as ChatMessage, message: MessageResult.InsertChatRoom };
}