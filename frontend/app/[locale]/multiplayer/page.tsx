'use client';

import { MainHeader, MultiplayerHeader } from "@/components/HeaderComponent";
import { useRouter } from "@/i18n/routing";
import { useEffect } from "react";

const MarketPlacePage = () => {
    const router = useRouter();
    
    useEffect(() => {
        router.push("/multiplayer/demo");
    }, []);

    return(
        <div className="background">
            <MainHeader activate={0}/>
            <MultiplayerHeader activate={0}/>
        </div>
    )
}

export default MarketPlacePage;