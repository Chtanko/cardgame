<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Approve Access</title>
</head>
<body>
    <h1>Approve Access</h1>
    <p>The application <strong>${client.clientId}</strong> is requesting access to your account.</p>
    <form id="kc-oauth-grant-form" action="${url.oauthAction}" method="post">
        <button type="submit" name="submit" value="accept">Accept</button>
        <button type="submit" name="submit" value="cancel">Deny</button>
    </form>
</body>
</html>
