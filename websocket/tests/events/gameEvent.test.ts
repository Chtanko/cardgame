import { Server, Socket } from "socket.io";
import { v4 as uuidv4 } from "uuid";
import { joinGameHandler } from "../../src/sockets/handlers/gameHandler";
import { getIncompleteRoom, createRoom, insertRoomState, getRoomState, joinRoom } from "../../src/services/roomService"; 
import { getDeckById} from "../../src/services/deckService";
import timescalePool from "../../src/database/timescale";
import redisClient from "../../src/database/redis";
import { MessageResult } from "../../src/models/gameModel";

jest.mock("../../src/services/deckService");
jest.mock("../../src/services/roomService");
jest.mock("../../src/database/redis");
jest.mock("../../src/database/timescale");

describe("joinGame event", () => {
  let mockSocket: jest.Mocked<Socket>;
  let mockIo: jest.Mocked<Server>;

  beforeEach(() => {
    jest.clearAllMocks();

    mockSocket = {
      emit: jest.fn(),
      join: jest.fn(),
      to: jest.fn().mockReturnValue({
        emit: jest.fn(),
      }),
    } as unknown as jest.Mocked<Socket>;

    mockIo = {
      to: jest.fn().mockReturnValue({
        emit: jest.fn(),
      }),
    } as unknown as jest.Mocked<Server>;
  });

  const expectedUUID = (): string => {
    return expect.stringMatching(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/)
  }

  test("should emit 'noDeck' if deck is not found", async () => {
    (getDeckById as jest.Mock).mockResolvedValue({ message: MessageResult.NoDeck });

    const payload = { player: { playerId: "player1", deckId: "deck1" } };

    await joinGameHandler(mockSocket, redisClient, timescalePool, payload);

    expect(getDeckById).toHaveBeenCalledWith("player1", "deck1", redisClient, timescalePool);
    expect(mockSocket.emit).toHaveBeenCalledWith("noDeck", {});
  });

  test("should create a new room if no incomplete room is found", async () => {
    const mockDeck = [{ cardId: "card1", mana: 1, attack: 1, life: 1 }];

    (getDeckById as jest.Mock).mockResolvedValue({ message: MessageResult.GetDeck, action: mockDeck });
    (getIncompleteRoom as jest.Mock).mockResolvedValue(null);
    (createRoom as jest.Mock).mockReturnValue({ roomId: "room1", players: [] });
    (insertRoomState as jest.Mock).mockResolvedValue(undefined);
    (getRoomState as jest.Mock).mockResolvedValue({ roomId: "room1", players: [] });
    (joinRoom as jest.Mock).mockReturnValue({ roomId: "room1", players: ["player1"] });

    const payload = { player: { playerId: "player1", deckId: "deck1" } };

    await joinGameHandler(mockSocket, redisClient, timescalePool, payload);

    expect(getDeckById).toHaveBeenCalledWith("player1", "deck1", redisClient, timescalePool);
    expect(getIncompleteRoom).toHaveBeenCalledWith(redisClient);
    expect(createRoom).toHaveBeenCalled();
    expect(insertRoomState).toHaveBeenCalled();
    expect(mockSocket.join).toHaveBeenCalledWith(expectedUUID());
    expect(mockSocket.emit).toHaveBeenCalledWith(
      "roomJoined",
      expect.objectContaining({
        roomId: expectedUUID(),
        roomState: expect.objectContaining({
          roomId: "room1",
          players: [],
        }),
      })
    );
  });

  test("should join an existing room if found", async () => {
    const mockDeck = [{ cardId: "card1", mana: 1, attack: 1, life: 1 }];

    (getDeckById as jest.Mock).mockResolvedValue({ message: MessageResult.GetDeck, action: mockDeck });
    (getIncompleteRoom as jest.Mock).mockResolvedValue("room1");
    (getRoomState as jest.Mock).mockResolvedValue({ roomId: "room1", players: [] });
    (joinRoom as jest.Mock).mockReturnValue({ roomId: "room1", players: ["player1"] });
    (insertRoomState as jest.Mock).mockResolvedValue(undefined);

    const payload = { player: { playerId: "player1", deckId: "deck1" } };

    await joinGameHandler(mockSocket, redisClient, timescalePool, payload);

    expect(getIncompleteRoom).toHaveBeenCalledWith(redisClient);
    expect(getRoomState).toHaveBeenCalledWith("room1", redisClient);
    expect(joinRoom).toHaveBeenCalled();
    expect(insertRoomState).toHaveBeenCalled();
    expect(mockSocket.join).toHaveBeenCalledWith("room1");
    expect(mockSocket.emit).toHaveBeenCalledWith("roomJoined", { roomId: "room1", roomState: { roomId: "room1", players: [] } });
    expect(mockSocket.to("room1").emit).toHaveBeenCalledWith("playerJoined", "player1");
  });

  test("should handle errors gracefully", async () => {
    (getDeckById as jest.Mock).mockRejectedValue(undefined);

    const payload = { player: { playerId: "player1", deckId: "deck1" } };

    await joinGameHandler(mockSocket, redisClient, timescalePool, payload);

    expect(getDeckById).toHaveBeenCalledWith("player1", "deck1", redisClient, timescalePool);
    expect(mockSocket.emit).not.toHaveBeenCalledWith("roomJoined", expect.anything());
    expect(mockSocket.to).not.toHaveBeenCalled();
  });
});
