import {
    AvatarComponent,
  } from '@rainbow-me/rainbowkit';

  import backdround from "@/public/profil.png";

export const CustomAvatar: AvatarComponent = ({ address, ensImage, size }) => {
    return ensImage ? (
      <img
        src={ensImage}
        width={size}
        height={size}
        style={{ borderRadius: 999 }}
      />
    ) : (
      <div
        style={{
          backgroundImage: `url("${backdround.src}")`,
          borderRadius: 999,
          backgroundSize: "cover",
          height: size,
          width: size,
        }}
      >
        
      </div>
    );
  };