import React, { useState, useRef } from "react";

export const CardComponent = ({card, position: {x, y}, placeCard}) => {
    const [isDragging, setIsDragging] = useState(false);
    const [position, setPosition] = useState({ x: x, y: y });
    const [initialPosition] = useState({ x: x, y: y }); // Position de départ
    const cardRef = useRef(null);

    const handleMouseDown = (e) => {
        // Vérifie si c'est un clic gauche (button === 0)
        if (e.button !== 0) {
            setIsDragging(false);
            return;
        }
        setIsDragging(true);
    };
  
    const handleMouseUp = () => {
      setIsDragging(false);

      const cardCurrent = cardRef.current;
      const plateau = document.getElementById("board");

      if (plateau) {
        const cardRect = cardCurrent.getBoundingClientRect();
        const plateauRect = plateau.getBoundingClientRect();
  
        const isInsidePlateau =
          (cardRect.right - 200) > plateauRect.left &&
          (cardRect.left + 200) < plateauRect.right &&
          (cardRect.bottom - 200) > plateauRect.top &&
          (cardRect.top + 200) < plateauRect.bottom;
  
        if (!isInsidePlateau) {
          // Revient à sa position initiale si hors plateau
          setPosition(initialPosition);
        }else {
          const val = placeCard(card);
          if(!val){
            setPosition(initialPosition);
          }
        }
      }
    };
  
    const handleMouseMove = (e) => {
      if (!isDragging) return;
  
      // Met à jour la position en fonction du mouvement de la souris
      setPosition((prev) => ({
        x: prev.x + e.movementX,
        y: prev.y + e.movementY,
      }));
    };

  return (
    <div
        ref={cardRef}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
        onMouseMove={handleMouseMove}
        style={{
            ...cardStyle,
            width: "200px",
            height: "250px",
            backgroundImage: `url(/cards/${card.cardId}.png)`,
            cursor: isDragging ? "grabbing" : "grab",
            position: "absolute",
            left: `${position.x}px`,
            top: `${position.y}px`,
        }}
    >
    </div>
  );
};

const cardStyle = {
  borderRadius: "5px",
  backgroundSize: 'cover',
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  textAlign: "center",
  color: "black",
};

export const CardComponentEnnemy = ({card, position: {x, y}}) => {
  const [position, setPosition] = useState({ x: x, y: y });  

return (
  <div
      style={{
          ...cardStyle,
          width: "100px",
          height: "125px",
          backgroundImage: "url(/cards/33333333-3333-3333-3333-333333333333.png)",
          backgroundSize: "100px 125px",
          position: "absolute",
          left: `${position.x}px`,
          top: `${position.y}px`,
      }}
  >
  </div>
);
};

export const CardComponentBoardEnnemy = ({card, position: {x, y}, attackCard}) => {
  const [position, setPosition] = useState({ x: x, y: y });  

  return (
    <div
      onMouseDown={() => attackCard(card)}
      style={{
          ...cardStyle,
          width: "125px",
          height: "175px",
          backgroundImage: `url(/cards/${card.cardId}.png)`,
          backgroundSize: "125px 175px",
          position: "absolute",
          left: `${position.x}px`,
          top: `${position.y}px`,
      }}
    />
  );
};

export const CardComponentBoard = ({card, position: {x, y}, selectCard}) => {
  const [position, setPosition] = useState({ x: x, y: y });  

  return (
    <div
      onMouseDown={() => selectCard(card)}
      style={{
        ...cardStyle,
        width: "125px",
        height: "175px",
        backgroundImage: `url(/cards/${card.cardId}.png)`,
        backgroundSize: "125px 175px",
        position: "absolute",
        left: `${position.x}px`,
        top: `${position.y}px`,
      }}
    />
  );
};

export default CardComponent;
