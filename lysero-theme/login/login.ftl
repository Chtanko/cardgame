<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Portal Lysero</title>
    <link rel="stylesheet" href="${url.resourcesPath}/css/style.css">
    <script src="${url.resourcesPath}/js/script.js"></script>
</head>
<body>
    <div class="login-container">
        <div class="logo">
            <img src="${url.resourcesPath}/img/lysero.webp" alt="Custom Logo">
        </div>
        <form id="kc-form-login" action="${url.loginAction}" method="post">
            <div class="form-group">
                <label for="username">Username:</label>
                <input type="text" id="username" name="username" placeholder="Enter your username" required>
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" placeholder="Enter your password" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn-submit">Login</button>
            </div>
        </form>

        <div class="footer">
            <p>Need help? <a href="#">Contact support</a></p>
        </div>
    </div>
</body>
</html> 