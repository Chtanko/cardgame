/* eslint-disable prefer-destructuring */
import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import styles from '../styles';
import { ActionButton, Alert, Card, GameInfo, PlayerInfo } from '../components';
import { useGlobalContext } from '../context';
import { attack, defense, player01 as player01Icon, player02 as player02Icon } from '../assets';
//import { playAudio } from '../utils/animation.js';
import { io } from "socket.io-client";

const socket = io("http://localhost:3001"); // URL de ton serveur WebSocket

const Battle = () => {
  const { contract, gameData, battleGround, walletAddress, setErrorMessage, showAlert, setShowAlert, player1Ref, player2Ref } = useGlobalContext();
  const [player2, setPlayer2] = useState({});
  const [player1, setPlayer1] = useState({});
  const { battleName } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const getPlayerInfo = async () => {
      try {
        let player01Address = null;
        let player02Address = null;

        if (gameData.activeBattle.players[0].toLowerCase() === walletAddress.toLowerCase()) {
          player01Address = gameData.activeBattle.players[0];
          player02Address = gameData.activeBattle.players[1];
        } else {
          player01Address = gameData.activeBattle.players[1];
          player02Address = gameData.activeBattle.players[0];
        }

        const p1TokenData = await contract.getPlayerToken(player01Address);
        const player01 = await contract.getPlayer(player01Address);
        const player02 = await contract.getPlayer(player02Address);

        const p1Att = p1TokenData.attackStrength.toNumber();
        const p1Def = p1TokenData.defenseStrength.toNumber();
        const p1H = player01.playerHealth.toNumber();
        const p1M = player01.playerMana.toNumber();
        const p2H = player02.playerHealth.toNumber();
        const p2M = player02.playerMana.toNumber();

        setPlayer1({ ...player01, att: p1Att, def: p1Def, health: p1H, mana: p1M });
        setPlayer2({ ...player02, att: 'X', def: 'X', health: p2H, mana: p2M });
      } catch (error) {
        setErrorMessage(error.message);
      }
    };

    if (contract && gameData.activeBattle) getPlayerInfo();
  }, [contract, gameData, battleName]);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (!gameData?.activeBattle) navigate('/');
    }, 2000);

    return () => clearTimeout(timer);
  }, [gameData]);

  const makeAMove = async (choice) => {
    //playAudio(choice === 1 ? attackSound : defenseSound);

    try {
      //await contract.attackOrDefendChoice(choice, battleName, { gasLimit: 200000 });
      socket.emit("playerAction", { battleName, playerName: walletAddress, actionType: choice });

      setShowAlert({
        status: true,
        type: 'info',
        message: `Initiating ${choice === 1 ? 'attack' : 'defense'}`,
      });
    } catch (error) {
      setErrorMessage(error);
    }
  };

  useEffect(() => {
    socket.on("actionRegistered", ({ playerName, actionType }) => {
      setShowAlert({
        status: true,
        type: "info",
        message: `${playerName} a choisi ${actionType === 1 ? 'attack' : 'defense'}`,
      });
    });

    socket.on("roundResult", (result) => {
      console.log("Résultat du round : ", result);

      setPlayer1(prev => ({
        ...prev,
        health: result.player1.health,
        mana: result.player1.mana,
      }));
      setPlayer2(prev => ({
        ...prev,
        health: result.player2.health,
        mana: result.player2.mana,
      }));

      setShowAlert({
        status: true,
        type: "success",
        message: "Résultat du round mis à jour !",
      });
    });

    socket.on("playerLeft", ({ players }) => {
      setShowAlert({
        status: true,
        type: "warning",
        message: "Un joueur a quitté la bataille.",
      });

      if (players.length === 1) setPlayer2({});
    });

    return () => {
      socket.off("actionRegistered");
      socket.off("roundResult");
      socket.off("playerLeft");
    };
  }, []);

    // Styles pour l'image de fond
  const containerStyle = {
    height: '100vh', // Fixe la hauteur à celle de l'écran
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundImage: `url(${battleGround})`,
    backgroundSize: 'contain', // Ajuste l'image entière à l'écran
    backgroundPosition: 'top center', // Centre l'image et évite les débordements
    backgroundRepeat: 'no-repeat',
    overflow: 'hidden',
  };

  return (
      <div className={`${styles.flexBetween} ${styles.gameContainer} ${battleGround}`}>
        {showAlert?.status && <Alert type={showAlert.type} message={showAlert.message} />}

        <PlayerInfo player={player2} playerIcon={player02Icon} mt />

        <div className={`${styles.flexCenter} flex-col my-10`}>
          <Card
              card={player2}
              title={player2?.playerName}
              cardRef={player2Ref}
              playerTwo
          />

          <div className="flex items-center flex-row">
            <ActionButton
                imgUrl={attack}
                handleClick={() => makeAMove(1)}
                restStyles="mr-2 hover:border-yellow-400"
            />

            <Card
                card={player1}
                title={player1?.playerName}
                cardRef={player1Ref}
                restStyles="mt-3"
            />

            <ActionButton
                imgUrl={defense}
                handleClick={() => makeAMove(2)}
                restStyles="ml-2 hover:border-red-600"
            />
          </div>
        </div>

        <PlayerInfo player={player1} playerIcon={player01Icon} />

        <GameInfo />

      </div>
  );
};


export default Battle;