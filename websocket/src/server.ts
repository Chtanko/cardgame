import {server, io} from "@src/app";
import { synchronizeRoomStates } from "@services/roomSync";
import { instrument } from "@socket.io/admin-ui";


const PORT = process.env.PORT || 3001;

server.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});

process.on("uncaughtException", (err) => {
    console.error("Uncaught Exception:", err);
    process.exit(1);
});

process.on("unhandledRejection", (reason, promise) => {
    console.error("Unhandled Rejection at:", promise, "reason:", reason);
});

setInterval(() => {
    console.log(`Statistiques :
        - Batailles actives : ${io.sockets.adapter.rooms.size}
        - Joueurs connectés : ${io.sockets.sockets.size}
    `);
}, 60000); // Toutes les 60 secondes

instrument(io, {
    auth: false,
    mode: "development",
});

setInterval(synchronizeRoomStates, 60 * 1000); // Toutes les 60 secondes