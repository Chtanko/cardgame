import { Card, CardRaw, ActionResult, MessageResult } from "@models/gameModel";
import Redis from "ioredis";

export const getDeckById = async (playerId: string, deckId: string, redisClient: Redis, pool: any): Promise<ActionResult> => {
    const redisKey = `deck:${deckId}`;
  
    // Vérifier si le deck est dans Redis
    const cachedDeck = await redisClient.get(redisKey);
    if (cachedDeck) {
      console.log('Deck récupéré depuis Redis');
      return {action: JSON.parse(cachedDeck), message: MessageResult.GetDeck}
    }
  
    // Si pas dans Redis, récupérer depuis TimescaleDB
    const query = `
    SELECT * 
    FROM get_cards_of_deck($1, $2);
    `;
    const values = [deckId, playerId];
    let deck: Card[];
    try {
      const result = await pool.query(query, values);
      deck = result.rows.map((row: CardRaw) => ({
        cardId: row.cardid,
        mana: row.mana,
        attack: row.attack,
        life: row.life,
      }));
    } catch (error) {
      console.error('Error fetching deck:', error);
      return {action: null, message: MessageResult.NoDeck};
    }
    // Stocker dans Redis avec une expiration (par exemple, 1 heure)
    await redisClient.set(redisKey, JSON.stringify(deck), 'EX', 3600);
    console.log('Deck récupéré depuis TimescaleDB et stocké dans Redis');
  
    return {action: deck, message: MessageResult.GetDeck};
  };