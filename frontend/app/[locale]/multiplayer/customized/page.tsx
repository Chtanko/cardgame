'use client';

import { SetStateAction, useEffect, useState } from "react";
import { useSocket } from '@/context/socketContext';
import { roomEvents } from '@/sockets/events/game'
import { useRouter } from '@/i18n/routing';
import { RoomStatus } from "@/models/gameModel";
import { RoomHandlers, HandlerMessageToServer } from "@/models/handlerModel";
import { useTranslations } from "next-intl";
import { PLAYER_TWO_ID, PLAYER_ONE_ID } from "@/models/constant";
import "../multiplayer.css";
import { MultiplayerHeader, MainHeader } from "@/components/HeaderComponent";

const DC1 = "11111111-1111-1111-1111-111111111111";
const DC2 = "10101010-1010-1010-1010-101010101010";

const FAKE_DECK = [
    {"id": "33333333-3333-3333-3333-333333333333"},
    {"id": "44444444-4444-4444-4444-444444444444"},
    {"id": "55555555-5555-5555-5555-555555555555"},
    {"id": "66666666-6666-6666-6666-666666666666"},
    {"id": "77777777-7777-7777-7777-777777777777"},
    {"id": "33333333-3333-3333-3333-333333333333"},
    {"id": "44444444-4444-4444-4444-444444444444"},
    {"id": "55555555-5555-5555-5555-555555555555"},
    {"id": "66666666-6666-6666-6666-666666666666"},
    {"id": "77777777-7777-7777-7777-777777777777"},
]

const CustomizedPage = () => {
    const t = useTranslations('Global');
    const { socket, isConnected } = useSocket();
    const [roomStatus, setRoomStatus] = useState<string>(RoomStatus.Empty);
    const [roomId, setRoomId] = useState<string>("");

    const router = useRouter();
    const [player, setPlayer] = useState(PLAYER_ONE_ID);
    const [deck, setDeck] = useState(DC1);
    const [startText, setStartText] = useState(t('RESEARCH'));

    const handleChange = (event: { target: { value: SetStateAction<string>; }; }) => {
        if(event.target.value === PLAYER_ONE_ID){
            setDeck(DC1);
        }else{
            setDeck(DC2);
        }
        
        setPlayer(event.target.value);
    };
  

    const handlers : RoomHandlers = {
        roomJoinedHandler: {
            setRoomStatus: setRoomStatus,
            setRoomId: setRoomId,
            router: router
        },
        quitGameToClientHandler: {
            setRoomStatus: setRoomStatus,
            setRoomId: setRoomId,
        }
    };

    useEffect(() => {
        if (!socket) return;
        const { cleanup } = roomEvents(socket, handlers);

        return () => {
            cleanup();
        };
    }, [socket]);

    useEffect(() => {
        if(!isConnected) return;
        if(!socket) return;
        socket.emit(HandlerMessageToServer.PlayerIsInGame, {"playerId": player});
    }, [isConnected]);
    
    const handleJoinGame = () => {
      if (isConnected && socket) {
        const send = {
            "playerId": player,
            "deckId": deck
        }
  
        // Envoyer le message 'join game' au serveur
        socket.emit(HandlerMessageToServer.JoinGame, send);
        console.log('Join game message sent:', send);
      } else {
        console.error('Socket not connected!');
      }
    };

    const handleLeaveGame = () => {
        if(roomId === "") return; // afficher une erreur
        if (isConnected && socket) {
          const send = {
              "playerId": player,
              "roomId": roomId
          }
    
          // Envoyer le message 'join game' au serveur
          socket.emit(HandlerMessageToServer.LeaveGame, send);
          console.log('Join game message sent:', send);
        } else {
          console.error('Socket not connected!');
        }
      };

    return(
        <div className="background">
            <MainHeader activate={1}/>
            <MultiplayerHeader activate={2}/>
            <main className="game-body-multiplayer">
                <div className="deck-menu">
                    <div>
                        <h1 className="h1-menu">
                            {t("DEMO_MODE")}
                        </h1>
                        <p className="p-menu">
                            Affronter des joueurs ou des IA dans un mode sans deck et NFT profiter du jeu sans jeu
                        </p>
                    </div>
                    <div className="deck-display">
                        {FAKE_DECK.map((card, id: number ) => (
                            <div key={id} className="card-display" style={{backgroundImage: `url(/cards/${card.id}.png)`}}>
                                
                            </div>
                        ))}
                    </div>
                    <div className="menu-button-container">
                        <button className="menu-button">
                            change deck
                        </button>
                        <button className="menu-button">
                            modify deck 
                        </button>
                    </div>
                </div>
                <div className="game-menu">
                    {roomStatus === RoomStatus.Empty && (
                        <button className="start-button" onClick={handleJoinGame}>
                            {t('JOIN_BATTLE')}
                        </button>
                    )}
                    {roomStatus === RoomStatus.Incomplete && (
                        <button className={`cancel-button`} onClick={handleLeaveGame}
                        onMouseEnter={() => { setStartText(t('CANCEL'));}}
                        onMouseLeave={() => { setStartText(t('RESEARCH')); }}>
                            {startText}
                        </button>
                    )}
                    {roomStatus === RoomStatus.Full && (
                        <p>{t('FULL')}</p>
                    )}
                </div>
            </main>
        </div> 
    )
}

export default CustomizedPage;