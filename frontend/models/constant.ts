import { Room, RoomStatus } from "@/models/gameModel";

export const MAX_PLAYER = 2;
export const MAX_LIFE : number = 30;
export const MAX_MANA : number = 10;
export const PLAYER_ONE_ID : string = "22222222-2222-2222-2222-222222222222";
export const PLAYER_TWO_ID : string = "99999999-9999-9999-9999-999999999999";
export const DEFAULT_ROOM_ID : string = "X1111111-1111-1111-1111-111111111111";

export const PLAYER_NAME_DICT = {
    "22222222-2222-2222-2222-222222222222": "player1",
    "99999999-9999-9999-9999-999999999999": "player2"
}
export const DEFAULT_ROOM : Room = {
    roomId: DEFAULT_ROOM_ID,
    players: [PLAYER_ONE_ID, ],
    status: RoomStatus.Empty,
    turn: 0,
    playerStates: {
        PLAYER_ONE_ID : {
            playerId: PLAYER_ONE_ID,
            mana: MAX_MANA/2,
            life: MAX_LIFE,
            deck: [],
            hand: [],
            board: [],
            grave: [],
            deckLength:0,
            graveLength:0,
            handLength:0
        },
        PLAYER_TWO_ID : {
            playerId: PLAYER_TWO_ID,
            mana: MAX_MANA/2,
            life: MAX_LIFE,
            deck: [],
            hand: [],
            board: [],
            grave: [],
            deckLength:0,
            graveLength:0,
            handLength:0
        }
    },
    dateInsert: new Date(),
    dateUpdate: new Date()
}

export const FAKE_DECK = [
    {"id": "33333333-3333-3333-3333-333333333333"},
    {"id": "44444444-4444-4444-4444-444444444444"},
    {"id": "55555555-5555-5555-5555-555555555555"},
    {"id": "66666666-6666-6666-6666-666666666666"},
    {"id": "77777777-7777-7777-7777-777777777777"},
    {"id": "33333333-3333-3333-3333-333333333333"},
    {"id": "44444444-4444-4444-4444-444444444444"},
    {"id": "55555555-5555-5555-5555-555555555555"},
    {"id": "66666666-6666-6666-6666-666666666666"},
    {"id": "77777777-7777-7777-7777-777777777777"},
] 