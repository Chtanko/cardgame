import { Server as SocketIOServer, ServerOptions, Socket } from "socket.io";
import { Server as HTTPServer } from "http";
import {socketOptions} from "@config/socketOptions"; // Import des options Socket.IO
import authMiddleware from "@sockets/middlewares/auth"; // Middleware d'authentification
import gameEvents from "@sockets/events/game"; // Événements du namespace "game"
import {redisClient} from "@database/redis";
import {timescalePool} from "@database/timescale";

const initializeSocketIO = (server: HTTPServer): SocketIOServer => {
  // Initialisation du serveur Socket.IO
  const io = new SocketIOServer(server, socketOptions as Partial<ServerOptions>);

  // Middleware global d'authentification
  //io.use(authMiddleware);

  // Gestion des namespaces
  const gameNamespace = io.of("/game");
  gameNamespace.on("connection", (socket: Socket) => {
    console.log(`New connection on /game: ${socket.id}`);
    gameEvents(socket, redisClient, timescalePool);
  });

  console.log("Socket.IO initialized");
  return io;
};

export default initializeSocketIO;
