import pkg from 'pg';
import {timescaleConfig} from "@config/pgConfig";

const { Pool } = pkg;

export const timescalePool = new Pool(timescaleConfig);

timescalePool
  .connect()
  .then(() => console.log("Connected to TimescaleDB"))
  .catch((err) => console.error("TimescaleDB connection error:", err));

export const persistRoomState = async (roomId: string, state: any) => {
  const query = `
      INSERT INTO game_states (room_id, state)
      VALUES ($1, $2)
      ON CONFLICT (room_id) DO UPDATE
      SET state = $2, updated_at = NOW();
  `;
  await timescalePool.query(query, [roomId, JSON.stringify(state)]);
};

export const persistGameLogs = async (gameId: string, logs: any[]) => {
  const query = `
      INSERT INTO game_logs (game_id, player_id, action, timestamp)
      VALUES ($1, $2, $3, NOW())
  `;
  const values = logs.map(log => [gameId, log.playerId, JSON.stringify(log.action)]);
  await timescalePool.query(query, values);
};

export default timescalePool;
